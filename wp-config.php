<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'geeko' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9D?vREoMwuy[&A*c<ynz^1-XC-@Gphumf++BX}#qJ$T~v>?4)C|%mxvg@#ZQNtUZ' );
define( 'SECURE_AUTH_KEY',  ')q)yRD6f@kT7PZ`h_o;&:K*TM>?G<G&P@L:u`ro34_+&zf}+N=VAQ$gIwfe[,eB*' );
define( 'LOGGED_IN_KEY',    'vVN]o.`9|6Q0,@5q8vOjzX[m~a<[=q45Fwe9,r_{1G7e):rC;Yn8N+Sc.P[LST}u' );
define( 'NONCE_KEY',        '^J-~zd#6g8{LPW&uG5mSI,Q$!lyd%u?2l{!?T1$CM0v;x5_&?5b.hKUkZPtOUpQ/' );
define( 'AUTH_SALT',        'M_}ok`93t2a]sAWC7&(liAw*DME75whvPY4%]2h>1(Z?h-FVQ$] *{@u}jG`!;3&' );
define( 'SECURE_AUTH_SALT', 'x{wBh58P J}s3|G|^>0r4epCufOEMJ@J*&(N!wo2p>zhGg~t~1WFdt3*7Tq{8^>$' );
define( 'LOGGED_IN_SALT',   'X.p8QbffVBI0$Y.2H#J/ DN_t,akKni4QI A`uc7*l$X-noF8Gm,$J$p0=c<x&xT' );
define( 'NONCE_SALT',       '$qEgvlgQ5vDQQp!)5-g227|WaXVpL/dpdx^WE|K;@%u]zKn>uDTDCkY{;NkZ!8zm' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'v2r_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
