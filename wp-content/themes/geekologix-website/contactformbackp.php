<div class="row contactform">
<div class="col-lg-6 col-md-6">
<label> Your Name (required)
    </label>
[text* text-283 class:form-control placeholder ""]
</div>
<div class="col-lg-6 col-md-6">
<label> Your Email (required)
   </label>
[email* email-494 class:form-control placeholder ""]
</div>
<div class="col-lg-6 col-md-6">
<label> Phone Number 
 </label>
[text* text-935 class:form-control placeholder ""]
</div>
<div class="col-lg-6 col-md-6">
<label> I am Interested in
 </label>
[select* menu-262 class:form-control "Web Development" "App Development"]
</div>
<div class="col-lg-12 col-md-12">
<label> Message</label>
[textarea* textarea-183 class:form-control class:messages placeholder ""]
</div>
</div>
[submit class:common_btns "Send Now"]