<?php
get_header();
// Template Name:Default Page
the_post();
$page_id = $post->ID;
?>
<!-- =======Gallery Lightbox========= -->
<section id="section11" class="section gallery-lightbox faq-page-section ">
  <div class="container ">
    <div class="row">
      <div class="col-lg-12">
        <h6 class="block-heading text-capitalize text-left"> <?php echo the_title(); ?></b></h6>
      </div>
      <div class="col-lg-12"> <?php  the_content() ?> </div> <?php
      // List of Holiday Page Data
      if($page_id == 435){ ?> <div class="col-lg-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Date</th>
              <th scope="col">Day</th>
              <th scope="col">Occassion</th>
            </tr>
          </thead>
          <tbody> <?php  $counter= 1;
          if (get_field('list_of_holidays')) : 
          while (has_sub_field('list_of_holidays')) : ?> <tr>
              <th scope="row"><?php  echo $counter;?></th>
              <td><?php the_sub_field('holiday_date'); ?></td>
              <td><?php the_sub_field('holiday_day'); ?></td>
              <td><?php the_sub_field('holiday_name'); ?></td>
            </tr> <?php $counter++; endwhile; ?> <?php endif; ?> </tbody>
        </table>
      </div> <?php }else { }?>
    </div>
</section>
<!-- =======End Gallery Lightbox========= --> <?php get_footer(); ?>