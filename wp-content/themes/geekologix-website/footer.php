<?php
global $wp;
  /**
   * The template for displaying the footer
   *
   * Contains the closing of the #content div and all content after.
   *
   * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
   *
   * @package geekologix_website
   */
  
  $site_url = site_url();

  ?>
<a id="buttonScroll"><i class="fal fa-chevron-circle-up"></i></a>
</div>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5ed5dffbc75cbf1769f17364/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>
<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- <script src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.3/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/typed.js@2.0.11"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/particles.min.js"></script>
<script type="text/javascript">
    var btn = $('#buttonScroll');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });


    $('#ourwork').owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        autoplay: true,
        dots: false,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 5
            }
        }
    })
    var typed2 = new Typed('#text-type', {
        strings: ["Turning your  <span>vision </span> into  <span>reality</span>"],
        typeSpeed: 80,
        backSpeed: 80,
        smartBackspace: true,
        loop: true,
        cursorChar: '',
        backDelay: 700,
        fadeOut: false,
        fadeOutClass: 'typed-fade-out',
        fadeOutDelay: 500,
    });
    $(document).ready(function () {

        $('#fullpage').fullpage({
            anchors: ['home', 'services', 'our-work', 'career', 'process', 'about-us', 'contact-us'],
            navigation: true,
            navigationPosition: 'left',
            navigationTooltips: ['Home', 'Services', 'Our Work', 'Career', 'Process', 'About Us', 'Contact Us'],
            fadingEffect: true,
            scrollingSpeed: 1000,
            responsiveWidth: 980,
            lazyLoading: true,
            loopHorizontal: true,
            responsiveWidth: 992,
            animateAnchor: true,
            verticalCentered: false,
            scrollBar: true,
            showActiveTooltip: true,

            // autoScrolling: true,
            afterRender: function () {
                new WOW().init();
            },

        });

    });

    /* ---- particles.js config ---- */
    particlesJS("particles-js", {
        "particles": {
            "number": {
                "value": 135,
                "density": {
                    "enable": true,
                    "value_area": 640.6919473030873
                }
            },
            "color": {
                "value": "#25a6b8"
            },
            "shape": {
                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 5
                },
                "image": {
                    "src": "img/github.svg",
                    "width": 100,
                    "height": 100
                }
            },
            "opacity": {
                "value": 0.5,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 1,
                    "opacity_min": 0.1,
                    "sync": false
                }
            },
            "size": {
                "value": 8.00864934128859,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 40,
                    "size_min": 0.1,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": true,
                "distance": 160.17298682577183,
                "color": "#25a6b8",
                "opacity": 0.2562767789212349,
                "width": 0.9610379209546309
            },
            "move": {
                "enable": true,
                "speed": 6,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 320.34597365154366
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": true,
                    "mode": "repulse"
                },
                "onclick": {
                    "enable": true,
                    "mode": "push"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 400,
                    "line_linked": {
                        "opacity": 1
                    }
                },
                "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                },
                "repulse": {
                    "distance": 56.7808502526748,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true
    });

    new WOW().init();




</script>