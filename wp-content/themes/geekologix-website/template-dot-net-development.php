<?php 
    //Template Name:.NET Development
    echo get_header();
?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xl-7">
                    <h1 class="banner-heading">
                        <?php 

              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>Empower your business with Microsoft .Net</p>
                    <!-- <?php echo the_content(); ?> -->
                    <a href="<?php echo get_permalink('321'); ?>/#showcase"
                        class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6 col-xl-5">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">.Net Development Service</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net-developer-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    .NET is a programming language with powerful tools for web, mobile and desktop development.
                </h5>
                <p class="comman_p">
                    Today, a large number of businesses are facing cut-throat competition and thus demand innovative
                    solutions in order to give them an edge. Doing so not just increases their revenue, but also
                    decreases the operational costs. Microsoft’s .NET is the perfect solution for all such business
                    issues as it enables us to develop scalable and flexible business applications within a defined
                    budget and time.

                </p>
                <p class="comman_p">
                    Coming from Microsoft itself, .NET allows building applications suitable for any device and platform
                    such as portals, mobile apps, BI tools, chatbots, data analytics, Dynamics CRM and much more.
                </p>
                <p class="comman_p">
                    Benefitting from the Microsoft technologies, Geekologix delivers fast, scalable, and
                    robust.NET-based applications.
                </p>
                <p class="comman_p">
                    Our team of .NET experts can furnish the developed solutions with efficient cloud services, ensure
                    engaging and highly interactive JavaScript SPA UX and seamless integration via a service bus.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start What Makes Laravel -->
<section class="what-makes-larawel">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Why is .NET perfect for your Web App ?</h3>
            </div>
        </div>
        <div class="dot-net-web-app">
            <div class="row">
                <div class="col-lg-6">
                    <a href="javascript:void(0)" title="Works with all Apps and Platform">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                1
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Works with all Apps and Platform</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Supports Cloud integration">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                2
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Supports Cloud integration</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Delivers high performance">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                3
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Delivers high performance</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Enables Easy globalization and localization">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                4
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Enables Easy globalization and localization</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Supports Integration with Microsoft applications">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                5
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Supports Integration with Microsoft applications</h5>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:void(0)" title="Facilitates asynchronous programming via async/wait">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                6
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Facilitates asynchronous programming via async/wait</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Highly Secure & Reliable">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                7
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Highly Secure & Reliable</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Highly Stable and Scalable">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                8
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Highly Stable and Scalable</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Large Ecosystem">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                9
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Large Ecosystem</h5>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" title="Monolithic & Microservice design architecture">
                        <div class="dot-net-web-card">
                            <div class="dot-net-card-img">
                                10
                            </div>
                            <div class="dot-net-card-heading">
                                <h5>Monolithic & Microservice design architecture</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End What Makes Laravel -->
<!-- Start Laravel Development Edge -->
<section class="laravel-development-edge ang-development-edge geeko-faq">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comma_h text-center">Perks of working with Geekologix Developers</h3>
                <p class="comma_p text-center">Here is how choosing Geekologix’s .NET Development Services gives you an
                    edge.</p>
            </div>
        </div>
        <div id="accordion" class="myaccordion laravel-accordian">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne" title="Passionate Developers">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/experienced-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Passionate Developers
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Our .NET developers are highly experienced, dynamic, and creative with out of the
                                    box thinking. They turn complex issues into easy to understand designs day in and
                                    day out, providing the end users with a smooth experience in their web activity.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="Assured High Quality Deliverables">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/top-notch-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Assured High Quality Deliverables
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    The Geekologix taskforce brings rich technical expertise and high-quality to each
                                    client relationship.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree" title="Scalable and Robust Solutions">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/focus-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Scalable and Robust Solutions
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Working in partnership with our client, we develop each project with maximum
                                    scalability and usability in mind to enhance customer experience.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure" title="Client-centric Approach">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/client-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Client-centric Approach
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We work hard to exceed client expectations thereby ensuring a long-lasting
                                    partnership.Catering to clients to not just satisfy them, but make them ecstatic is
                                    our primary aim.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                    aria-controls="collapseFive" title="Affordable Solutions">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/cost-effective-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Affordable Solutions
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We deliver what others promise with on-time and affordable range.Low operating costs
                                    make the services of Geekologix very affordable in comparison with other companies.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseSix" aria-expanded="false"
                                    aria-controls="collapseSix" title="High Security">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/enhanced-geeko-faq.png"
                                            alt="">
                                    </div>
                                    High Security
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We ensure that the client security is never at risk. All our procedures guarantee
                                    100% security and no threats of data leakage.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false"
                                    aria-controls="collapseSeven" title="Complete Transparency">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/fully-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Complete Transparency
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Constant communication with our clients makes it easy for us to deliver exactly as
                                    per their need. Our transparent procedures help in building better relationships
                                    with clients.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingEight">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseEight" aria-expanded="false"
                                    aria-controls="collapseEight" title="On Time Delivery">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/ontime-geeko-faq.png"
                                            alt="">
                                    </div>
                                    On Time Delivery
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Geekologix firmly believes in time limits. All our projects have always been
                                    completed on time. Our proven track record bears testimony to this.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingNine">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseNine" aria-expanded="false"
                                    aria-controls="collapseNine" title="Product Orientation">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/flexible-geeko-faq.png"
                                            alt="">
                                    </div>
                                    Product Orientation
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Innovation, strong management and active communication during the development
                                    process makes us highly product-oriented. We provide our clients with solutions and
                                    not the problems..
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTen">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTen" aria-expanded="false"
                                    aria-controls="collapseTen" title="One-Stop Destination">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/seamless-geeko-faq.png"
                                            alt="">
                                    </div>
                                    One-Stop Destination
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Our development procedure efficiently manages all departments to produce amazing
                                    results. So no matter what your project is, we can be the one-stop solution for all
                                    your online business requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Development Edge -->
<!-- Start Tech Stack -->
<section class="our-tech-stack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Our Tech Stack</h3>
            </div>
        </div>
        <div class="tech-cards tech-tabing">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-languages-tab" data-toggle="pill"
                                href="#pills-languages" role="tab" aria-controls="pills-languages" aria-selected="true"
                                title="Languages">Languages</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-frameworks-tab" data-toggle="pill" href="#pills-frameworks"
                                role="tab" aria-controls="pills-frameworks" aria-selected="false"
                                title="Frameworks">Frameworks</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-orms-tab" data-toggle="pill" href="#pills-orms" role="tab"
                                aria-controls="pills-orms" aria-selected="false" title="ORMs">ORMs </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-databases-tab" data-toggle="pill" href="#pills-databases"
                                role="tab" aria-controls="pills-databases" aria-selected="false"
                                title="Databases">Databases </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-architecture-tab" data-toggle="pill"
                                href="#pills-architecture" role="tab" aria-controls="pills-architecture"
                                aria-selected="false" title="Architecture">Architecture </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-languages" role="tabpanel"
                            aria-labelledby="pills-languages-tab">

                            <div class="row">
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual Basic .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual Basic .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C++ .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C++ .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual J# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual J# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-frameworks" role="tabpanel"
                            aria-labelledby="pills-frameworks-tab">
                            <div class="row">
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual Basic .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual Basic .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C++ .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C++ .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual J# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual J# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-orms" role="tabpanel" aria-labelledby="pills-orms-tab">
                            <div class="row">
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual Basic .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual Basic .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C++ .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C++ .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual J# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual J# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-databases" role="tabpanel"
                            aria-labelledby="pills-databases-tab">
                            <div class="row">
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual Basic .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual Basic .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C++ .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C++ .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual J# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual J# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-architecture" role="tabpanel"
                            aria-labelledby="pills-architecture-tab">
                            <div class="row">
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual Basic .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual Basic .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual C++ .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual C++ .Net</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-6 col-lg-3">
                                    <a href="javascript:void(0)" title="Visual J# .Net">
                                        <div class="tech-card">
                                            <div class="tech-logo-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
                                                    alt="">
                                            </div>
                                            <h5>Visual J# .Net</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start Tech Stack -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Laravel Key offerings -->
<section class="laravel-key-offerings">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Key Offerings</h3>
            </div>
        </div>
        <div class="laravel-offering-cards dot-net-offering-cards">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Enterprise grade ASP.NET Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/enterprise-dot-net.jpg"
                                    alt="">
                            </div>
                            <h5>Enterprise grade ASP.NET Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Latest framework for .NET Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/latest-framwork-dot-net.jpg"
                                    alt="">
                            </div>
                            <h5>Latest framework for .NET Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Database Designing">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/database-dot-net.jpg"
                                    alt="">
                            </div>
                            <h5>Database Designing</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Desktop App Development by .NET">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/desktop-app-dot-net.jpg"
                                    alt="">
                            </div>
                            <h5>Desktop App Development by .NET</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="DevOps">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/devops-dot-net.jpg" alt="">
                            </div>
                            <h5>DevOps</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Third-party Integration & Customization">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/third-party-dot-net.jpg"
                                    alt="">
                            </div>
                            <h5>Third-party Integration & Customization</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title=".NET Application Migration with Legacy Systems">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net-application-migration.jpg"
                                    alt="">
                            </div>
                            <h5>.NET Application Migration with Legacy Systems</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="ASP.Net Web App Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/asp-dot-net-web-app.jpg"
                                    alt="">
                            </div>
                            <h5>ASP.Net Web App Development</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Key offerings -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire .NET Developers</h4>
                <h5>Geekologix is one of the globally acclaimed providers of cost-effective ASP.NET solutions. </h5>
                <p>Our engineers are well-versed with the advanced tools and technologies that transform your ideas into
                    solutions, with perfection. If you have a project idea that has potential to bring great disruptions
                    in industry tomorrow, get in touch with our innovators today.</p>
                <div class="hire-dedicated-btn">
                    <a href="<?php echo get_permalink('288'); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote"
                    title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
                <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left">
                                <?php echo do_shortcode('[contact type="office_address"]') ?>
                            </p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left" href="mailto:<?php echo do_shortcode('[contact type="
                                email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?>
                            </a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type=" skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?>
                            </a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left" href="tel:<?php echo do_shortcode('[contact type=" india mobile"]')
                                ?>">
                                India:
                                <?php echo do_shortcode('[contact type="india mobile"]') ?>
                            </a>,
                            <a href="tel:<?php echo do_shortcode('[contact type=" india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?>
                            </a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                            </a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>