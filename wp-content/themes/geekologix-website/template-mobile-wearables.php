<?php
  // Template Name:Mobile Wearables
  the_post();

  get_header(); ?>
<!-- Start section 1 -->
<section class="mobile_section">
	<div class="mobile_wearables">
		<?php include 'header2.php'; ?>
		<div class="container px-0 common_heading  detail_heading">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<h1 class="banner-heading">
						<?php 

              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
						<!--  <div id="text-type"></div> -->
					</h1>
					<p>Developing Powerful Apps for Mobile and Wearables for the smart users of today’s digital era</p>
					<?php echo the_content(); ?>
					<a href="<?php echo get_permalink(288); ?>" class="text-uppercase requst_quote common_btns"
						title="Request a quote">Request a quote</a>
					<!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
					<!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
				</div>
				<div class="col-lg-6 col-md-6">
					<img src="<?php echo get_template_directory_uri(); ?>/images/wearable-banner.png"
						class="img-fluid mx-auto banner_img">
					<!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End section 1 -->
<!-- start Section 2  Top Mobile -->
<section class="top_mobile">
	<div class="container px-0">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h4 class="text-uppercase inner_heading">Flawless Mobile App Development Services</h4>
				<p class="inner_content">
					Our team of expert developers build flawless, powerful and super adaptable IOS and Android apps
					for Smartphones and compatible with wearables to allow businesses build their own brand persona
					with technology. Our custom mobile apps are highly adaptable and our developers make sure that
					they fulfil all your requirements with the functionality and performance discussed with you.
				</p>
			</div>
		</div>
		<div class="row">
			<!-- start loop -->
			<?php
                $topMobile =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'top_mobile', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getMobileData = new WP_Query($topMobile);
                   // $count = 1;
                 
                  if($getMobileData->have_posts()) {
                  while ($getMobileData->have_posts()) : $getMobileData->the_post();
                  $get_mobile_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                  
                    $mobileimage = $get_mobile_image[0];
                    ?>
			<div class="col-lg-4 col-md-4">
				<a href="" title="<?php echo the_title(); ?>">
					<div class="development_block">
						<img src="<?php  echo $mobileimage;?>" alt="<?php echo the_title(); ?>">
						<h5><?php echo the_title(); ?></h5>
					</div>
				</a>
			</div>
			<?php
		endwhile;
	}
			?>
			<!-- 					<div class="col-lg-4">
							<div class="development_block">
					<img src="<?php  echo get_template_directory_uri() ?>/images/ios-develop.png" alt="">
					<h5 class="text-capitalize">iOS App Development</h5>
				</div>
						</div>
	<div class="col-lg-4">
		<div class="development_block">
					<img src="<?php  echo get_template_directory_uri() ?>/images/ios-develop.png" alt="">
					<h5 class="text-capitalize">Cross Platform Apps</h5>
				</div>
	</div> -->
		</div>
	</div>
</section>
<!-- End section 2  Services-->
<!-- Start Section 3 we serve -->
<section class="we_serve">
	<div class="container px-0">
		<div class="row text-center ">
			<div class="col-12">
				<h4 class="text-uppercase inner_heading">Industries we serve</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Real Estate">
					<div class="industry_block real_state_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images real_state mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Real Estate</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Education">
					<div class="industry_block education_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images education mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Education</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Healthcare">
					<div class="industry_block healthcare_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images healthcare mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Healthcare</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Finance">
					<div class="industry_block finance_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images finance mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Finance</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Automotives">
					<div class="industry_block automotives_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images automotives mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Automotives</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Gaming">
					<div class="industry_block gaming_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images gaming mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Gaming</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Event & Tickets">
					<div class="industry_block event_tickets_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png"
								alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images event_tickets mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Event & Tickets</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Travel & Tours">
					<div class="industry_block travel_tours_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images travel_tours mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Travel & Tours</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Ecommerce">
					<div class="industry_block ecommerce_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images ecommerce mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Ecommerce</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Entertainment">
					<div class="industry_block entertainment_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- 	<div class="industry_images entertainment mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Entertainment</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Food & Beverage">
					<div class="industry_block food_beverage_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png"
								alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images food_beverage mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Food & Beverage</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Fitness">
					<div class="industry_block  fitness_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- 	<div class="industry_images fitness mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Fitness</h5>
					</div>
				</a>
			</div>
			<a href="<?php echo get_permalink('272'); ?>/#contact" class="common_btns let_started text-uppercase mx-auto" title="Let's Get Started">Let's Get
				Started</a>
		</div>
	</div>
</section>
<!-- End Section 3 we serve -->
<!-- Start Section 4 building app -->
<section class="building_app">
	<div class="container px-0">
		<div class="row ">
			<div class="col-12">
				<h4 class="text-uppercase inner_heading text-center">Creative Apps with High-Performance Features</h4>
			</div>
		</div>
		<div class="row">
			<!-- start loop -->
			<?php
                $features =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'building_perfact', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getfeaturesData = new WP_Query($features);
                  if($getfeaturesData->have_posts()) {
                  while ($getfeaturesData->have_posts()) : $getfeaturesData->the_post();
                  $get_features_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                    $featuresimage = $get_features_image[0];
                    ?>
			<div class="col-lg-6 col-md-6">
				<div class="building_block">
					<img src="<?php  echo  $featuresimage;?>" alt="<?php echo the_title(); ?>">
					<h5><?php echo the_title(); ?></h5>
					<?php echo the_content(); ?>
				</div>
			</div>
			<?php endwhile; }?>
		</div>
	</div>
</section>
<!-- End Section 4 building app -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start section 6 app development -->
<section class="mobile_devlop">
	<div class="container px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="text-uppercase inner_heading text-center">Our App Development Process</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 mobile_steps pr-0">
				<div class="mobile_develops_inner">
					<img src="<?php echo get_template_directory_uri()?>/images/research.png">
					<img src="<?php echo get_template_directory_uri()?>/images/icon/shape1.png" class="shapes1 shapes">
					<img class="steps_div step_1" src="<?php echo get_template_directory_uri()?>/images/1.png">
					<div class="step_box">
						<h5>Research</h5>
						<p>Research about similar apps in the competitive market based on your app requirements.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="mobile_develops_inner">
					<img src="<?php echo get_template_directory_uri()?>/images/wireframing.png">
					<img src="<?php echo get_template_directory_uri()?>/images/icon/shape-copy.png"
						class="shapes2 shapes">
					<img class="steps_div step_2" src="<?php echo get_template_directory_uri()?>/images/2.png">
					<div class="step_box">

						<h5>Wireframing</h5>
						<p>A thorough discussion on the blueprint of your custom app, its functions and features in
							detail.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="mobile_develops_inner">

					<img src="<?php echo get_template_directory_uri()?>/images/ui-design.png" alt="ui & ux Design">
					<img src="<?php echo get_template_directory_uri()?>/images/icon/shape2.png" class="shapes3 shapes">

					<img class="steps_div step_4" src="<?php echo get_template_directory_uri()?>/images/3.png">
					<div class="step_box">
						<h5>UI/UX design</h5>
						<p>A visual dummy of your app to give you an idea of the look and feel as well as
							functionality of the final app.</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4">
				<div class="mobile_develops_inner">
					<img src="<?php echo get_template_directory_uri()?>/images/launch & maintenance.png" alt="">
					<img src="<?php echo get_template_directory_uri()?>/images/icon/shape1.png" class="shapes3 shapes">

					<img class="steps_div step_4" src="<?php echo get_template_directory_uri()?>/images/6.png">
					<div class="step_box">

						<h5>Launch & Maintenance</h5>
						<p>The final launch of the app and after-launch maintenance as per your queries and
							handling requirements.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="mobile_develops_inner">

					<img src="<?php echo get_template_directory_uri()?>/images/testing-qa.png" alt="Testing and QA">
					<img src="<?php echo get_template_directory_uri()?>/images/icon/shape-copy.png"
						class="shapes3 shapes">

					<img class="steps_div step_5" src="<?php echo get_template_directory_uri()?>/images/5.png">
					<div class="step_box">
						<h5>Testing and QA</h5>
						<p>Assessing the quality and functionality of the app, making changes and bug fixes if
							required.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="mobile_develops_inner">
					<img src="<?php echo get_template_directory_uri()?>/images/development-img.png" alt="Development">

					<img class="steps_div step_4 step_6" src="<?php echo get_template_directory_uri()?>/images/4.png">
					<div class="step_box">
						<h5>Development</h5>
						<p>A team of developers engage on building your app with high tech tools, technology and
							vast expertise.</p>
					</div>
				</div>
			</div>
			<a href="<?php echo get_permalink('272'); ?>/#contact" title="Let's Discuss Your Project" class="common_btns let_started text-uppercase mx-auto">Let's
				Discuss Your Project</a>
		</div>
	</div>
</section>
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
	<div class="container px-0">
		<div class="row">
			<h4 class="text-uppercase inner_heading mx-auto">Client's Reviews</h4>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-theme" id="client_slider">
					<!-- start loop -->
					<?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
					<div class="item">
						<div class="client_block">
							<div class="client_img">
								<img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
							</div>


							<?php echo the_content(); ?>

						</div>

					</div>

					<?php 
                    endwhile;
                } ?>

				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Section 7 client review -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- Start section 9 Technology Framework-->
<section class="technology_framework">
	<div class="container px-0">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h4 class="text-uppercase inner_heading mx-auto">Technologies & Frameworks</h4>
			</div>
		</div>
		<div class="framework-slider">
			<div class="row">
				<div class="col-12">
					<!-- start loop -->
					<div class="owl-carousel owl-theme" id="framework_slider">
						<?php
					$features =  array(
						'post_status'     => 'publish', 
						'post_type'       => 'frameworks', 
						'posts_per_page'  =>  -1,
						'order'           => 'ASC',
					  );
					  $getfeaturesData = new WP_Query($features);
					  if($getfeaturesData->have_posts()) {
					  while ($getfeaturesData->have_posts()) : $getfeaturesData->the_post();
					  $get_features_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
						$featuresimage = $get_features_image[0];
						?>
						<div class="item text-center">
							<a href="javascript:void(0)" title="<?php echo the_title(); ?>">
								<div class="framework_block">
									<img src="<?php  echo $featuresimage;?>" alt="<?php echo the_title(); ?>"
										class="mx-auto">
									<h5><?php echo the_title(); ?></h5>
								</div>
							</a>
						</div>
						<?php 
				   endwhile;
					 } ?>

					</div>
				</div>
			</div>
		</div>

	</div>
</section>
<!-- End Section 9 Technology Framework -->
<!-- start Inner footer -->
<section class="common_footer" id="contact">
	<div class="container px-0">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
				<p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7  col-md-7 pr-0">
				<div class="form_part">
					<div class="row">
						<?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
					</div>
					<!-- 		<form>
						<div class="row">
							<div class="col-lg-6">
								<label> Your Name</label>
								<input type="text" class="form-control" name="" placeholder="Johan Smith">
							</div>
							<div class="col-lg-6">
									<label>Email Adress</label>
								<input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
							</div>
							<div class="col-lg-6">
									<label>Phone Number</label>
								<input type="text" class="form-control" placeholder="91+123456789" name="">
							</div>
							<div class="col-lg-6">
									<label>I'am Interested in</label>
								<input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
							</div>
							<div class="col-lg-12">
									<label>Messge</label>
								<input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
							</div>
								<div class="col-lg-12">
							<button class="common_btns">Send Now</button>
						</div>
						</div>
						
					</form> -->
				</div>
			</div>
			<div class="col-lg-5  col-md-5 pl-0">
				<div class="right_side">
					<h5 class="text-uppercase">Reach Us</h5>
					<ul>
						<li>
							<i class="fal fa-map-marker-alt float-left"></i>
							<p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
							<div class="clearfix"></div>
						</li>
						<li>
							<i class="fal fa-envelope"></i>
							<a class="float-left"
								href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
								<?php echo do_shortcode('[contact type="email_address"]') ?></a>
							<div class="clearfix"></div>
						</li>
						<li>
							<i class="fab fa-skype"></i>
							<a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
								<?php echo do_shortcode('[contact type="skype"]') ?></a>
							<div class="clearfix"></div>
						</li>
						<li>
							<img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
								class="fa_phone">
							<a class="float-left"
								href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
								India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
							<a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
								<?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
							<br>
							USA:
							<a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
								<?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
							<div class="clearfix"></div>
						</li>
					</ul>
					<div class="offices">
						<div class="float-left">
							Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
								class="ml-2"><span class="india_div">India</span>
						</div>
						<div class="float-left usa-div">

							<img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
								class="">USA</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
	<div class="footer-overlay"></div>
	<div class="container p-0">
		<div class="row">
			<div class="col-lg-12">
				<h4 class="page-title text-uppercase text-center">
					<?php echo  $conatct_title ?>
				</h4>
			</div>
			<!-- Let's talk about you, -->
			<div class="col-lg-12 ">
				<div class="let-talk-about">
					<?php echo $conatct_content; ?>
					<!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
				</div>
			</div>
			<div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
				<div class="footer-responsive">
					<div class="float-icon">
						<img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
					</div>
					<address class="address-footer address-footer2">
						<h5>
							<?php echo do_shortcode('[contact type="office_name"]') ?>
						</h5>
						<a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
							<i class="fal fa-phone"></i>
							<?php echo do_shortcode('[contact type="india mobile"]') ?>
						</a>
						<i class="fal fa-map-marker-alt"></i>
						<p>
							<?php echo do_shortcode('[contact type="office_address"]') ?>
						</p>
						<div class="clearfix"></div>
						<a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
							<i class="fal fa-envelope"></i>
							<?php echo do_shortcode('[contact type="email_address"]') ?>
						</a>
						<a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
							<i class="fab fa-skype"></i>
							<?php echo do_shortcode('[contact type="email_address"]') ?>
						</a>
						<div class="clearfix"></div>
					</address>
					<div class="clearfix"></div>
				</div>

			</div>
			<div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
				<div class="footer-add">
					<div class="float-icon">
						<img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
					</div>
					<address class="address-footer">
						<h5>
							<?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
						</h5>
						<a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
							<img src="<?php echo get_template_directory_uri() ?>/images/call.png">
							<?php echo do_shortcode('[contact type="other_mobile2"]') ?>
						</a>
					</address>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
				<div class="footer-add">
					<div class="float-icon">
						<img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
					</div>
					<address class="address-footer">
						<h5>
							<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
						</h5>
						<a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
							<img src="<?php echo get_template_directory_uri() ?>/images/call.png">
							<?php echo do_shortcode('[contact type="other_mobile"]') ?>
						</a>
					</address>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row copy-footer">
			<div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
				<p class="copy-right">
					&copy;
					<?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
				</p>
			</div>
			<div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
				<ul>
					<li>
						<a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
							title="facebook"> <i class="fab fa-facebook-f"></i></a>
					</li>
					<!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
					<li>
						<a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
							title="linkedin"><i class="fab fa-linkedin-in"></i></a>
					</li>
					<li>
						<a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
							title="instagram"><i class="fab fa-instagram"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>