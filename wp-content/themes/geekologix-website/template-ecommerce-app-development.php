<?php 
// Template Name:E-Commerce App Development
    the_post();
    get_header();
?>
<!-- Start Hire Dedicated -->
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h1 class="banner-heading">
                        <?php 
              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>
                        Innovative Ecommerce Solutions to keep you ahead of your competitors
                    </p>
                    <?php //echo the_content(); ?>
                    <a href="<?php echo get_permalink('334'); ?>" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">Custom Ecommerce App Development Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ecommerce-development-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    Our custom ecommerce web development solutions are designed to help brands, businesses or
                    enterprises take their business online, as a digitalisation strategy or just for an additional sales
                    channel online.
                </h5>
                <p class="comman_p">
                    Our ecommerce web development solutions are focused on setting up e-shops from group, and helping
                    them elevate to heights of sales, activity and engagement on their ecommerce platforms.
                </p>
                <p class="comman_p">
                    Our web developers excel at building high quality ecommerce platforms, web, mobile or desktop, with
                    amazing user experience, seamless navigation and smooth back-end maintenance all-in-one services,
                    across industries and markets.
                </p>
                <ul class="comman_list">
                    <li>Ecommerce website design and development</li>
                    <li>Website integration with back-office processes and in-store operations</li>
                    <li>Customer experience personalization</li>
                    <li>Marketing automation</li>
                    <li>Advanced data analytics</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start Doveloper Expertise -->
<section class="doveloper-expertise geeko-ecommerce-web-app">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">
                    Special Features of Geekologix Ecommerce Solutions
                </h3>
            </div>
        </div>
        <div class="benefits-hire">
            <div class="row">
                <div class="col-lg-6">
                    <ul class="benefits-list">
                        <li>
                            <a href="javascript:void(0)" title="Smooth checkout experience">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/easy-checkout-experience-ecommerce.png"
                                        alt="">
                                    Smooth checkout experience
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Effective order management">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/efficient-order-management-ecommerce.png"
                                        alt="">
                                    Effective order management
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Easy customer management">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/powerful-customer-management-ecommerce.png"
                                        alt="">
                                    Easy customer management
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Robust category management">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/robust-category-management-ecommerce.png"
                                        alt="">
                                    Robust category management
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Easy configuration for shipping methods">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/easy-configuration-ecommerce.png"
                                        alt="">
                                    Easy configuration for shipping methods
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Simple invoice creation and management">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/simple-invoices-ecommerce.png"
                                        alt="">
                                    Simple invoice creation and management
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Admin dashboard loaded with game-changing features">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/admin-dashbored-ecommerce.png"
                                        alt="">
                                    Admin dashboard loaded with game-changing features
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/geeko-ecommerce-right.jpg"
                        class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Expertise -->
<!-- Start Laravel Development Edge -->
<!-- Start Our Key Offerings -->
<section class="our-key-offerings">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Key Offerings</h4>
            </div>
        </div>
        <div class="php-development-cards">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Custom eCommerce application development ">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/custom-ecommerce-development-offering.png"
                                    alt="">
                            </div>
                            <h5>Custom eCommerce application development </h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="eCommerce mobile app development">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ecommerce-mobile-app-development-offering.png"
                                    alt="">
                            </div>
                            <h5>eCommerce mobile app development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="B2B eCommerce solutions">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/b2b-ecommerce-solution-offering.png"
                                    alt="">
                            </div>
                            <h5>B2B eCommerce solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="B2C eCommerce solutions">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/b2c-ecommerce-solution-offering.png"
                                    alt="">
                            </div>
                            <h5>B2C eCommerce solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Custom plugin development">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/custom-plugin-development-offering.png"
                                    alt="">
                            </div>
                            <h5>Custom plugin development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Multi-vendor eCommerce solutions">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/multivendor-ecommerce-solution-offring.png"
                                    alt="">
                            </div>
                            <h5>Multi-vendor eCommerce solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="eCommerce payment gateway integration services">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ecommerce-payment-gateway-offering.png"
                                    alt="">
                            </div>
                            <h5>eCommerce payment gateway integration services</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Application support and maintenance">
                        <div class="php-development-card">
                            <div class="php-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/application-support-offering.png"
                                    alt="">
                            </div>
                            <h5>Application support and maintenance</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Key Offerings -->
<section class="laravel-development-edge wordpress-advantage java-advantage ecommerce-advantage">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comma_h text-center">Geekologix as your Ecommerce Development Partner</h3>
                <p class="comma_p text-center">Providing efficient ecommerce web development services with all the
                    latest innovative functions and features.</p>
            </div>
        </div>
        <div class="wordpress-advantage-cards">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/safety-management.png" alt="">
                        </div>
                        <h5>Flexible Hiring</h5>
                        <p>At Geekologix, you can choose who you want to work with. We allow you to choose you developer
                            or build a team of developers with our employees to help you with your ecommerce
                            development.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/flexibel-engagement-java.png"
                                alt="">
                        </div>
                        <h5>Pocket-Friendly App Development</h5>
                        <p>Geekologix is known for providing the best deals and the most affordable packages when it
                            comes to Ecommerce Web development.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/safety-management.png" alt="">
                        </div>
                        <h5>Industry Experience</h5>
                        <p>Our Ecommerce Developers have been working on various projects across industries and
                            geographical regions, allowing them a profound knowledge of ecommerce development.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">

                            <img src="<?php echo get_template_directory_uri(); ?>/images/on-time-java.png" alt="">
                        </div>
                        <h5>Creativity, Skills and Expertise</h5>
                        <p>Our ecommerce developers are passionate and creative. They bring you amazing ecommerce
                            solutions which you will like and incorporate into your business with ease and perfection.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cost-effective-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Working on prerequisites</h5>
                        <p>We don’t exceed any constraints put forth by our clients, be it time or money. We deliver all
                            our project on time and within the promised budgets.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/experienced-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Support and Maintenance</h5>
                        <p>Our team managers are always available to resolve your queries or doubts from start to end,
                            or even after the project is closed. We help you through it all until you are fully
                            satisfied.</p>
                    </div>
                </div>
                <!-- <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/client-centric-java.png"
                                alt="">
                        </div>
                        <h5>Client-centric Approach</h5>
                        <p>Our time-tested procedures and methodologies ensure that the web application fits every
                            requirement that is crucial for your business growth.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/platforms-key.png" alt="">
                        </div>
                        <h5>Flexible Engagement Models</h5>
                        <p>Team Geekologix offers tailor-made engagement models specifically designed to meet the
                            client’s diverse requirements with perfection.</p>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Development Edge -->
<!-- Start Tech Stack -->
<section class="our-tech-stack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Our Tech Stack</h3>
            </div>
        </div>
        <div class="tech-cards ecommerce-tech-card">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Shopify">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/shopify-logo.png" alt="">
                            </div>
                            <h5>Shopify</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="osCommerce">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/oscommerce-logo.png"
                                    alt="">
                            </div>
                            <h5>osCommerce</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Magento">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/magento-logo.png" alt="">
                            </div>
                            <h5>Magento</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="PrestaShop">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/prestashop-logo.png"
                                    alt="">
                            </div>
                            <h5>PrestaShop</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="WooCommerce">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/woocommerce-logo.png"
                                    alt="">
                            </div>
                            <h5>WooCommerce</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="OpenCart">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/opencart-logo.png" alt="">
                            </div>
                            <h5>OpenCart</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="nopCommerce">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/nopcommerce-logo.png"
                                    alt="">
                            </div>
                            <h5>nopCommerce</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Smartstore">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/smartstore-logo.png"
                                    alt="">
                            </div>
                            <h5>Smartstore</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start Tech Stack -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Expert Ecommerce Developers at Geekologix</h4>
                <h5>Building robust ecommerce solutions for businesses across industries</h5>
                <p>Now take your business online. Multiply your sales, showcase your products and services, and make
                    them more accessible over the internet. Make your sales channels unlimited with our ecommerce web
                    development services.</p>
                <div class="hire-dedicated-btn">
                    <a href="<?php echo get_permalink('288'); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Empowering businesses to Showcase Better and Sell More.
                </h4>
                <p class="inner_content">Collaborate with our developers and get your own custom ecommerce platform in
                    affordable prices.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>