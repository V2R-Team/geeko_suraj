<?php
  // Template Name:Hire DevOps Developer & Engineer 
  the_post();

  get_header(); ?>
<!-- Start Hire Dedicated -->
<section class="mobile_section our-portfolio hire-dedicated angularjs-bg" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <h1 class="banner-heading ">
                        Hire DevOps Developerand Engineer at Geekologix
                    </h1>
                    <p>
                        Hire expert DevOps Developer and Engineer at Geekologix to work on task automation and short
                        SOPs for your business. Providing Tailored DevOps development services across industries and
                        business categories in affordable prices.
                    </p>
                    <div class="hire-dedicated-btn">
                        <a href="<?php echo the_permalink(288) ?>" class="let-disuss-btn" title="Let's Discuss">Let's
                            Discuss</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="our-experts-form">
                        <div class="form-heading">
                            <h4>Request A Free Quote</h4>
                        </div>
                        <?php echo do_shortcode('[contact-form-7 id="357" title="REQUEST A FREE QUOTE"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Dedicated -->
<!-- Start Doveloper Hiring -->
<section class="developer-hiring">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <h2>
                    Hire DevOps Developers in India
                </h2>
                <p>
                    Ever wanted to automate long and time consuming tasks of daily routine in your business? Ever
                    wondered how you could organise manual entry details in the best readable and searchable way across
                    platforms? All you need is a team of DevOps developers to understand your processes and provide you
                    the highly customised and the most time-saving automation tools built just for you and your
                    business.

                </p>
                <p>
                    Our DevOps consultants hold experience and expertise with past and ongoing projects in various
                    industries. We design IT Tools to automate your daily tasks and shorten the long manual tasks, so
                    that you can save time and redirect it into your business growth. Our tools also work as branding
                    tools, ascending your business standards. Our apps can be hybrid and cross-platform as per your the
                    applications and requirements.
                </p>
                <div class="developer-hiring-btn">
                    <a href="<?php echo the_permalink(288) ?>" title="Consult with Angular Experts"
                        class="read_more text-uppercase">Consult with
                        Angular Experts</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="developer-hiring-blocks">
                    <div class="row">
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Budget-Friendly Pricing">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/lesser-capital-cost.jpg"
                                            alt="">
                                    </div>
                                    <h5>Budget-Friendly Pricing</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Flawless Coding">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/source-code.jpg"
                                            alt="">
                                    </div>
                                    <h5>Flawless Coding</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Complete Confidentiality">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/non-disclosure.jpg"
                                            alt="">
                                    </div>
                                    <h5>Complete Confidentiality</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="On-time Delivery">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/less-time.jpg"
                                            alt="">
                                    </div>
                                    <h5>On-time Delivery</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Hiring -->
<!-- Start Doveloper Timeing  -->
<section class="doveloper-timing">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Full-Time Hiring">
                    <div class="time-card full-time-bg">
                        <h4>Full-Time Hiring</h4>
                        <p class="min-h-38">If dedicated full-time development needed.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Part-Time Hiring">
                    <div class="time-card part-time-bg">
                        <h4>Part-Time Hiring</h4>
                        <p class="min-h-38">If required for only few hours each day.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>4 hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Hourly Hiring">
                    <div class="time-card hourly-time-bg">
                        <h4>Hourly Hiring</h4>
                        <p class="min-h-38">If required only for a few hours.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>2 hours/day, 5days/week (negotiable)</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Weekly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Contractual Hiring">
                    <div class="time-card contractual-time-bg">
                        <h4>Contractual Hiring</h4>
                        <p class="min-h-38">If required on project-basis.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly or end-term</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Timeing  -->
<!-- Start  Flutter Services  -->
<section class="flutter-services">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">
                    CustomDevOps Development Services tailored for your Business Needs
                </h3>
                <p class="comman_p text-center">
                    DevOps can change the entire scenario of the day-to-day life in your business, your corporate
                    office, or your enterprise. No matter whether yours is a small business or a large, DevOps can bring
                    ripples of productivity and standardisation into your business. Here are some technical advantages
                    of employing DevOps Development in your business:
                </p>
            </div>
        </div>
        <div class="flutter-cards devops-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Simpler, Fast& Automated Processes">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/high-speed-icon.png"
                                    alt="">
                            </div>
                            <h5 class="">Simpler, Fast& Automated Processes</h5>
                            <p class="">
                                DevOps can automate your processes. Manual operations can be shortened and made faster
                                with the help of various web and app development features, such as filterable lists,
                                carousels, templates, etc.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Higher Productivity">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/rapid-deployment-icon.png"
                                    alt="">
                            </div>
                            <h5 class="">Higher Productivity</h5>
                            <p class="">
                                Fast processes and short SOPs allow more space for high productivity and happier teams
                                as the manual work gets reduced at various stages of your business, allowing you to
                                release more deliverables each day.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Scalability and Reliability">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/scalability-icon.png"
                                    alt="">
                            </div>
                            <h5 class="">Reliability and Scalability</h5>
                            <p class="">
                                DevOps makes your data more reliable as there are low chances of human errors. While you
                                can read the data more properly, it allows you to plan and scale your business further.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Standard and Organisational">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/security-icon.png" alt="">
                            </div>
                            <h5 class="">Standard and Organisational</h5>
                            <p class="">
                                With DevOps, your data is more organised and available in a single space, which can be
                                browsed across various platforms. Your data also becomes standardised as DevOps allows
                                you to put a filter on the data entered into your systems.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Higher Security and Backup">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/effective-icon.png" alt="">
                            </div>
                            <h5 class="">Higher Security and Backup</h5>
                            <p class="">
                                With DevOps, your processes are safe and secure, there are no breakdowns due to external
                                circumstances and no data leaks. It is possible to keep a backup file, unlike manual
                                entry books.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Save Time, Money and Space">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/reliability-icon.png"
                                    alt="">
                            </div>
                            <h5 class="">Save Time, Money and Space</h5>
                            <p class="">
                                With so many advantages, DevOps can help you to save a lot of time and money on a daily
                                basis. Your data remains on the cloud or your system, helping you to save both money and
                                physical storage space.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
</section>
<!-- End  Flutter Services  -->
<!-- Start Hire Flutter -->
<section class="hire-flutter trusted-devops">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">
                    Custom DevOps Development Services in India at Geekologix
                </h4>
            </div>
        </div>
        <div class="flutter-ninehertz-cards">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="DevOps as a Services (DaaS)">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/loop.png" alt="">
                            </div>
                            <h5>DevOps as a Services (DaaS)</h5>
                            <p>
                                DevOps as a service are not only capable to develop software but also to speed up the
                                delivery. For effective implementation, your business needs a DevOps managed service
                                provider for configuring the web hosting applications.

                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Cloud Consulting Services">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/cloud-consulting.png"
                                    alt="">
                            </div>
                            <h5>Cloud Consulting Services</h5>
                            <p>
                                We offer profound DevOps solutions that enhance that fasten the development of an
                                application. Our team of DevOps engineers has vast experience in Virtualization, Server
                                Support, Server Security, Containerization, and Server Orchestration.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Enterprise DevOps Solutions">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/enterprise-devops.png"
                                    alt="">
                            </div>
                            <h5>Enterprise DevOps Solutions</h5>
                            <p>
                                Enterprise needs to stimulate digital transformation initiatives for enterprise agility
                                and Continuous Experimentation. We offer Enterprise DevOps Solutions and provide
                                Assessment to enterprises to improve their Software delivery Cycle, Automation,
                                visibility, and Analytics.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="DevOps PAAS">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/devops-paas.png" alt="">
                            </div>
                            <h5>DevOps PAAS</h5>
                            <p>
                                The tools and mid-tier services required for agile development are used by the team
                                which adds value to the project. The cloud is the middle ground that occupies the space
                                between IaaS and SaaS.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="End-to-End Deployment Automation">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/end-to-end.png" alt="">
                            </div>
                            <h5>End-to-End Deployment Automation</h5>
                            <p>
                                We help our clients in automating the development and deployment life cycle. Our DevOps
                                engineers automate, manage, and deploy complex infrastructures and solutions to satisfy
                                our global clients.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Infrastructure Automation">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/infrastructure.png" alt="">
                            </div>
                            <h5>Infrastructure Automation</h5>
                            <p>
                                Being fast is the only way to stay ahead in the business as it enhances growth rates.
                                Implementing automation solutions amplifies application development with minimal
                                functional faults.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Safety Management & Monitoring">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/safety-management.png"
                                    alt="">
                            </div>
                            <h5>Safety Management & Monitoring</h5>
                            <p>
                                Our purpose is to help you build, deploy, arrange, and maintain service-orientated
                                applications. Deployment from development to production needs to be automated. We help
                                you to deliver all safety measures and monitor your work as per the documents. We never
                                keep the customer waiting and generating dissatisfaction.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Continuous Integration and Configuration Management">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/continuous-integration.png"
                                    alt="">
                            </div>
                            <h5>Continuous Integration and Configuration Management</h5>
                            <p>
                                Detecting errors at an early stage can save a lot of development time. Continuous
                                integration offers a consistent verification of the product with the help of an
                                automated build. We offer continuous integration and configuration of management with
                                the help of Chef.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="DevOps Testing and Security">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/devops-testing.png" alt="">
                            </div>
                            <h5>DevOps Testing and Security</h5>
                            <p>
                                Our DevOps developers and testing team work together to deliver high-quality and
                                hassle-free apps. We test every product on diverse platforms to offer a smooth launch of
                                the product on every software and hardware. From load testing to security layers, we
                                implement the best solutions depending on the business requirements.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Flutter -->
<!-- Start Left Right Content -->
<section class="left-right-content">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-6">
                <h4>AWS DevOps Integration</h4>
                <p>
                    DevOps is the combination of cultural philosophies, practices, and tools that increases an
                    organization’s ability to deliver applications and services at high velocity: evolving and improving
                    products at a faster pace than organizations using traditional software development and
                    infrastructure
                    management processes. This speed enables organizations to better serve their customers and compete
                    more
                    effectively in the market.
                </p>
                <p>
                    Continuous DevOps AWS integration is a software development practice where developers regularly
                    merge
                    their code changes into a central repository, after which automated builds and tests are run. The
                    key
                    goals of continuous integration with an AWS certified DevOps engineer are to find and address bugs
                    quicker, improve software quality, and reduce the time it takes to validate and release new software
                    updates. Contact our AWS DevOps professional today.
                </p>
            </div>
            <div class="col-lg-6">
                <div class="right-content-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/aws-devops-integration.jpg" alt=""
                        class="img-fluid">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="left-content-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/agile-development.jpg" alt=""
                        class="img-fluid">
                </div>
            </div>
            <div class="col-lg-6">
                <h4>Agile Development and DevOps</h4>
                <p>
                    In recent years, application development and deployment have become an increasingly critical part of
                    business operations. Because of this, various entities have sought to optimize their product
                    development
                    process. This has led to a rise in the popularity of agile development and DevOps, which is designed
                    for
                    that purpose. In simple words, the DevOps and agile development application during the software
                    development process reduces the number of steps necessary to bring software to market. These faster
                    releases and streamlined processes mean swift user feedback.
                </p>
                <p>
                    The DevOps agile development implementation focuses also on the software scalability, how well it
                    could be deployed, and also its monitoring and maintenance after the future releases. However, there
                    is
                    also a downside to traditional DevOps agile software development benefits. The system does not
                    include the
                    kind of continuous testing and improvement that Agile offers. Know more about agile development vs
                    DevOps
                    with our experts.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Left Right Content -->
<!-- Start Angular Steps -->
<!-- Start Angular Steps -->
<section class="angular-steps">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire DevOps Developers in 5 Simple Steps</h4>
                <p class="comman_p text-center">We value your time and money. Hiring developers at Geekologix is easy
                    and fast. We make sure that our services match your project requirements.</p>
            </div>
        </div>
        <div class="ang-step-cards">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="step-card-list">
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="Request Hiring">
                                <h5>01</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-01.png" alt="">
                                </div>
                                <h6>Request Hiring</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="View and Select Developers">
                                <h5>02</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-02.png" alt="">
                                </div>
                                <h6>View and Select Developers</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="Interview and Approve">
                                <h5>03</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-03.png" alt="">
                                </div>
                                <h6>Interview and Approve</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Get the best Deal for your Project Terms">
                                <h5>04</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-04.png" alt="">
                                </div>
                                <h6>Get the best Deal for your Project Terms</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Start your Development with our team">
                                <h5>05</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-05.png" alt="">
                                </div>
                                <h6>Start your Development with our team</h6>
                            </a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Angular Steps -->
<!-- End Angular Steps -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire DevOps Developers on the basis of Work Location</h4>
            </div>
        </div>
        <div class="experts-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Site">
                        <div class="experts-card">
                            <div class="expert-img on-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/on-site.png" alt="">
                            </div>
                            <h5>ON-SITE</h5>
                            <p>Hire DevOps Developers to work with you at your office or preferred Job Location, so that
                                you can work with more transparency and speed.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Off-Site">
                        <div class="experts-card">
                            <div class="expert-img off-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/off-site.png" alt="">
                            </div>
                            <h5>OFF-SITE</h5>
                            <p>Hire DevOps Developers to work from our development office at your preferred time. We
                                ensure full transparency, total reporting and on-time delivery.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hybrid">
                        <div class="experts-card">
                            <div class="expert-img hybrid-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/hybrid.png" alt="">
                            </div>
                            <h5>Hybrid</h5>
                            <p>Hire DevOps developers who are ready to work at your flexibility. Get a mix of the onsite
                                and offsite working to match your requirements.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Why To Hire -->
<section class="why-to-hire">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">
                    Benefits of Choosing Geekologix as your DevOps Development Partner
                </h4>
            </div>
        </div>
        <div class="hire-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Passionate DevOps Developers">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                1
                            </div>
                            <h5>Passionate DevOps Developers</h5>
                            <p>
                                At Geekologix, We hire coders and programmers who are really passionate about what they
                                do and how they do it. They come up with the most creative and quick solutions for each
                                project, and it is a pleasant sight to see them bringing your idea into reality.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Dedicated and Diligent Team">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                2
                            </div>
                            <h5>Dedicated and Diligent Team</h5>
                            <p>
                                An advantage of hiring passionate developers is that they work with complete honesty and
                                discipline to their work. Our teams are very dedicated and work diligently on the task
                                assigned to them, to turn it in as per the given deadline.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Experienced Team Managers">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                3
                            </div>
                            <h5>Experienced Team Managers</h5>
                            <p>
                                Geekologix Teams are hierarchy based. They are headed by experienced team managers who
                                schedule and monitor each task as well as double check it before delivery. Such team
                                managers enable our teams to work on big projects smoothly.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Project Transparency">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                4
                            </div>
                            <h5>Project Transparency</h5>
                            <p>
                                Projects at Geekologix are carried out with full transparency with the clients. Our team
                                managers will be eager to provide you with all the minute details of your project, with
                                accurate task completion schedules and project status for your satisfaction.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Live Reporting">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                5
                            </div>
                            <h5>Live Reporting</h5>
                            <p>
                                Along with the transparency schedules, our team are also used to for live reporting
                                projects. We give our clients the liberty to ask for current project status or put any
                                live query whenever they want, and always revert to them with sincerity.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="No Hidden Charges">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                6
                            </div>
                            <h5>No Hidden Charges</h5>
                            <p>
                                We do not put any hidden charges in the projects during the billing process. We make
                                sure that everything is discussed at the commencement of the project and followed
                                throughout as per your considerations, on your terms and conditions.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="100% Confidentiality">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                7
                            </div>
                            <h5>100% Confidentiality</h5>
                            <p>
                                For all our projects, we maintain complete confidentiality on our ends. We ensure that
                                there are no data leaks. No information goes out from our development centre without the
                                instructions from our clients.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Secured DevOps Web/App Development">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                8
                            </div>
                            <h5>Secured DevOps Web/App Development</h5>
                            <p>
                                All the apps and websites built at our development centres are completely secure and
                                free of data leaks. Our teams are trained about the critical importance of
                                confidentiality during the development process and abide by it.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Time Project Delivery">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                9
                            </div>
                            <h5>On-Time Project Delivery</h5>
                            <p>
                                We value our project delivery promises. Our development teams are true to their words.
                                They ensure that the projects are finished within the promised deadlines through
                                thorough planning and scheduling.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Best Pricing Deals">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                10
                            </div>
                            <h5>Best Pricing Deals</h5>
                            <p>
                                At Geekologix, you can get your requirements fulfilled in the most affordable and fairly
                                priced deals with assured project quality. We deliver what we promise, and we charge
                                fairly keeping in mind your project requirements.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Thorough Testing Before Delivery">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                11
                            </div>
                            <h5>Thorough Testing Before Delivery</h5>
                            <p>
                                Before handing over each project to our clients, we run various testing rounds to make
                                sure that everything is in place and working exactly how you expect it to work. We leave
                                no scope for no complaints after the project delivery.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Queries, Bug-fixes, and Maintenance">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                12
                            </div>
                            <h5>Queries, Bug-fixes, and Maintenance</h5>
                            <p>
                                Even after the completion of the projects, we make sure that our clients do not face any
                                difficulties in handling the project. We are glad to solve their queries, bugs and
                                maintenance issues even after the official project closure.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Why To Hire -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Flexible DevOps Developers at Geekologix</h4>
            </div>
        </div>
        <div class="experts-cards flexible-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Fixed Charge hiring for Start-ups">
                        <div class="experts-card">
                            <div class="expert-img fixed-rate-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Fixed Charge hiring for Start-ups</h5>
                            <p>If you are looking to hire developers for your start-up, we suggest you to convey your
                                requirements to us, so that we can provide you with the best possible fixed price
                                quotation which suits your business venture.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Dedicated team hiring for Enterprises">
                        <div class="experts-card">
                            <div class="expert-img dedicated-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Dedicated team hiring for Enterprises</h5>
                            <p>If you are looking to hire for your enterprise, it is better to hire dedicated teams to
                                work for you with full attention, giving your project more time on a day-to-day basis.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hourly hiring for Entrepreneurs">
                        <div class="experts-card">
                            <div class="expert-img hourly-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Hourly hiring for Entrepreneurs</h5>
                            <p>If you are looking for developers to work on your individual or personal ventures, it is
                                best to hire developers on a hourly basis which would prove to be more pocket-friendly
                                for you, while fulfilling all your development needs.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                        $testimonialSlider =  array(
                            'post_status'    => 'publish', 
                            'post_type'      => 'client_slider', 
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                          );
                          $getTestimonialData = new WP_Query($testimonialSlider);
                          if($getTestimonialData ->have_posts()) {
                          while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                          $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                            $testimonial_img = $testimonialimg[0];
                        ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                        endwhile;
                    } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Faq  -->
<section class="faq">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Frequently Asked Question</h4>
            </div>
            <div class="col-lg-7">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne"
                                    title="How can I hire a DevOpsDeveloper/Developers according to my preferences?">
                                    How can I hire a DevOpsDeveloper/Developers according to my preferences?
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    When you hire developers at Geekologix, you have the liberty to choose DevOps
                                    Developers according to your preferences and requirements. You can also choose your
                                    own team. We help you to browse through the resumes of our developers and also
                                    interview them, if required, so that you hire the exact people you want to work
                                    with.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="What are your pricing specifications?">
                                    What are your pricing specifications?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We provide you the pricing details customised for your specific project right at the
                                    beginning, to allow you to understand our charges in detail as well as manage your
                                    budget by adding or decreasing your project specifications.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree"
                                    title="How long will it take to finish my DevOps project?">
                                    How long will it take to finish my DevOps project?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    At the beginning of each project, we ensure that the project timelines and finishing
                                    deadlines are conveyed to our clients, along with all the details and documentation.
                                    We ensure the fastest delivery with best-in-class services.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure"
                                    title="What is the process for hiring React Native developers at Geekologix?">
                                    What is the process for hiring React Native developers at Geekologix?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    You can hire DevOps Developers at Geekologix in 3 easy steps:
                                </p>
                                <ul class="comman_list">
                                    <li>
                                        1 Request Hiring by conveying your requirements
                                    </li>
                                    <li>
                                        View, Select and Interview DevOps Developers
                                    </li>
                                    <li>
                                        Sign the Deal and Start Development instantly
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="faq-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/frequently-question.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Faq  -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>