<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package geekologix_website
 */

get_header();
the_post();
?>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php echo the_content(); ?>
		</div>
	</div>
</div>



<?php get_footer(); ?>