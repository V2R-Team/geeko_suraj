<?php 
// Template Name: Java Development
the_post();
get_header();
?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h1 class="banner-heading">
                        <?php 
              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>
                        Revolutionary technology to sky-rocket your business!
                    </p>
                    <?php //echo the_content(); ?>
                    <a href="<?php echo get_permalink('330') ?>/#showcase" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">Java Development Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/java-development-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    JAVA is dominating the app development industry and has become the leading choice of businesses.
                </h5>
                <p class="comman_p">
                    Geekologix is a leading Java App Development Company renowned for its state of art Java
                    Applications. Whether you want to build a new Java Application from Scratch, migrate your existing
                    application to Java, or simply need to add new modules to your software.We utilize both cutting-edge
                    and time-proven Java releases that offer scalable, flexible, reliable, and highly compatible
                    Java-based app solutions.
                </p>
                <p class="comman_p">
                    At Geekologix, we have a highly experienced and skilled Java Developer’s team that is committed to
                    offer faster enterprise Java app development.
                </p>
                <p class="comman_p">
                    We are the most preferred Java development service provider offering exceptional Java/J2EE
                    programming services to suit the mobile and web application development requirements of our clients.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start Java Best Web App -->
<section class="java-best-app">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">
                    Why choose Java for your Web App?
                </h3>
            </div>
        </div>
        <div class="java-web-app-content">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="java-web-app-card text-center">
                        <div class="single-line line-01"></div>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/location-01.png" alt="">
                        <h5>Platform Independent</h5>
                    </div>
                    <div class="java-web-app-card text-center">
                        <div class="single-line line-02"></div>
                        <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/line-02-new.png" alt=""
                            class="line-java line-02"> -->
                        <img src="<?php echo get_template_directory_uri(); ?>/images/location-02.png" alt="">
                        <h5>Highly Secure</h5>
                    </div>
                    <div class="java-web-app-card text-center">
                        <div class="single-line line-03"></div>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/location-03.png" alt="">
                        <h5>Rich set of APIs</h5>
                    </div>
                </div>
                <div class="col-lg-4 d-none d-lg-block text-center">
                    <div class="java-center-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-center-img-1.png" alt="">
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="java-web-app-card text-center">
                        <div class="single-line line-04"></div>
                        <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/line-04-new.png" alt=""
                            class="line-java line-04"> -->
                        <img src="<?php echo get_template_directory_uri(); ?>/images/location-04.png" alt="">
                        <h5>Supports Multi-Threading</h5>
                    </div>
                    <div class="java-web-app-card text-center">
                        <div class="single-line line-05"></div>
                        <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/line-05-new.png" alt=""
                            class="line-java line-05"> -->
                        <img src="<?php echo get_template_directory_uri(); ?>/images/location-05.png" alt="">
                        <h5>Scalable</h5>
                    </div>
                    <div class="java-web-app-card text-center">
                        <div class="single-line line-06"></div>
                        <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/line-06-new.png" alt=""
                            class="line-java line-06"> -->
                        <img src="<?php echo get_template_directory_uri(); ?>/images/location-06.png" alt="">
                        <h5>Budget Friendly</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Java Best Web App -->
<!-- Start Laravel Development Edge -->
<section class="laravel-development-edge wordpress-advantage java-advantage">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comma_h text-center">Perks of Working with Geekologix </h3>
                <p class="comma_p text-center">Here is how choosing Geekologix for Java App Development Services gives you an edge.</p>
            </div>
        </div>
        <div class="wordpress-advantage-cards">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/platforms-key.png" alt="">
                        </div>
                        <h5>Complete Transparency</h5>
                        <p>Our clients are informed and updated at every step of the process to keep them aware of the
                            progress of their project.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/platforms-key.png" alt="">
                        </div>
                        <h5>Client-Centric Approach</h5>
                        <p>Our team of skilled Java Developers is highly committed to providing the clients with
                            ultimate satisfaction regarding their projects with us</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/on-time-java.png" alt="">
                        </div>
                        <h5>On Time, Everytime</h5>
                        <p>We are very punctual with our work. We strive at delivering our work before the deadline,
                            always!</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cost-effective-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Affordable Solutions</h5>
                        <p>We strive at providing the best services at the most competitive and affordable prices in the
                            market without compromising with quality.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/client-centric-java.png"
                                alt="">
                        </div>
                        <h5>Client Satisfaction</h5>
                        <p>The ultimate aim of our company is client-satisfaction. We ensure that the client is not just
                            satisfied, but ecstatic with our services.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/experienced-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Certified Developers</h5>
                        <p>Our Java developers are highly skilled and have many years of industry experience which help
                            us in providing the best services to our clients.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/flexibel-engagement-java.png"
                                alt="">
                        </div>
                        <h5>Flexible Working</h5>
                        <p>We believe that our partnership should be flexible and adaptable thereby creating a win-win
                            relationship for both parties.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/safety-management.png" alt="">
                        </div>
                        <h5>High Quality Deliverables</h5>
                        <p>We deliver nothing but the best to our clients. Providing state-of-the-art applications is
                            our first priority.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Development Edge -->
<!-- Start Platforms Industries -->
<section class="platforms-industries">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Domain Expertise</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Real Estate">
                    <div class="industry_block real_state_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images real_state mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Real Estate</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Education">
                    <div class="industry_block education_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images education mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Education</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Healthcare">
                    <div class="industry_block healthcare_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images healthcare mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Healthcare</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Finance">
                    <div class="industry_block finance_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images finance mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Finance</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Automotives">
                    <div class="industry_block automotives_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images automotives mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Automotives</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Gaming">
                    <div class="industry_block gaming_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images gaming mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Gaming</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Event & Tickets">
                    <div class="industry_block event_tickets_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png"
                                alt="" class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images event_tickets mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Event & Tickets</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Travel & Tours">
                    <div class="industry_block travel_tours_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images travel_tours mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Travel & Tours</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Ecommerce">
                    <div class="industry_block ecommerce_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images ecommerce mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Ecommerce</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Entertainment">
                    <div class="industry_block entertainment_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- 	<div class="industry_images entertainment mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Entertainment</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Food & Beverage">
                    <div class="industry_block food_beverage_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png"
                                alt="" class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images food_beverage mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Food & Beverage</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Fitness">
                    <div class="industry_block  fitness_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- 	<div class="industry_images fitness mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Fitness</h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Platforms Industries -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Tech Stack -->
<section class="our-tech-stack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Our Tech Stack</h3>
            </div>
        </div>
        <div class="tech-cards tech-tabing-java">
            <div class="row">
                <div class="col-lg-4">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-framwork-tab" data-toggle="pill" href="#v-pills-framwork"
                            role="tab" aria-controls="v-pills-framwork" aria-selected="true"
                            title="Frameworks">Frameworks
                            <div class="active-border"></div>
                        </a>
                        <a class="nav-link" id="v-pills-tools-tab" data-toggle="pill" href="#v-pills-tools" role="tab"
                            aria-controls="v-pills-tools" aria-selected="false" title="IDE tools">IDE
                            tools
                            <div class="active-border"></div></a>
                        <a class="nav-link" id="v-pills-jtwojee-tab" data-toggle="pill" href="#v-pills-jtwojee"
                            role="tab" aria-controls="v-pills-jtwojee" aria-selected="false"
                            title="j2EE Application Server">j2EE Application
                            Server
                            <div class="active-border"></div>
                        </a>
                        <a class="nav-link" id="v-pills-web-tab" data-toggle="pill" href="#v-pills-web" role="tab"
                            aria-controls="v-pills-web" aria-selected="false" title="Web and User Interface">Web and
                            User Interface
                            <div class="active-border"></div>
                        </a>
                        <a class="nav-link" id="v-pills-database-tab" data-toggle="pill" href="#v-pills-database"
                            role="tab" aria-controls="v-pills-database" aria-selected="false"
                            title="Database and ORM Tools">Database and ORM Tools
                            <div class="active-border"></div>
                        </a>
                        <a class="nav-link" id="v-pills-clude-tab" data-toggle="pill" href="#v-pills-clude" role="tab"
                            aria-controls="v-pills-clude" aria-selected="false" title="Cloud">Cloud
                            <div class="active-border"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-framwork" role="tabpanel"
                            aria-labelledby="v-pills-framwork-tab">
                            <div class="java-tect-cards">
                                <!--Start Row  -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Spring boot & Spring suit">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/spring-boot.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Spring boot & Spring suit</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Hibernate">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/hybernate.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Hibernate</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="JHipster">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/jhipster.jpg"
                                                        alt="">
                                                </div>
                                                <h5>JHipster</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Struts">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/struts.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Struts</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Wicket">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-wicket.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Wicket</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Camel">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-camel.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Camel</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!--End Row -->
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-tools" role="tabpanel"
                            aria-labelledby="v-pills-tools-tab">
                            <div class="java-tect-cards">
                                <!--Start Row  -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Spring boot & Spring suit">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/spring-boot.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Spring boot & Spring suit</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Hibernate">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/hybernate.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Hibernate</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="JHipster">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/jhipster.jpg"
                                                        alt="">
                                                </div>
                                                <h5>JHipster</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Struts">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/struts.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Struts</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Wicket">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-wicket.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Wicket</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Camel">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-camel.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Camel</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!--End Row -->
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-jtwojee" role="tabpanel"
                            aria-labelledby="v-pills-jtwojee-tab">
                            <div class="java-tect-cards">
                                <!--Start Row  -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Spring boot & Spring suit">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/spring-boot.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Spring boot & Spring suit</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Hibernate">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/hybernate.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Hibernate</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="JHipster">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/jhipster.jpg"
                                                        alt="">
                                                </div>
                                                <h5>JHipster</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Struts">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/struts.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Struts</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Wicket">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-wicket.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Wicket</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Camel">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-camel.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Camel</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!--End Row -->
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-web" role="tabpanel" aria-labelledby="v-pills-web-tab">
                            <div class="java-tect-cards">
                                <!--Start Row  -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Spring boot & Spring suit">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/spring-boot.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Spring boot & Spring suit</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Hibernate">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/hybernate.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Hibernate</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="JHipster">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/jhipster.jpg"
                                                        alt="">
                                                </div>
                                                <h5>JHipster</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Struts">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/struts.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Struts</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Wicket">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-wicket.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Wicket</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Camel">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-camel.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Camel</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!--End Row -->
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-clude" role="tabpanel"
                            aria-labelledby="v-pills-clude-tab">
                            <div class="java-tect-cards">
                                <!--Start Row  -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Spring boot & Spring suit">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/spring-boot.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Spring boot & Spring suit</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Hibernate">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/hybernate.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Hibernate</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="JHipster">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/jhipster.jpg"
                                                        alt="">
                                                </div>
                                                <h5>JHipster</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Struts">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/struts.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Struts</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Wicket">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-wicket.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Wicket</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <a href="javascript:void(0)" title="Apache Camel">
                                            <div class="java-tech-card">
                                                <div class="java-tech-card-img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apache-camel.jpg"
                                                        alt="">
                                                </div>
                                                <h5>Apache Camel</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!--End Row -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Tech Stack -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Java Developers</h4>
                <h5>Geekologix is one of the globally acclaimed providers of cost-effective Java solutions.</h5>
                <p>Our Java Developers are highly experienced and well-versed with the advanced tools and technologies
                    that transform your ideas into solutions, with utter perfection.</p>
                <div class="hire-dedicated-btn">
                    <a href="<?php echo get_permalink('288'); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
                <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>