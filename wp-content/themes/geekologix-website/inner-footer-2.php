
</body>

</html>

<style>
    .wpcf7-not-valid-tip {
        display:none !important;
    }
    .error {
        color:red !important;
    }
    .wpcf7 form.invalid .wpcf7-response-output, .wpcf7 form.unaccepted .wpcf7-response-output {
        border:0px !important;
    }
    .wpcf7 form .wpcf7-response-output{
        margin:0px !important;
    }
</style>
<?php echo wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.3/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custome.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

<script>
$(function() {
  $(".form_part .wpcf7-form").validate({
    rules: {
      username: "required",
       email: {
        required: true,
        email: true
      },
        mobile: {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10
      },
      message: "required"
     
    },
    messages: {
      username: "Please enter your name",
      email: "Please enter a valid email address",
         mobile:
        { required: "Please enter phone no", digits: "Please enter numbers Only", minlength: "Please enter 10 digit phone no.", maxlength: "Please enter 10 digit mobile no." },
         message: "Please write message "
    },
  });
});




$(function() {
  $(".detail_heading .wpcf7-form").validate({
    rules: {
      username: "required",
       email: {
        required: true,
        email: true
      },
        mobile: {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10
      },
      message: "required"
     
    },
    messages: {
      username: "Please enter your name",
      email: "Please enter a valid email address",
         mobile:
        { required: "Please enter phone no", digits: "Please enter numbers Only", minlength: "Please enter 10 digit phone no.", maxlength: "Please enter 10 digit mobile no." },
         message: "Enter brief about your project "
    },
  });
});

$(function() {
  $(".waiting-for .wpcf7-form").validate({
    rules: {
      yourname: "required",
       emailaddress: {
        required: true,
        email: true
      },
      messages: "required"
     
    },
    messages: {
      yourname: "Please enter your name",
      emailaddress: "Please enter a valid email address",
      messages: "Please enter Message "
    },
  });
});

</script>