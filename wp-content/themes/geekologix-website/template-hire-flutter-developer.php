<?php
  // Template Name:Hire Flutter Developer
  the_post();

  get_header(); ?>
<!-- Start Hire Dedicated -->
<section class="mobile_section our-portfolio hire-dedicated angularjs-bg" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <h1 class="banner-heading ">
                        Hire Flutter Developer at Geekologix
                    </h1>
                    <p>
                        Hire expert Flutter Developers at Geekologix to lift off your next Flutter project. Providing
                        flawless Flutter App development services with assured quality and on-time delivery. Now expand
                        your team or get your Flutter development requirements fulfilled with a smooth, stress-free
                        experience at Geekologix.
                    </p>
                    <div class="hire-dedicated-btn">
                        <a href="<?php echo the_permalink(288) ?>" class="let-disuss-btn" title="Let's Discuss">Let's
                            Discuss</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="our-experts-form">
                        <div class="form-heading">
                            <h4>Request A Free Quote</h4>
                        </div>
                        <?php echo do_shortcode('[contact-form-7 id="357" title="REQUEST A FREE QUOTE"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Dedicated -->
<!-- Start Doveloper Hiring -->
<section class="developer-hiring">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <h2>Why Choose Flutter App Developers for Applications?</h2>
                <p>
                    The Flutter platform for App Development is Google’s attempt to encourage passionate developers to
                    accelerate their development processes. Being one of the best app development technology as of
                    today, Flutter supports development of both native as well as cross-platform apps. It provides
                    various advantages to the developers such as the ability to create beautiful, expressive and
                    flexible user interfaces, and accelerate the development process with the help of widgets. Moreover,
                    Flutter apps are believed to show amazing performance when it comes to native apps.

                </p>
                <div class="developer-hiring-btn">
                    <a href="<?php echo the_permalink(288) ?>" title="Let’s have a word"
                        class="read_more text-uppercase">Let’s have a
                        word</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="developer-hiring-blocks">
                    <div class="row">
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Budget-Friendly Pricing">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/lesser-capital-cost.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Budget-Friendly Pricing</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Flawless Coding">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/source-code.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Flawless Coding</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Complete Confidentiality">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/non-disclosure.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Complete Confidentiality</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="On-time Delivery">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/less-time.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>On-time Delivery</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Hiring -->
<!-- Start Doveloper Timeing  -->
<section class="doveloper-timing">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Full-Time Hiring">
                    <div class="time-card full-time-bg">
                        <h4>Full-Time Hiring</h4>
                        <p class="min-h-38">If dedicated full-time development needed.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Part-Time Hiring">
                    <div class="time-card part-time-bg">
                        <h4>Part-Time Hiring</h4>
                        <p class="min-h-38">If required for only few hours each day.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>4 hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Hourly Hiring">
                    <div class="time-card hourly-time-bg">
                        <h4>Hourly Hiring</h4>
                        <p class="min-h-38">If required only for a few hours.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>2 hours/day, 5days/week (negotiable)</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Weekly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Contractual Hiring">
                    <div class="time-card contractual-time-bg">
                        <h4>Contractual Hiring</h4>
                        <p class="min-h-38">If required on project-basis.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly or end-term</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Timeing  -->
<!-- Start  Flutter Services  -->
<section class="flutter-services">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">
                    Hire Flutter Developer for Desktop
                </h3>
                <p class="comman_p text-center">
                    Flutter can be used for developing desktop apps and web applications for various purposes,
                    encompassing all industries. Accelerate your desktop web development with our desktop flutter
                    developers.
                </p>
            </div>
        </div>
        <div class="flutter-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hire Flutter Developer for Desktop">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-desktop.png"
                                    alt="">
                            </div>
                            <h5 class="">Hire Flutter Developer for Desktop</h5>
                            <p class="">
                                Flutter can be used for developing desktop apps and web applications for various
                                purposes, encompassing all industries. Accelerate your desktop web development with our
                                desktop flutter developers.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hire Flutter Developer for Embedded Devices">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-device.png" alt="">
                            </div>
                            <h5 class="">Hire Flutter Developer for Embedded Devices</h5>
                            <p class="">
                                Flutter can be used in the development of apps for the embedded devices. This is
                                possible when the developers strategically integrate embedding APU into the smart phone
                                devices via coding.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hire Flutter Developer for Dart App Development">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-dart-app.png"
                                    alt="">
                            </div>
                            <h5 class="">Hire Flutter Developer for Dart App Development</h5>
                            <p class="">
                                A lot of developers use Dart language due to its ability to create high performance and
                                highly secure apps. Our developers are expert at using Dart with Flutter to create apps
                                for required frameworks.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hire Flutter Developer for Hybrid App Development">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-responsive.png"
                                    alt="">
                            </div>
                            <h5 class="">Hire Flutter Developer for Hybrid App Development</h5>
                            <p class="">
                                Our developers are pro at creating applications for both IOS and Android platforms on
                                various platforms, including Flutter. Flutter gives the liberty of high customisation
                                leading to client’s complete satisfaction.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hire Flutter Developer for Native App Development">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-native.png" alt="">
                            </div>
                            <h5 class="">Hire Flutter Developer for Native App Development</h5>
                            <p class="">
                                Flutter can be used for creating powerful native apps. Its widgets are incorporated with
                                all the functions which differ from platform to platform, for both IOS as well as
                                Android Apps. Hence it provides amazing native performance.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hire Flutter Developer for Web Development">
                        <div class="flutter-card">
                            <div class="flutter-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-web.png" alt="">
                            </div>
                            <h5 class="">Hire Flutter Developer for Web Development</h5>
                            <p class="">
                                Flutter is sued widely for web development due to its flexible and accelerated
                                development abilities, available for all platforms. Our developers are expert at various
                                Web Development applications using Flutter.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
</section>
<!-- End  Flutter Services  -->
<!-- Start Angular Steps -->
<section class="angular-steps">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Flutter Developers in 5 Simple Steps</h4>
                <p class="comman_p text-center">We value your time and money. Hiring developers at Geekologix is easy
                    and fast. We make sure that our services match your project requirements.</p>
            </div>
        </div>
        <div class="ang-step-cards">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="step-card-list">
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="Request Hiring">
                                <h5>01</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-01.png" alt="">
                                </div>
                                <h6>Request Hiring</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="View and Select Flutter Developer Resume">
                                <h5>02</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-02.png" alt="">
                                </div>
                                <h6>View and Select Flutter Developer Resume</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Interview and Approve Flutter Developers">
                                <h5>03</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-03.png" alt="">
                                </div>
                                <h6>Interview and Approve Flutter Developers</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Get the best Deal for your Project Terms">
                                <h5>04</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-04.png" alt="">
                                </div>
                                <h6>Get the best Deal for your Project Terms</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Start your Flutter Development with our team">
                                <h5>05</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-05.png" alt="">
                                </div>
                                <h6>Start your Flutter Development with our team</h6>
                            </a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Angular Steps -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Flutter Developers on the basis of Work Location</h4>
            </div>
        </div>
        <div class="experts-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Site">
                        <div class="experts-card">
                            <div class="expert-img on-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/on-site.png" alt="">
                            </div>
                            <h5>ON-SITE</h5>
                            <p>Hire Flutter Developers to work with you at your office or preferred Job Location, so
                                that you can work with more transparency and speed.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Off-Site">
                        <div class="experts-card">
                            <div class="expert-img off-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/off-site.png" alt="">
                            </div>
                            <h5>OFF-SITE</h5>
                            <p>Hire Flutter Developers to work from our development office at your preferred time. We
                                ensure full transparency, total reporting and on-time delivery.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hybrid">
                        <div class="experts-card">
                            <div class="expert-img hybrid-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/hybrid.png" alt="">
                            </div>
                            <h5>Hybrid</h5>
                            <p>Hire Flutter developers who are ready to work at your flexibility. Get a mix of the
                                onsite and offsite working to match your requirements.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Hire Flutter -->
<section class="hire-flutter">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">
                    Benefits of Choosing Geekologix as your Flutter Development Partner
                </h4>
            </div>
        </div>
        <div class="flutter-ninehertz-cards">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <a href="javascript:void(0)" title="Passionate FlutterDevelopers">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/loop.png" alt="">
                            </div>
                            <h5>Passionate FlutterDevelopers</h5>
                            <p>At Geekologix, We hire coders and programmers who are really passionate about what they
                                do and how they do it. They come up with the most creative and quick solutions for each
                                project, and it is a pleasant sight to see them bringing your idea into reality.</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <a href="javascript:void(0)" title="Dedicated and Diligent Team">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/loop.png" alt="">
                            </div>
                            <h5>Dedicated and Diligent Team</h5>
                            <p>An advantage of hiring passionate developers is that they work with complete honesty and
                                discipline to their work. Our teams are very dedicated and work diligently on the task
                                assigned to them, to turn it in as per the given deadline.</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <a href="javascript:void(0)" title="Experienced Team Managers">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/loop.png" alt="">
                            </div>
                            <h5>Experienced Team Managers</h5>
                            <p>Geekologix Teams are hierarchy based. They are headed by experienced team managers who
                                schedule and monitor each task as well as double check it before delivery. Such team
                                managers enable our teams to work on big projects smoothly.
                            <p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <a href="javascript:void(0)" title="Queries, Bug-fixes, and Maintenance">
                        <div class="flutter-ninehertz-card">
                            <div class="ninehertz-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/loop.png" alt="">
                            </div>
                            <h5>100% secure</h5>
                            <p>Even after the completion of the projects, we make sure that our clients do not face any
                                difficulties in handling the project. We are glad to solve their queries, bugs and
                                maintenance issues even after the official project closure.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="flutter-blocks">
            <div class="row">
                <div class="col-lg-2 col-6 col-md-4">
                    <a href="javascript:void(0)" title="Project Transparency">
                        <div class="industry_block real_state_outer text-center">
                            <div class=" mx-auto">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png"
                                    alt="" class="img_desktop">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png"
                                    alt="" class="img_hover mx-auto">
                            </div>
                            <!-- <div class="industry_images real_state mx-auto">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                    </div> -->
                            <h5 class="text-capitalize">Project Transparency</h5>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-6 col-md-4">
                    <a href="javascript:void(0)" title="No Hidden Charges">
                        <div class="industry_block education_outer text-center">
                            <div class=" mx-auto">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png"
                                    alt="" class="img_desktop">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
                                    class="img_hover mx-auto">
                            </div>
                            <!-- <div class="industry_images education mx-auto">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                    </div> -->
                            <h5 class="text-capitalize">No Hidden Charges</h5>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-6 col-md-4">
                    <a href="javascript:void(0)" title="100% Confidentiality">
                        <div class="industry_block healthcare_outer text-center">
                            <div class=" mx-auto">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
                                    class="img_desktop">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png"
                                    alt="" class="img_hover mx-auto">
                            </div>
                            <!-- <div class="industry_images healthcare mx-auto">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                    </div> -->
                            <h5 class="text-capitalize">Easy Hiring
                                Process</h5>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-6 col-md-4">
                    <a href="javascript:void(0)" title="100% Confidentiality">
                        <div class="industry_block finance_outer text-center">
                            <div class=" mx-auto">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
                                    class="img_desktop">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png"
                                    alt="" class="img_hover mx-auto">
                            </div>
                            <!-- <div class="industry_images finance mx-auto">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                    </div> -->
                            <h5 class="text-capitalize">100% Confidentiality</h5>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-6 col-md-4">
                    <a href="javascript:void(0)" title="On-Time Project Delivery">
                        <div class="industry_block automotives_outer text-center">
                            <div class=" mx-auto">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png"
                                    alt="" class="img_desktop">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png"
                                    alt="" class="img_hover mx-auto">
                            </div>
                            <!-- <div class="industry_images automotives mx-auto">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                    </div> -->
                            <h5 class="text-capitalize">On-Time Project Delivery</h5>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-6 col-md-4">
                    <a href="javascript:void(0)" title="Best Pricing Deals">
                        <div class="industry_block gaming_outer text-center">
                            <div class=" mx-auto">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
                                    class="img_desktop">
                                <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png"
                                    alt="" class="img_hover mx-auto">
                            </div>
                            <!-- <div class="industry_images gaming mx-auto">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                    </div> -->
                            <h5 class="text-capitalize">Best Pricing Deals</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Flutter -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Flexible Flutter Developers at Geekologix</h4>
            </div>
        </div>
        <div class="experts-cards flexible-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Fixed Charge hiring for Start-ups">
                        <div class="experts-card">
                            <div class="expert-img fixed-rate-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Fixed Charge hiring for Start-ups</h5>
                            <p>If you are looking to hire developers for your start-up, we suggest you to convey your
                                requirements to us, so that we can provide you with the best possible fixed price
                                quotation which suits your business venture.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Dedicated team hiring for Enterprises">
                        <div class="experts-card">
                            <div class="expert-img dedicated-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Dedicated team hiring for Enterprises</h5>
                            <p>If you are looking to hire for your enterprise, it is better to hire dedicated teams to
                                work for you with full attention, giving your project more time on a day-to-day basis.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hourly hiring for Entrepreneurs">
                        <div class="experts-card">
                            <div class="expert-img hourly-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Hourly hiring for Entrepreneurs</h5>
                            <p>If you are looking for developers to work on your individual or personal ventures, it is
                                best to hire developers on a hourly basis which would prove to be more pocket-friendly
                                for you, while fulfilling all your development needs.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                        $testimonialSlider =  array(
                            'post_status'    => 'publish', 
                            'post_type'      => 'client_slider', 
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                          );
                          $getTestimonialData = new WP_Query($testimonialSlider);
                          if($getTestimonialData ->have_posts()) {
                          while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                          $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                            $testimonial_img = $testimonialimg[0];
                        ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                        endwhile;
                    } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Faq  -->
<section class="faq">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Frequently Asked Question</h4>
            </div>
            <div class="col-lg-7">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne"
                                    title="How can I hire a Flutter Developer according to my preferences?">
                                    How can I hire a Flutter Developer according to my preferences?
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Geekologix gives you the liberty to choose the developers and create a team of
                                    developers by choosing our employees, so that you can scan and test their skills and
                                    talent yourself. In case yours is a short project, our manager assigns you the best
                                    suited developer for your specific requirements, ensuring you a full satisfaction.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="What are your pricing specification?">
                                    What are your pricing specification?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    The pricing details for your project are provided to you as soon as you convey your
                                    project details and requirements to our team manager. We provide you the best priced
                                    deals and promise you the most affordable web development team hiring.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree"
                                    title="How long will it take to finish my Flutter Project?">
                                    How long will it take to finish my Flutter Project?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Every project needs a different timeline. At the commencement of every Flutter
                                    project, we discuss your project with our Flutter Developers and provide you an
                                    estimated deadline by which we aim to finish your project. If it is not agreeable,
                                    we can negotiate it as per your requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure"
                                    title="What is the process of hiring Flutter Developers at Geekologix?">
                                    What is the process of hiring Flutter Developers at Geekologix?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    It is very easy to hire a Flutter Developer or a team of Flutter Developers at
                                    Geekologix. All you need to do is to follow these three simple steps:
                                </p>
                                <ul class="comman_list">
                                    <li>Request Hiring by conveying your requirements</li>
                                    <li>View, Select and Interview Developers</li>
                                    <li>Sign the Deal and Start Development instantly</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="faq-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/frequently-question.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Faq  -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>