<?php
  // Template Name:Contact Us
  get_header(); 
  ?>
  <style>
    .wpcf7-not-valid-tip {
        display:none !important;
    }
    .error {
        color:red !important;
            margin-bottom: 15px !important;
    }
.form_part .form-control {
   
   
    margin-bottom: 10px;
}
</style>

  
<!-- Start Our Portfolio -->
<section class="mobile_section our-portfolio contact_us">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-xl-7 col-md-7 ">
                    <h1 class="banner-heading">
                        Contact us
                    </h1>
                    <p>
                        Ask us everything and we will guide you to your dream
                    </p>
                </div>
                <div class="col-lg-6 col-xl-5">
                    <div class="contact-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/contact-left.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Portfolio -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">CONVERT YOUR IDEA INTO REALITY WITH OUR EXPERTS!</h4>
                <p class="inner_content">Let's turn the world upside down with your ideas and our innovation.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
						<div class="row">
							<div class="col-lg-6">
								<label> Your Name</label>
								<input type="text" class="form-control" name="" placeholder="Johan Smith">
							</div>
							<div class="col-lg-6">
									<label>Email Adress</label>
								<input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
							</div>
							<div class="col-lg-6">
									<label>Phone Number</label>
								<input type="text" class="form-control" placeholder="91+123456789" name="">
							</div>
							<div class="col-lg-6">
									<label>I'am Interested in</label>
								<input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
							</div>
							<div class="col-lg-12">
									<label>Messge</label>
								<input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
							</div>
								<div class="col-lg-12">
							<button class="common_btns">Send Now</button>
						</div>
						</div>
						
					</form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side contact_right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <ul class="social-icon-list">
                        <li>
                            <a href="https://www.facebook.com/geekologix" target="_blank" title="Facebook"
                                class="facebook_link">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" target="_blank" title="Google Plus" class="google_link">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" target="_blank" title="Twitter" class="twitter_link">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/geekologix" target="_blank" title="Linkedin"
                                class="linkedin_link"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/geekologix/" target="_blank" title="Instagram"
                                class="instagram_link">
                                <i class="fab fa-instagram"></i></a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!-- Start Contact Map  -->
<section class="contact-map">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <div class="map-content">
                    <div class="office_h float-left">
                        Offices:
                    </div>
                    <div class="india-c float-left">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/india.png" alt=""> India
                    </div>
                    <div class="pipeline float-left">|</div>
                    <div class="usa-c float-left">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/usa.png" alt=""> USA
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-12 text-center">
                <?php echo do_shortcode("[display-map id='400']"); ?>
                <!--<img src="<?php echo get_template_directory_uri(); ?>/images/map.jpg" alt="" class="img-fluid">-->
            </div>
        </div>
    </div>
</section>
<!-- End Contact Map  -->
<!-- Start Country Details -->
<section class="country-details">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="country-content">
                    <div class="country-flag">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/usa-flag.jpg" alt=""
                            class="img-fluid">
                    </div>
                    <h5>
                        USA
                    </h5>
                    <ul class="">
                        <li>
                            <div class="country-list-icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/addres-icon.png" alt="">
                            </div>
                            <h6>ADDRESS:</h6>
                            <p class="address-text">1st Floor, Laxmi Tower, Bhaskar Circle, Ratanada,
                                Jodhpur - Rajasthan</p>
                        </li>
                        <li>
                            <div class="country-list-icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png" alt="">
                            </div>
                            <h6>Phone:</h6>
                            <p>
                                <a href="tel:" title="+61 408 15 88 02" class="mr-1">+61 408 15 88 02</a>
                            </p>
                        </li>
                        <li>
                            <div class="country-list-icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/email-icon.png" alt="">
                            </div>
                            <h6>Email Us:</h6>
                            <p><strong>Email:</strong> <a href="mailto:info@geekologix.com"
                                    title="info@geekologix.com">info@geekologix.com</a></p>
                            <p><strong>Skype:</strong> <a href="skype:Geekologix Technologies"
                                    title="Geekologix Technologies">Geekologix Technologies</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="country-content mb-0">
                    <div class="country-flag">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/india-flag.jpg" alt=""
                            class="img-fluid">
                    </div>
                    <h5>
                        India
                    </h5>
                    <ul class="">
                        <li>
                            <div class="country-list-icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/addres-icon.png" alt="">
                            </div>
                            <h6>ADDRESS:</h6>
                            <p class="address-text">1st Floor, Laxmi Tower, Bhaskar Circle, Ratanada,
                                Jodhpur - Rajasthan</p>
                        </li>
                        <li>
                            <div class="country-list-icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png" alt="">
                            </div>
                            <h6>Phone:</h6>
                            <p><a href="tel:" title="+91 - 856 108 9567" class="mr-1">+91-856 108 9567, </a>
                                <a href="tel:" title="+91 - 978 241 9155">+91-978 241 9155</a></p>
                        </li>
                        <li>
                            <div class="country-list-icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/email-icon.png" alt="">
                            </div>
                            <h6>Email Us:</h6>
                            <p><strong>Email:</strong> <a href="mailto:info@geekologix.com"
                                    title="info@geekologix.com">info@geekologix.com</a></p>
                            <p><strong>Skype:</strong> <a href="skype:Geekologix Technologies"
                                    title="Geekologix Technologies">Geekologix Technologies</a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Start contact  section7-->

<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>