<?php 
// Template Name: Hire Dedicated Developers
  get_header();
?>
<!-- Start Hire Dedicated -->
<section class="mobile_section our-portfolio hire-dedicated" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <h1 class="banner-heading ">
                        Hire Dedicated Developers at Geekologix
                    </h1>
                    <p>
                        Hire the best web developers for your new or ongoing projects and meet your project goals
                        effortlessly.
                    </p>
                    <div class="hire-dedicated-btn">
                        <a href="<?php echo the_permalink(288) ?>" class="let-disuss-btn" title="Let's Discuss">Let's
                            Discuss</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="our-experts-form">
                        <div class="form-heading">
                            <h4>Request A Free Quote</h4>
                        </div>
                        <?php echo do_shortcode('[contact-form-7 id="357" title="REQUEST A FREE QUOTE"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Dedicated -->
<!-- Start Doveloper Hiring -->
<section class="developer-hiring">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <h2>Why should you hire Dedicated Developers for your Business? </h2>
                <p>
                    Hiring dedicated developers help you find a trustworthy technical partner for your project. You can
                    get in touch with top software development companies to leverage their expertise and association
                    with businesses across domains and sizes.
                </p>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="developer-hiring-blocks">
                    <div class="row">
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Pocket Friendly">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/lesser-capital-cost.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Pocket Friendly</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="<?php echo the_permalink(288) ?>" title="Advanced Technology">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/better-efficiency.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Advanced Technology</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Real Time Collaboration">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/high-sucess-ratio.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Real Time Collaboration</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Timely Reporting">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/improved-workforce.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Timely Reporting</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Hiring -->
<!-- Start Diverse -->
<section class="features-play-glam diverse">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="text-capitalize">Our Dedicated Developers to Fulfill all your Project Requirements</h3>
                <p>Geekologix offers an assorted range of solutions for all your requirements, irrespective of how vivid
                    or precise they are.</p>
            </div>
        </div>
    </div>
</section>
<!-- End Diverse -->
<!-- Start Mobility Solutions -->
<div class="mobility-solutions">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Hire Web Developers for Mobility Solutions</h4>
                <p>We provide you access to the best dedicated developers in the industry who ensure that you have a
                    world-class app developed for your business which can ooze out maximum profit for you.
                </p>
            </div>
        </div>
        <div class="mobility-apps-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Android Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-android.jpg" alt="">
                            Android Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="iOS Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/apple-icon.jpg" alt="">
                            iOS Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Kotlin Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-kotlin.jpg" alt="">
                            Kotlin Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="React Native Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-react.jpg" alt="">
                            React Native Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Flutter Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-flutter.jpg" alt="">
                            Flutter Developer
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h4>Hire Developers for Full-stack Web Development</h4>
                <p>Hire world-class web developers who have vivid experience in building scalable, robust, and highly
                    user-friendly web applications to cater to your tech-savvy users.</p>
            </div>
        </div>
        <div class="mobility-apps-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title=".NET Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                            .NET Developer
                        </div>
                    </a>
                </div>
                 <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Laravel Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-larawel.jpg" alt="">
                            Laravel Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Symfony Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/symphony.jpg" alt="">
                            Symfony Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Cake PHP Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cake-php.jpg" alt="">
                            Cake PHP Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="PHP Developers">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-php.jpg" alt="">
                            PHP Developers
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="React Native Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/react-native.jpg" alt="">
                            React Native Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Full Stack Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/full-stack.jpg" alt="">
                            Full Stack Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Node JS Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/js.jpg" alt="">
                            Node JS Developer
                        </div>
                    </a>
                </div>
                 <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="React JS Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-react.jpg" alt="">
                            React JS Developer
                        </div>
                    </a>
                </div>
                  <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Vue JS Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/vue-js.jpg" alt="">
                            Vue JS Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Angular Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/angular.jpg" alt="">
                            Angular Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Java Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-java.jpg" alt="">
                            Java Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Paython Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                            Paython Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="CMS Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cms.jpg" alt="">
                            CMS Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="E-Commerce Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/e-commerce.jpg" alt="">
                            E-Commerce Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="WordPress Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/wordpress.jpg" alt="">
                            WordPress Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Drupal Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/drupal.jpg" alt="">
                            Drupal Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Joomla Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/joomla.jpg" alt="">
                            Joomla Developer
                        </div>
                    </a>
                </div>
                <!--<div class="col-sm-6 col-md-4 col-lg-3">-->
                <!--    <a href="javascript:void(0)" title="Cake PHP Developer">-->
                <!--        <div class="mobility-apps">-->
                <!--            <img src="<?php echo get_template_directory_uri(); ?>/images/cake-php.jpg" alt="">-->
                <!--            Cake PHP Developer-->
                <!--        </div>-->
                <!--    </a>-->
                <!--</div>-->
                
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Golang Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/golang.jpg" alt="">
                            Golang Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Flask Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/flask.jpg" alt="">
                            Flask Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Sharepoint Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/sharepoint.jpg" alt="">
                            Sharepoint Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Sitecore Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/sitecore.jpg" alt="">
                            Sitecore Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Magento Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/magento.jpg" alt="">
                            Magento Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Sitefinity Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/sitefinity.jpg" alt="">
                            Sitefinity Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Umbraco Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/umbraco.jpg" alt="">
                            Umbraco Developer
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h4>Development with Advanced Technology</h4>
                <p>Geekologix offers a pool of skilled, talented and highly experienced dedicated developers who have
                    deep knowledge of advanced tools and technology to supplement your web development teams with their
                    expertise..</p>
            </div>
        </div>
        <div class="mobility-apps-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="IoT Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/iot.jpg" alt="">
                            IoT Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="AR/VR Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ar.jpg" alt="">
                            AR/VR Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="DevOps Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/devops.jpg" alt="">
                            DevOps Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Hadoop Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/hadoop.jpg" alt="">
                            Hadoop Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Blockchain Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/blockchain.jpg" alt="">
                            Blockchain Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Tensorflow Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tensorflow.jpg" alt="">
                            Tensorflow Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Laravel Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-dovloper.jpg" alt="">
                            Laravel Developer
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Hyperledger Developer">
                        <div class="mobility-apps">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/hyperledger.jpg" alt="">
                            Hyperledger Developer
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- End Mobility Solutions -->
<!-- Start Various Industries -->
<section class="various-industries">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Web Development for Industry Specific-Requirements</h4>
                <p>Geekologix offers an extensive range of custom-made services to meet your businesses requirements
                    across various industries.</p>
            </div>
        </div>
        <div class="various-industries-cards">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Healthcare">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/health-care-card.png"
                                    alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Healthcare</h5>
                                <p class="card-text">
                                    Reach out to the masses and give an online presence to your healthcare services.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Education">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/education-card.png" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Education</h5>
                                <p class="card-text">
                                    State-of-Art portals to take your educational services to another level.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Finance">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/finance-card.png" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Finance</h5>
                                <p class="card-text">
                                    Get a highly functional, robust, and scalable financial app for your business.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Real-Estate">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/realstate-card.png" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Real-Estate</h5>
                                <p class="card-text">
                                    Manage your properties effectively and display them with advanced web solutions.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Media & Entertainment">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/media-card.png" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Media & Entertainment</h5>
                                <p class="card-text">
                                    Build content creation & distribution platforms to boost your digital presence. .
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Automotives">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/automotive-card.png"
                                    alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Automotives</h5>
                                <p class="card-text">
                                    Display and provide your products and services across platforms and states.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Ecommerce">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/e-commerce-card.png"
                                    alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Ecommerce</h5>
                                <p class="card-text">
                                    Build an end-to-end retail app or bring your store online with the help of our
                                    experts.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Fitness">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/fitness-card.png" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Fitness</h5>
                                <p class="card-text">
                                    Empower your fitness training centre or business with custom web solutions.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <a href="javascript:void(0)" title="Travel & Tours">
                        <div class="card">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/travel-card.png" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Travel & Tours</h5>
                                <p class="card-text">
                                    Innovative and custom Display and Information solutions for travel businesses.
                                </p>
                                <a href="javascript:void(0)" class="card-read-more" title="Read More">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Various Industries -->
<!-- Start Waiting For -->
<section class="waiting-for">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-xl-10 offset-lg-1 offset-xl-1">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="waiting-for-content">
                            <h4>What are you waiting for?</h4>
                            <p>Share your imagination and get started with the project of your dreams.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="our-experts-form">
                            <?php echo do_shortcode('[contact-form-7 id="356" title="What are you waiting for?"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Waiting For -->
<!-- Start Geekologix ios -->
<section class="geekologix-ios">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Choose our Super Developers for your next Web Development Project</h4>
                <p>Our web developers are super adaptive and competitive. There are various perks of working with our
                    expert developers.

                </p>
            </div>
        </div>
        <div class="geekologix-ios-cards">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Passionate Web Developers">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/trained-card.png" alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>Passionate Web Developers</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>Passionate Web Developers</h5>
                                <p>Our developers come up with the most creative and quick solutions for each project,
                                    and it is a pleasant sight to see them bringing your idea into reality.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Dedicated and Diligent Team">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/dedicated-and-diligent-team.png" alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>Dedicated and Diligent Team</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>Dedicated and Diligent Team</h5>
                                <p>Our teams are very dedicated and work diligently on the task assigned to them, to
                                    turn it in as per the given deadline.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Project Transparency">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/need-based-card.png"
                                    alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>Project Transparency</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>Project Transparency</h5>
                                <p>Our team managers will be eager to provide you with all the minute details of your
                                    project and project status for your satisfaction.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="No Hidden Charges">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/solution-card.png" alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>No Hidden Charges</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>No Hidden Charges</h5>
                                <p>We do not put any hidden charges in the projects during the billing process. There
                                    are no extra charges other than the ones discussed.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="On-Time Project Delivery">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/technology-card.png"
                                    alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>On-Time Project Delivery</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>On-Time Project Delivery</h5>
                                <p>They ensure that the projects are finished within the promised deadlines through
                                    thorough planning and scheduling.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Best Pricing Deals">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/no-card.png" alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>Best Pricing Deals</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>Best Pricing Deals</h5>
                                <p>At Geekologix, you can get your requirements fulfilled in the most affordable and
                                    fairly priced deals with assured project quality.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Queries, Bug-fixes, and Maintenance">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/expertise-card.png" alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>Queries, Bug-fixes, and Maintenance</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>Queries, Bug-fixes, and Maintenance</h5>
                                <p>We are glad to solve their queries, bugs and maintenance issues even after the
                                    official project closure.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title=" 100% Confidentiality">
                        <div class="ios-card">
                            <div class="ios-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/comprensive-caed.png"
                                    alt="">
                            </div>
                            <div class="ios-heading">
                                <h5>100% Confidentiality</h5>
                            </div>
                            <!-- on hover show -->
                            <div class="hover-ios">
                                <h5>Easy Project Management</h5>
                                <p>For all our projects, we maintain complete confidentiality on our ends. We ensure
                                    that there are no data leaks on our end.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Geekologix ios -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                        $testimonialSlider =  array(
                            'post_status'    => 'publish', 
                            'post_type'      => 'client_slider', 
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                          );
                          $getTestimonialData = new WP_Query($testimonialSlider);
                          if($getTestimonialData ->have_posts()) {
                          while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                          $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                            $testimonial_img = $testimonialimg[0];
                        ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                        endwhile;
                    } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Faq  -->
<section class="faq">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Frequently Asked Question</h4>
            </div>
            <div class="col-lg-7">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne"
                                    title="How can I hire dedicated Web Developers according to my preferences?">
                                    How can I hire dedicated Web Developers according to my preferences?
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    At Geekologix, you only pay for what you like. At the commencement of each project,
                                    we send you resumes of our developers from which you can select and filter the ones
                                    that you want on your team. You can also interview the developers individually if
                                    you want. For short projects, our team manager himself chooses the best suited
                                    developer to work on your project, so as to save your time and efforts.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="What are your pricing specifications?">
                                    What are your pricing specifications?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Before we start working on your Web Development Project, we discuss all the pricing
                                    details with you which are based on your specific project requirements and
                                    preferences. The idea is to provide you the best deals tailored as per your
                                    individual projects.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree"
                                    title="How long will it take to finish my Web Development project?">
                                    How long will it take to finish my Web Development project?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Before our team starts working on your Web Development project, we discuss the
                                    project details and some suggested timelines with you, as per the time required by
                                    our developers to finish your project. The suggested timelines are always
                                    negotiable.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure"
                                    title="What is the process for hiring Web Developers at Geekologix?">
                                    What is the process for hiring Web Developers at Geekologix?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Geekologix is where you can hire Web Developers in minimum time and efforts. Your
                                    hiring is finished in 3 easy steps:
                                </p>
                                <ul class="comman_list">
                                    <li>Request Hiring by conveying your requirements</li>
                                    <li>View, Select and Interview Web Developers</li>
                                    <li>Sign the Deal and Start Development instantly</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="faq-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/frequently-question.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Faq  -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>