<div class="burger-search-wrapper navbar-fixed-top">
                  <div class="container">
                    <div class="hamburger-menu">
                      <div class="menu-bg" style="margin-top: 35px;"></div>
                      <div class="circle"></div>
                      <div class="menu row" style="display: none;">
                        <div
                          class="fix-menu hidden-sm hidden-xs col-md-9 col-sm-12 col-12 p0"
                        >
                          <div class="row">
                            <div
                              class="col-md-12 col-sm-12 col-12 automate h-50vh"
                            >
                              <div class="top_menu_img_wrapper">
                                <img
                                  src="<?php echo get_template_directory_uri() ?>/images/always-learning-menu.png"
                                  class="being-resilient"
                                />
                              </div>
                              <div class="row">
                                <div class="top_menu_cont_wrapper">
                                  <div class="col-md-6 col-sm-12 col-12">
                                    <a
                                      href="navigate-your-next/being-resilient.html"
                                      title="Being Resilient. That's Live Enterprise."
                                    >
                                      <h2 class="head-txt mt100">
                                        Being Resilient. That's Live Enterprise.
                                      </h2>
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div
                            class="col-md-4 col-sm-12 col-12 automate bg-img-pos h-50vh"
                          >
                            <a
                              href="navigate-your-next/digital-capabilities.html"
                              title="Digital Core Capabilities"
                            >
                              <h2 class="head-txt">
                                Digital Core Capabilities
                              </h2>
                            </a>
                          </div>
                          <div
                            class="col-md-4 col-sm-12 col-12 innovate bg-img-pos h-50vh"
                          >
                            <a
                              href="navigate-your-next/digital-operating-models.html"
                              title="Digital Operating Models"
                            >
                              <h2 class="head-txt">Digital Operating Models</h2>
                            </a>
                          </div>
                          <div
                            class="col-md-4 col-sm-12 col-12 learn bg-img-pos h-50vh"
                          >
                            <a
                              href="navigate-your-next/talent-transformations.html"
                              title="Empowering Talent Transformations"
                            >
                              <h2 class="head-txt">
                                Empowering Talent Transformations
                              </h2>
                            </a>
                          </div>
                          </div>
                          
                        </div>
                        <!-- Industries Menu -->
                        <div
                          class="industries-menu wow fadeOutRightBig animated"
                          data-wow-duration="0.3s"
                          style="
                            visibility: visible;
                            animation-duration: 0.3s;
                            display: none;
                          "
                        >
                          <div class="cross visible1024-cross">
                            <a
                              title="close"
                              href="javascript:void(0);"
                              alt="Cross"
                            >
                            <i class="fas fa-chevron-right"></i>
                            </a>
                          </div>
                          <div class="submenu-portion row">
                            <div class="col-md-12 col-sm-12 col-12">
                              <h2 class="h2-heading mb10">Industries</h2>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a
                                    title="Aerospace &amp; Defense"
                                    href="industries/aerospace-defense.html"
                                    >Aerospace &amp; Defense</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Agriculture"
                                    href="industries/agriculture.html"
                                    >Agriculture</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Automotive"
                                    href="industries/automotive.html"
                                    >Automotive</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Communication Services"
                                    href="industries/communication-services.html"
                                    >Communication Services</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Consumer Packaged Goods"
                                    href="industries/consumer-packaged-goods.html"
                                    >Consumer Packaged Goods</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Education"
                                    href="industries/education.html"
                                    >Education</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Financial Services"
                                    href="industries/financial-services.html"
                                    >Financial Services</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Healthcare"
                                    href="industries/healthcare.html"
                                    >Healthcare</a
                                  >
                                </li>
                              </ul>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a
                                    title="High Technology"
                                    href="industries/high-technology.html"
                                    >High Technology</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Industrial Manufacturing"
                                    href="industries/industrial-manufacturing.html"
                                    >Industrial Manufacturing</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Information Services &amp; Publishing"
                                    href="industries/publishing.html"
                                    >Information Services &amp; Publishing</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Insurance"
                                    href="industries/insurance.html"
                                    >Insurance</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Life Sciences"
                                    href="industries/life-sciences.html"
                                    >Life Sciences</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Logistics &amp; Distribution"
                                    href="industries/logistics-distribution.html"
                                    >Logistics &amp; Distribution</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Media and Entertainment"
                                    href="industries/media-entertainment.html"
                                    >Media and Entertainment</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Mining"
                                    href="industries/mining.html"
                                    >Mining</a
                                  >
                                </li>
                              </ul>
                            </div>
                            <div class="col-md-4 col-sm-12 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a
                                    title="Oil and Gas"
                                    href="industries/oil-and-gas.html"
                                    >Oil and Gas</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Professional Services"
                                    href="industries/professional-services.html"
                                    >Professional Services</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Public Sector"
                                    href="industries/public-sector.html"
                                    >Public Sector</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Retail"
                                    href="industries/retail.html"
                                    >Retail</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Travel and Hospitality"
                                    href="industries/travel-hospitality.html"
                                    >Travel and Hospitality</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Utilities"
                                    href="industries/utilities.html"
                                    >Utilities</a
                                  >
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <!-- Services Menu -->
                        <div
                          class="services-menu wow animated fadeOutRightBig"
                          data-wow-duration="0.3s"
                          style="
                            visibility: visible;
                            animation-duration: 0.3s;
                            display: none;
                          "
                        >
                          <div class="cross visible1024-cross">
                            <a title="close" href="javascript:void(0);">
                            <i class="fas fa-chevron-right"></i>
                            </a>
                          </div>
                          <div class="submenu-portion row">
                            <div class="col-md-12 col-sm-12 col-12">
                              <h2 class="h2-heading mb10">Services</h2>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="row">
                                <div class="col-md-3 col-sm-3 col-12">
                                <i class="fas fa-chevron-right"></i>
                                  <h3 class="hear-txt-sub">Experience</h3>
                                </div>
                                <div class="col-md-9 col-sm-9 col-12">
                                  <div class="row">
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/digital-marketing.html"
                                            title="Digital Marketing"
                                            >Digital Marketing</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/digital-commerce.html"
                                            title="Digital Commerce"
                                            >Digital Commerce</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/digital-interaction.html"
                                            title="Digital Interactions"
                                            >Digital Interactions</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="clearfix visible-sm hidden-xs"
                                    ></div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/digital-workplace-services.html"
                                            title="Digital Workplace Services"
                                            >Digital Workplace Services</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="menu-seprator"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="row">
                                <div class="col-md-3 col-sm-3 col-12">
                                  <img
                                    src="content/dam/infosys-web/burger-menu/en/images/insight.svg"
                                    alt="Insight"
                                    class=""
                                  />
                                  <h3 class="hear-txt-sub">Insight</h3>
                                </div>
                                <div
                                  class="col-md-9 col-sm-9 col-12 align-list"
                                >
                                  <div class="row">
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/ai-automation.html"
                                            title="AI &amp; Automation"
                                            >AI &amp; Automation</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline">
                                        <li>
                                          <a
                                            href="services/data-analytics.html"
                                            title="Data Analytics"
                                            >Data Analytics</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="menu-seprator"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="row">
                                <div class="col-md-3 col-sm-3 col-12">
                                <i class="fas fa-chevron-right"></i>
                                  <h3 class="hear-txt-sub">Innovate</h3>
                                </div>
                                <div class="col-md-9 col-sm-9 col-12">
                                  <div class="row">
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/blockchain.html"
                                            title="Blockchain"
                                            >Blockchain</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/engineering-services.html"
                                            title="Engineering Services"
                                            >Engineering Services</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline">
                                        <li>
                                          <a
                                            href="services/engineering-services/service-offerings/internet-of-things.html"
                                            title="Internet of Things (IoT)"
                                            >Internet of Things (IoT)</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="menu-seprator"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="row">
                                <div class="col-md-3 col-sm-3 col-12">
                                <i class="fas fa-chevron-right"></i>
                                  <h3 class="hear-txt-sub">Accelerate</h3>
                                </div>
                                <div class="col-md-9 col-sm-9 col-12">
                                  <div class="row">
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/agile-devops.html"
                                            title="Agile &amp; DevOps"
                                            >Agile &amp; DevOps</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/api-economy.html"
                                            title="API Economy &amp; Microservices"
                                            >API Economy &amp; Microservices</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/cloud.html"
                                            title="Cloud"
                                            >Cloud</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="clearfix visible-sm hidden-xs"
                                    ></div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/digital-process-automation.html"
                                            title="Digital Process Automation"
                                            >Digital Process Automation</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/modernization.html"
                                            title="Mainframe Modernization"
                                            >Mainframe Modernization</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/microsoft-cloud-business.html"
                                            title="Microsoft Cloud Business"
                                            >Microsoft Cloud Business</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="clearfix visible-sm hidden-xs"
                                    ></div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/open-source.html"
                                            title="Open Source"
                                            >Open Source</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/oracle.html"
                                            title="Oracle"
                                            >Oracle</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/sap.html"
                                            title="SAP"
                                            >SAP</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/salesforce.html"
                                            title="Salesforce"
                                            >Salesforce</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/experience-transformation.html"
                                            title="Service Experience Transformation"
                                            >Service Experience
                                            Transformation</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb0">
                                        <li>
                                          <a
                                            href="services/digital-supply-chain.html"
                                            title="Digital Supply Chain"
                                            >Digital Supply Chain</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="menu-seprator"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="row">
                                <div class="col-md-3 col-sm-3 col-12">
                                <i class="fas fa-chevron-right"></i>
                                  <h3 class="hear-txt-sub">Assure</h3>
                                </div>
                                <div class="col-md-9 col-sm-9 col-12">
                                  <div class="row">
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline mb-xs-0">
                                        <li>
                                          <a
                                            href="services/cyber-security.html"
                                            title="Cyber Security"
                                            >Cyber Security</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                    <div
                                      class="col-md-4 col-sm-4 col-12 align-list"
                                    >
                                      <ul class="list-inline">
                                        <li>
                                          <a
                                            href="services/validation-solutions.html"
                                            title="Testing"
                                            >Testing</a
                                          >
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="menu-seprator"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12 mt30">
                              <div class="row">
                                <div class="col-md-3 col-sm-12 col-12">
                                  <a
                                    href="services/application-development-maintenance.html"
                                    title="Application Development and Maintenance"
                                  >
                                  <i class="fas fa-chevron-right"></i>
                                    <h3 class="hear-txt-sub">
                                      Application Development and Maintenance
                                    </h3>
                                  </a>
                                </div>
                                <div class="col-md-3 col-sm-12 col-12">
                                  <a
                                    target="_blank"
                                    href="https://www.infosysbpm.com/Pages/index.aspx"
                                    title="Business Process Management"
                                    rel="noopener noreferrer"
                                  >
                                    <img
                                      src="content/dam/infosys-web/en/global-resource/background-image/business-management.svg"
                                      alt="Business Process Management"
                                      class=""
                                    />
                                    <h3 class="hear-txt-sub">
                                      Business Process Management
                                    </h3>
                                  </a>
                                </div>
                                <div class="col-md-3 col-sm-12 col-12">
                                  <a
                                    href="services/consulting.html"
                                    title="Consulting Services"
                                  >
                                    <img
                                      src="content/dam/infosys-web/en/global-resource/background-image/consulting.svg"
                                      alt="Consulting Services"
                                      class=""
                                    />
                                    <h3 class="hear-txt-sub">
                                      Consulting Services
                                    </h3>
                                  </a>
                                </div>
                                <div class="col-md-3 col-sm-12 col-12">
                                  <a
                                    href="services/incubating-emerging-technologies.html"
                                    title="Incubating Emerging Offerings"
                                  >
                                    <img
                                      src="content/dam/infosys-web/en/global-resource/background-image/incubating-emerging.svg"
                                      alt="Incubating Emerging Offerings"
                                      class=""
                                    />
                                    <h3 class="hear-txt-sub">
                                      Incubating Emerging Offerings
                                    </h3>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Platform Menu -->
                        <div
                          class="platforms-menu wow animated fadeOutRightBig"
                          data-wow-duration="0.3s"
                          style="
                            visibility: visible;
                            animation-duration: 0.3s;
                            display: none;
                          "
                        >
                          <div class="cross visible1024-cross">
                            <a title="close" href="javascript:void(0);">
                              <img
                                src="content/dam/infosys-web/burger-menu/en/images/multiply.svg"
                                alt="Cross"
                              />
                            </a>
                          </div>
                          <div class="submenu-portion row">
                            <div class="col-md-12 col-sm-12 col-12">
                              <h2 class="h2-heading mb10">Platforms</h2>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a
                                    href="https://www.edgeverve.com/"
                                    title="EdgeVerve"
                                    >EdgeVerve</a
                                  >
                                </li>
                                <li>
                                  <a
                                    href="https://www.edgeverve.com/finacle/"
                                    title="Infosys Finacle"
                                    >Infosys Finacle</a
                                  >
                                </li>
                                <li>
                                  <a
                                    href="navigate-your-next/live-enterprise-suite.html"
                                    title="Infosys Live Enterprise Suite"
                                    >Infosys Live Enterprise Suite</a
                                  >
                                </li>
                                <li>
                                  <a
                                    href="https://www.edgeverve.com/artificial-intelligence/nia/"
                                    title="Infosys Nia"
                                    >Infosys Nia</a
                                  >
                                </li>
                              </ul>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a
                                    href="http://www.panaya.com/"
                                    title="Panaya"
                                    >Panaya</a
                                  >
                                </li>
                                <li>
                                  <a href="http://www.skava.com/" title="Skava"
                                    >Skava</a
                                  >
                                </li>
                                <li>
                                  <a
                                    href="products-and-platforms/wingspan.html"
                                    title="Infosys Wingspan"
                                    >Infosys Wingspan</a
                                  >
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <!-- About Us -->
                        <div
                          class="aboutus-menu wow animated fadeOutRightBig"
                          data-wow-duration="0.3s"
                          style="
                            visibility: visible;
                            animation-duration: 0.3s;
                            display: none;
                          "
                        >
                          <div class="cross visible1024-cross">
                            <a title="close" href="javascript:void(0);">
                              <img
                                src="content/dam/infosys-web/burger-menu/en/images/multiply.svg"
                                alt="Cross"
                              />
                            </a>
                          </div>
                          <div class="submenu-portion row">
                            <div class="col-md-12 col-sm-12 col-12">
                              <h2 class="h2-heading mb10">About Us</h2>
                            </div>
                            <div class="col-md-4 col-sm-4 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a title="Overview" href="about.html"
                                    >Overview</a
                                  >
                                </li>
                                <li>
                                  <a title="History" href="about/history.html"
                                    >History</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Corporate Governance"
                                    href="about/corporate-governance.html"
                                    >Corporate Governance</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Innovation Fund"
                                    href="about/innovation-fund/index.html"
                                    >Innovation Fund</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Management Profiles"
                                    href="about/management-profiles.html"
                                    >Management Profiles</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Infosys Knowledge Institute"
                                    href="about/knowledge-institute.html"
                                    >Infosys Knowledge Institute</a
                                  >
                                </li>
                              </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <a
                                    title="Subsidiaries"
                                    href="about/subsidiaries.html"
                                    >Subsidiaries</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Clients Speak"
                                    href="about/clients-speak.html"
                                    >Clients Speak</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Alliances"
                                    href="about/alliances.html"
                                    >Alliances</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Sustainability"
                                    href="sustainability.html"
                                    >Sustainability</a
                                  >
                                </li>
                                <li>
                                  <a title="Awards" href="about/awards.html"
                                    >Awards</a
                                  >
                                </li>
                              </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 col-12 align-list">
                              <ul class="list-unstyled li-separator">
                                <li>
                                  <strong>Sports Partnerships</strong>
                                </li>
                                <li>
                                  <a title="ATP" href="atp.html">ATP</a>
                                </li>
                                <li>
                                  <a
                                    title="Australian Open"
                                    href="australian-open.html"
                                    >Australian Open</a
                                  >
                                </li>
                                <li>
                                  <a
                                    title="Roland-Garros"
                                    href="roland-garros.html"
                                    >Roland-Garros</a
                                  >
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <!-- Side Menu Title -->
                        <div class="col-md-3 col-sm-12 col-12 menuItems">
                          <a href="#">
                            <img
                              src="content/dam/infosys-web/burger-menu/en/images/logo.svg"
                              alt="Infosys"
                              class="img-responsive logo-inner"
                            />
                          </a>
                          <ul class="list-unstyled">
                            <li class="" title="Navigate your next">
                              <a
                                href="navigate-your-next.html"
                                title="Navigate your next"
                                class="hover-menu-hide"
                                target="_self"
                                >Navigate your next
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="" title="Industries">
                              <a
                                href="javascript:void(0)"
                                title="Industries"
                                class="industries"
                                target="_self"
                                >Industries
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="" title="Services">
                              <a
                                href="javascript:void(0)"
                                title="Services"
                                class="services"
                                target="_self"
                                >Services
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="" title="Platforms">
                              <a
                                href="javascript:void(0)"
                                title="Platforms"
                                class="platforms"
                                target="_self"
                                >Platforms
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="" title="About Us">
                              <a
                                href="javascript:void(0)"
                                title="About Us"
                                class="about-txt"
                                target="_self"
                                >About Us
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="smalltext" title="Investors">
                              <a
                                href="investors.html"
                                title="Investors"
                                class="hover-menu-hide"
                                target="_self"
                                >Investors
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="smalltext" title="Careers">
                              <a
                                href="careers.html"
                                title="Careers"
                                class="hover-menu-hide"
                                target="_self"
                                >Careers
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="smalltext" title="Newsroom">
                              <a
                                href="newsroom.html"
                                title="Newsroom"
                                class="hover-menu-hide"
                                target="_self"
                                >Newsroom
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="smalltext" title="Contact Us">
                              <a
                                href="contact.html"
                                title="Contact Us"
                                class="hover-menu-hide"
                                target="_self"
                                >Contact Us
                                <span
                                  class="un-line hidden-sm hidden-xs hidden-tab"
                                ></span>
                              </a>
                            </li>
                            <li class="social-icons" title="Linked in">
                              <a
                                href="https://www.linkedin.com/company/infosys"
                                title="Linked in"
                                class=""
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <svg width="16" height="16">
                                  <image
                                    xlink:href="content/dam/infosys-web/burger-menu/en/images/linkedin.svg"
                                    src="content/dam/infosys-web/burger-menu/en/images/linkedin.svg"
                                    width="16"
                                    height="16"
                                  ></image>
                                </svg>
                              </a>
                            </li>
                            <li class="social-icons" title="Twitter">
                              <a
                                href="https://www.twitter.com/infosys"
                                title="Twitter"
                                class=""
                                target="_self"
                              >
                                <svg width="16" height="16">
                                  <image
                                    xlink:href="content/dam/infosys-web/burger-menu/en/images/twitter.svg"
                                    src="content/dam/infosys-web/burger-menu/en/images/twitter.svg"
                                    width="16"
                                    height="16"
                                  ></image>
                                </svg>
                              </a>
                            </li>
                            <li class="social-icons" title="Facebook">
                              <a
                                href="https://www.facebook.com/Infosys"
                                title="Facebook"
                                class=""
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <svg width="16" height="16">
                                  <image
                                    xlink:href="content/dam/infosys-web/burger-menu/en/images/facebook.svg"
                                    src="content/dam/infosys-web/burger-menu/en/images/facebook.svg"
                                    width="16"
                                    height="16"
                                  ></image>
                                </svg>
                              </a>
                            </li>
                            <li class="social-icons" title="YouTube">
                              <a
                                href="https://www.youtube.com/user/Infosys"
                                title="YouTube"
                                class=""
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <svg width="16" height="16">
                                  <image
                                    xlink:href="content/dam/infosys-web/burger-menu/en/images/youtube.svg"
                                    src="content/dam/infosys-web/burger-menu/en/images/youtube.svg"
                                    width="16"
                                    height="16"
                                  ></image>
                                </svg>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div class="burger" style="margin-top: 35px;">
                        <div class="icon-bar1"></div>
                        <div class="icon-bar2" style="display: block;"></div>
                        <div class="icon-bar3"></div>
                      </div>
                    </div>
                  </div>
                </div>