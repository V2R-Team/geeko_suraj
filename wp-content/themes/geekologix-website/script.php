<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- <script src="<?php echo get_template_directory_uri() ?>/js/jquery-3.3.1.slim.min.js"></script> -->
<script src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()  ?>/js/fullpage.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/owl.carousel.min.js"></script>
<!-- <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/151801/fullpage.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        function getResolution() {
            alert("Your screen resolution is: " + screen.width + "x" + screen.height);
        }
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        })
        // // Scroll JS
        fullpage.initialize('#fullpage', {
            anchors: [
                'home-page',
                'about-us',
                'school-leader',
                'our-campus',
                'student-life',
                'students-schievements',
                'academics',
                'extra-circulum-activities',
                'admission',
                'at-the-school',
                'gallery',
                'parent-connets',
                'last-page',
                'last-page'
            ],
            menu: '#menu',
            scrollingSpeed: 1000,
            scrollingSpeed: 1000,
            scrollBar: false,

            responsiveWidth: 980,
            responsiveHeight: 500,
            lazyLoading: true,
            // showActiveTooltip: true,
            // slidesNavigation: true,
            // slidesNavPosition: 'bottom',
            // controlArrows: true,
            // css3: true
        });

        // Gallery lightbox

        // owlCarousel
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });

    });
    // Load popop data from all-modal-data.php
    $('.ajax-model').on('click', function() {
        var a = $(this).attr('data-id');
        $('#our_mission1').modal('hide');
        $('#loaders').show();
        $.ajax({
            type: 'GET',
            url: "<?php echo get_template_directory_uri() ?>/template-parts/all-modal-data.php?pagid=" + a,
            success: function(res) {

                $('#model-content-ajax').html(res);
                $('#our_mission1').modal('show');
                $('#loaders').hide();
            }
        });
        return false;

    });
</script>