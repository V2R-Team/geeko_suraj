<?php
   // Template Name:Web Development Services 
  the_post();
  get_header(); 
  ?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
  <div id="" class="mobile_wearables">
    <?php include 'header2.php'; ?>
    <div class="container px-0 common_heading  detail_heading">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h1 class="banner-heading">
            Web Development
            <?php 

             // $about_ID = 168;
             // $about_title = get_page($about_ID);
             // echo the_title();?>
            <!--  <div id="text-type"></div> -->
          </h1>
          <p>
            Providing innovative Web development services tailored for your specific business needs.
          </p>
          <?php //echo the_content(); ?>
          <a href="<?php echo get_permalink('230'); ?>/#showcase" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
          <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
          <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
        </div>
        <div class="col-lg-6 col-md-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
            class="img-fluid mx-auto banner_img">
          <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <h2 class="comman_h text-center">Custom Web Development Services</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="web-application-img">
          <img src="<?php echo get_template_directory_uri(); ?>/images/web-application-left.jpg" alt="">
        </div>
      </div>
      <div class="col-lg-6">
        <h5>
          Geekologix provides end-to-end webs development services. Everything from a simple app to a complex website,
          ecommerce platform, CMS, or SaaS. We cater to all your web development needs.
        </h5>
        <p class="comman_p">
          Geekologix excels at web development. Our web development teams are experts at their jobs. They code with
          passion and create with intuition. The company deals with various web development projects in multiple
          industries and business categories, giving us a competitive edge and great knowledge and expertise related to
          industry specific needs.
        </p>
        <p class="comman_p">
          We Development has become a crucial part of business growth. And Geekologix has decided to help businesses
          build themselves us with our effective solutions tailored for specific requirements.
        </p>
      </div>
    </div>
  </div>
</section>
<!-- End Web Application  -->
<!-- Start  Geekologix Navigating -->
<section class="geekologix-navigating">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <h3 class="comman_h text-center">Developing Robust Apps for your Business Needs</h3>
        <p class="comman_p text-center">A breakdown of our web development services to help you understand our processes better.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <ul class="geekologix-navigating-list">
          <li>
            <h5>Professional Consultation</h5>
            <p>Our development experts consider your needs and requirements specifically, and suggest multiple solutions
              to aid your concerns with detailed explanations to help you choose the best one for your business.</p>
          </li>
          <li>
            <h5>Strategy and Plan</h5>
            <p>We plan and provide you a roadmap or a strategy which is to be followed during the development of your
              solutions to help you scale and manage timelines, budget, etc., and prepare necessary documentation.</p>
          </li>
          <li>
            <h5>App or Software Prototyping</h5>
            <p>A blueprint of you’re the software or web app to be built for your business is presented to you. Changes,
              recommendations and ideas are discussed at this point. This is to majorly evaluate the impact of this
              solution on your business, and attempts to maximise it.</p>
          </li>
          <li>
            <h5>Agile Methodology</h5>
            <p>Our developers work with Agile framework, meaning that they are completely flexible and adaptive based on
              your needs. We help you to track the development with ease and transparency throughout.</p>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <div class="geekologix-navigating-img">
          <img src="<?php echo get_template_directory_uri(); ?>/images/geekologix-navigating.png" alt=""
            class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End  Geekologix Navigating -->
<!-- Start Platforms Industries -->
<section class="platforms-industries">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <h3 class="comman_h text-center">Geekologix — One-Stop Solution for all your Web Development needs</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12 platforms-tabing">
        <ul class="nav nav-pills " id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-services-tab" data-toggle="pill" href="#pills-services" role="tab"
              aria-controls="pills-services" aria-selected="true"> Services We offer </a>
          </li>
          <li class="nav-item khadipie">
            |
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-industries-tab" data-toggle="pill" href="#pills-industries" role="tab"
              aria-controls="pills-industries" aria-selected="false">Leading Industries</a>
          </li>
          <div class="clearfix"></div>
        </ul>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-services" role="tabpanel"
            aria-labelledby="pills-services-tab">
            <div class="row">
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Custom Web Design & Development">
                  <div class="industry_block real_state_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images real_state mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Custom Web Design & Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="ECommerce Web Development">
                  <div class="industry_block education_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images education mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">ECommerce Web Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="WordPress Development">
                  <div class="industry_block healthcare_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images healthcare mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">WordPress Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="AngularJS Development">
                  <div class="industry_block finance_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images finance mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">AngularJS Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Flutter Development">
                  <div class="industry_block automotives_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images automotives mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Flutter Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="React Native Development">
                  <div class="industry_block gaming_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images gaming mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">React Native Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="DevOps Developer and Engineer">
                  <div class="industry_block event_tickets_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images event_tickets mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">DevOps Developer and Engineer</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="IOS Development">
                  <div class="industry_block travel_tours_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images travel_tours mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">IOS Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Full Stack Development">
                  <div class="industry_block ecommerce_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images ecommerce mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Full Stack Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Laravel Development">
                  <div class="industry_block entertainment_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- 	<div class="industry_images entertainment mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Laravel Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Angular Development">
                  <div class="industry_block food_beverage_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images food_beverage mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Angular Development</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="SEO and SMM Services">
                  <div class="industry_block  fitness_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- 	<div class="industry_images fitness mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">SEO and SMM Services</h5>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-industries" role="tabpanel" aria-labelledby="pills-industries-tab">
            <div class="row">
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Real Estate">
                  <div class="industry_block real_state_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images real_state mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Real Estate</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Education">
                  <div class="industry_block education_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images education mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Education</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Healthcare">
                  <div class="industry_block healthcare_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images healthcare mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Healthcare</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Finance">
                  <div class="industry_block finance_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images finance mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Finance</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Automotives">
                  <div class="industry_block automotives_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images automotives mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Automotives</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Gaming">
                  <div class="industry_block gaming_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images gaming mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Gaming</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Event & Tickets">
                  <div class="industry_block event_tickets_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images event_tickets mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Event & Tickets</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Travel & Tours">
                  <div class="industry_block travel_tours_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images travel_tours mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Travel & Tours</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Ecommerce">
                  <div class="industry_block ecommerce_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images ecommerce mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Ecommerce</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Entertainment">
                  <div class="industry_block entertainment_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- 	<div class="industry_images entertainment mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Entertainment</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Food & Beverage">
                  <div class="industry_block food_beverage_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- <div class="industry_images food_beverage mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Food & Beverage</h5>
                  </div>
                </a>
              </div>
              <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Fitness">
                  <div class="industry_block  fitness_outer text-center">
                    <div class=" mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt=""
                        class="img_desktop">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt=""
                        class="img_hover mx-auto">
                    </div>
                    <!-- 	<div class="industry_images fitness mx-auto">
                                    <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                                </div> -->
                    <h5 class="text-capitalize">Fitness</h5>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Platforms Industries -->
<!-- Start  Flutter Services  -->
<section class="flutter-services advantages-expertise">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <h3 class="comman_h text-center">
          Geekologix as your Perfect Web Development Partner
        </h3>
        <p class="text-center">
          Accelerated Web Development Solutions with quality products and innovation are what we serve to our clients.
        </p>
      </div>
    </div>
    <div class="flutter-cards devops-cards">
      <div class="row">
        <div class="col-md-6 col-lg-4">
          <a href="javascript:void(0)" title="Seamless User Experience Across Devices">
            <div class="flutter-card">
              <div class="flutter-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/seamless.png" alt="">
              </div>
              <h5 class="">Flexible Hiring</h5>
              <p class="">
                At Geekologix, you can choose who you want to work with. We allow you to choose you developer or build a
                team of developers with our employees to help you with your development needs.
              </p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a href="javascript:void(0)" title="Pocket-Friendly App Development">
            <div class="flutter-card">
              <div class="flutter-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/easy-to-use.png" alt="">
              </div>
              <h5 class="">Pocket-Friendly App Development</h5>
              <p class="">
                Geekologix is known for providing the best deals and the most affordable packages when it comes to
                Industry Specific Web development.
              </p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a href="javascript:void(0)" title="Industry Experience">
            <div class="flutter-card">
              <div class="flutter-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/well-structured.png" alt="">
              </div>
              <h5 class="">Industry Experience</h5>
              <p class="">
                Our Web Developers have been working on various projects across industries and geographical regions,
                allowing them a profound knowledge of various frameworks and languages used for web development.
              </p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a href="javascript:void(0)" title="Creativity, Skills and Expertise">
            <div class="flutter-card">
              <div class="flutter-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/highly-scalable.png" alt="">
              </div>
              <h5 class="">Creativity, Skills and Expertise</h5>
              <p class="">
                Our web developers are passionate and creative. They design innovative web development solutions to help
                you empower your business towards growth and expansion.
              </p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a href="javascript:void(0)" title="Working on prerequisites">
            <div class="flutter-card">
              <div class="flutter-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/custom-solution.png" alt="">
              </div>
              <h5 class="">Working on prerequisites</h5>
              <p class="">
                We don’t exceed any constraints put forth by our clients, be it time or money. We deliver all our
                project on time and within the promised budgets.
              </p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a href="javascript:void(0)" title="Support and Maintenance">
            <div class="flutter-card">
              <div class="flutter-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/quality-assurance.png" alt="">
              </div>
              <h5 class="">Support and Maintenance</h5>
              <p class="">
                Our team managers are always available to resolve your queries or doubts from start to end, or even
                after the project is closed. We help you through it all until you are fully satisfied.
              </p>
            </div>
          </a>
        </div>
      </div>
    </div>
</section>
<!-- End  Flutter Services  -->
<!-- Start Technologies Faq -->
<section class="technologies-faq">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <h3 class="comman_h text-center">Our Technological Expertise</h3>
        <p class="comman_p text-center">The web development team of Geekologix collectively holds a vast knowledge of
          various advanced technologies used for building effective and powerful web development products.</p>
      </div>
    </div>
    <div id="accordion" class=" myaccordion technologies-accordian">
      <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h6 class="mb-0">
                <button class="d-flex align-items-center justify-content-between btn btn-link" data-toggle="collapse"
                  data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" title="Web Technology">
                  <div class="ios-offering-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-technology-key.png" alt="">
                  </div>
                  Web Technology
                  <span class="fa-stack fa-sm">
                    <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                  </span>
                </button>
              </h6>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                <ul class="technology-icon-list">
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Java">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-icon.png" alt="">
                      </div>
                      <small>Java</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="React">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/react-icon.png" alt="">
                      </div>
                      <small>React</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Python">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                      </div>
                      <small>Python</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Php">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/php-technology.png" alt="">
                      </div>
                      <small>Php</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Ruby">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ruby-technology.png" alt="">
                      </div>
                      <small>Ruby</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Nodejs">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/nodejs-technology.png" alt="">
                      </div>
                      <small>Nodejs</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Asp.net">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                      </div>
                      <small>Asp.net</small>
                    </a>
                  </li>

                </ul>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h6 class="mb-0">
                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"
                  title="Databases">
                  <div class="ios-offering-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/database-key.png" alt="">
                  </div>
                  Databases
                  <span class="fa-stack fa-2x">
                    <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                  </span>
                </button>
              </h6>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                <ul class="technology-icon-list">
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Java">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-icon.png" alt="">
                      </div>
                      <small>Java</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="React">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/react-icon.png" alt="">
                      </div>
                      <small>React</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Python">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                      </div>
                      <small>Python</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Php">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/php-technology.png" alt="">
                      </div>
                      <small>Php</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Ruby">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ruby-technology.png" alt="">
                      </div>
                      <small>Ruby</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Nodejs">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/nodejs-technology.png" alt="">
                      </div>
                      <small>Nodejs</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Asp.net">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                      </div>
                      <small>Asp.net</small>
                    </a>
                  </li>

                </ul>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h6 class="mb-0">
                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                  aria-controls="collapseThree" title="Platforms">
                  <div class="ios-offering-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/platforms-key.png" alt="">
                  </div>
                  Platforms
                  <span class="fa-stack fa-2x">
                    <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                  </span>
                </button>
              </h6>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                <ul class="technology-icon-list">
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Java">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-icon.png" alt="">
                      </div>
                      <small>Java</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="React">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/react-icon.png" alt="">
                      </div>
                      <small>React</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Python">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                      </div>
                      <small>Python</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Php">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/php-technology.png" alt="">
                      </div>
                      <small>Php</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Ruby">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ruby-technology.png" alt="">
                      </div>
                      <small>Ruby</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Nodejs">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/nodejs-technology.png" alt="">
                      </div>
                      <small>Nodejs</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Asp.net">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                      </div>
                      <small>Asp.net</small>
                    </a>
                  </li>

                </ul>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card">
            <div class="card-header" id="headingFoure">
              <h6 class="mb-0">
                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                  data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                  aria-controls="collapseFoure" title="Web Frameworks">
                  <div class="ios-offering-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-frameworks-key.png" alt="">
                  </div>
                  Web Frameworks
                  <span class="fa-stack fa-2x">
                    <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                  </span>
                </button>
              </h6>
            </div>
            <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure" data-parent="#accordion">
              <div class="card-body">
                <ul class="technology-icon-list">
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Java">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-icon.png" alt="">
                      </div>
                      <small>Java</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="React">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/react-icon.png" alt="">
                      </div>
                      <small>React</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Python">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                      </div>
                      <small>Python</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Php">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/php-technology.png" alt="">
                      </div>
                      <small>Php</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Ruby">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ruby-technology.png" alt="">
                      </div>
                      <small>Ruby</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Nodejs">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/nodejs-technology.png" alt="">
                      </div>
                      <small>Nodejs</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Asp.net">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                      </div>
                      <small>Asp.net</small>
                    </a>
                  </li>

                </ul>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingFive">
              <h6 class="mb-0">
                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                  data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"
                  title="UI/UX and Graphic">
                  <div class="ios-offering-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ui-key.png" alt="">
                  </div>
                  UI/UX and Graphic
                  <span class="fa-stack fa-2x">
                    <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                  </span>
                </button>
              </h6>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
              <div class="card-body">
                <ul class="technology-icon-list">
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Java">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-icon.png" alt="">
                      </div>
                      <small>Java</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="React">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/react-icon.png" alt="">
                      </div>
                      <small>React</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Python">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                      </div>
                      <small>Python</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Php">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/php-technology.png" alt="">
                      </div>
                      <small>Php</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Ruby">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ruby-technology.png" alt="">
                      </div>
                      <small>Ruby</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Nodejs">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/nodejs-technology.png" alt="">
                      </div>
                      <small>Nodejs</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Asp.net">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                      </div>
                      <small>Asp.net</small>
                    </a>
                  </li>

                </ul>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingSix">
              <h6 class="mb-0">
                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                  data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"
                  title="CMS and e-Commerce">
                  <div class="ios-offering-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/financial-ios-key-offering.png" alt="">
                  </div>
                  CMS and e-Commerce
                  <span class="fa-stack fa-2x">
                    <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                  </span>
                </button>
              </h6>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
              <div class="card-body">
                <ul class="technology-icon-list">
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Java">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/java-icon.png" alt="">
                      </div>
                      <small>Java</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="React">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/react-icon.png" alt="">
                      </div>
                      <small>React</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Python">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/paython.jpg" alt="">
                      </div>
                      <small>Python</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Php">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/php-technology.png" alt="">
                      </div>
                      <small>Php</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Ruby">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ruby-technology.png" alt="">
                      </div>
                      <small>Ruby</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Nodejs">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/nodejs-technology.png" alt="">
                      </div>
                      <small>Nodejs</small>
                    </a>
                  </li>
                  <li class="float-left">
                    <a href="javascript:void(0)" class="technology-icon-link" title="Asp.net">
                      <div class="technology-icon-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/dot-net.jpg" alt="">
                      </div>
                      <small>Asp.net</small>
                    </a>
                  </li>

                </ul>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Technologies Faq -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Star We Perfectit -->
<section class="we-perfectit">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <h3 class="comman_h text-center">We Just Don't Develop It, WePerfectIt</h3>
      </div>
    </div>
    <div class="we-perfecit-cards">
      <div class="row">
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Discovery">
            <div class="we-perfectit-card">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/discovery-perfectit.png" alt="">
              </div>
              <h5>Discovery</h5>
              <div class="discovery-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/discovery-arrow.png" alt="">
              </div>
            </div>
          </a>

        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Architecture">
            <div class="we-perfectit-card">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/architecture-perfectit.png" alt="">
              </div>
              <h5>Architecture</h5>
              <div class="architecture-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/architecture-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="UX/UI Design">
            <div class="we-perfectit-card">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ui-perfectit.png" alt="">
              </div>
              <h5>UX/UI Design</h5>
              <div class="ux-arrow-img discovery-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ux-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Development">
            <div class="we-perfectit-card">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/development-perfectit.png" alt="">
              </div>
              <h5>Development</h5>
              <div class="development-arrow-img architecture-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/development-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Post Launce Support">
            <div class="we-perfectit-card mb-100">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/post-perfectit.png" alt="">
              </div>
              <h5>Post Launce Support</h5>
              <div class="post-arrow-img architecture-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/post-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Website Launch">
            <div class="we-perfectit-card mb-100">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/website-perfectit.png" alt="">
              </div>
              <h5>Website Launch</h5>
              <div class="website-arrow-img discovery-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/website-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Intermediate Release">
            <div class="we-perfectit-card mb-100">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/intermediate-perfectit.png" alt="">
              </div>
              <h5>Intermediate Release</h5>
              <div class="intermediate-arrow-img architecture-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/intermediate-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
          <a href="javascript:void(0)" title="Quality Assurance">
            <div class="we-perfectit-card mb-100">
              <div class="perfect-card-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/quality-perfectit.png" alt="">
              </div>
              <h5>Quality Assurance</h5>
              <div class="intermediate-arrow-img architecture-arrow-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/quality-arrow.png" alt="">
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End We Perfectit -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
  <div class="container px-0">
    <div class="row">
      <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="owl-carousel owl-theme" id="client_slider">
          <!-- start loop -->
          <?php
                  $testimonialSlider =  array(
                      'post_status'    => 'publish', 
                      'post_type'      => 'client_slider', 
                      'posts_per_page' => -1,
                      'order'          => 'DESC',
                    );
                    $getTestimonialData = new WP_Query($testimonialSlider);
                    if($getTestimonialData ->have_posts()) {
                    while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                    $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                      $testimonial_img = $testimonialimg[0];
                  ?>
          <div class="item">
            <div class="client_block">
              <div class="client_img">
                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
              </div>


              <?php echo the_content(); ?>

            </div>

          </div>

          <?php 
                  endwhile;
              } ?>

        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
  <div class="container px-0">
    <div class="row">
      <div class="col-lg-7 col-xl-7">
        <h4 class="comman_h">Hire Flutter App Development</h4>
        <h5>Your idea, our execution. If you have an idea of making an android app feel free to contact</h5>
        <p>Let’s innovate together. Whether you are a start-up, a business enterprise or Fortune 500 company,
          hire our expert Android App development team today, to digitalize your business.</p>
        <div class="hire-dedicated-btn">
          <a href="<?php echo get_permalink(288); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
        </div>
      </div>
      <div class="col-lg-5 col-xl-5">
        <div class="hire-app-developer-img">
          <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
  <div class="container px-0">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
        <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
        <a href="<?php echo get_permalink(288); ?>" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
      </div>
    </div>
  </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
  <div class="container px-0">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Building Powerful Web Development Solutions across Industries
          round the globe.</h4>
        <p class="inner_content">Collaborate with our expert web developers and improvise your Business SOPs.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-7  col-md-7 pr-0">
        <div class="form_part">
          <div class="row">
            <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
          </div>
          <!-- 		<form>
          <div class="row">
            <div class="col-lg-6">
              <label> Your Name</label>
              <input type="text" class="form-control" name="" placeholder="Johan Smith">
            </div>
            <div class="col-lg-6">
                <label>Email Adress</label>
              <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
            </div>
            <div class="col-lg-6">
                <label>Phone Number</label>
              <input type="text" class="form-control" placeholder="91+123456789" name="">
            </div>
            <div class="col-lg-6">
                <label>I'am Interested in</label>
              <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
            </div>
            <div class="col-lg-12">
                <label>Messge</label>
              <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
            </div>
              <div class="col-lg-12">
            <button class="common_btns">Send Now</button>
          </div>
          </div>
          
        </form> -->
        </div>
      </div>
      <div class="col-lg-5  col-md-5 pl-0">
        <div class="right_side">
          <h5 class="text-uppercase">Reach Us</h5>
          <ul>
            <li>
              <i class="fal fa-map-marker-alt float-left"></i>
              <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fal fa-envelope"></i>
              <a class="float-left" href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fab fa-skype"></i>
              <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                <?php echo do_shortcode('[contact type="skype"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt="" class="fa_phone">
              <a class="float-left" href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
              <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
              <br>
              USA:
              <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
              <div class="clearfix"></div>
            </li>
          </ul>
          <div class="offices">
            <div class="float-left">
              Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                class="ml-2"><span class="india_div">India</span>
            </div>
            <div class="float-left usa-div">

              <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span class="">USA</span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
  <div class="footer-overlay"></div>
  <div class="container p-0">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="page-title text-uppercase text-center">
          <?php echo  $conatct_title ?>
        </h4>
      </div>
      <!-- Let's talk about you, -->
      <div class="col-lg-12 p-lg-0">
        <div class="let-talk-about">
          <?php echo $conatct_content; ?>
          <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
        </div>
      </div>
      <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
        <div class="footer-responsive">
          <div class="float-icon">
            <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
          </div>
          <address class="address-footer address-footer2">
            <h5>
              <?php echo do_shortcode('[contact type="office_name"]') ?>
            </h5>
            <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
              <i class="fal fa-phone"></i>
              <?php echo do_shortcode('[contact type="india mobile"]') ?>
            </a>
            <i class="fal fa-map-marker-alt"></i>
            <p>
              <?php echo do_shortcode('[contact type="office_address"]') ?>
            </p>
            <div class="clearfix"></div>
            <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
              <i class="fal fa-envelope"></i>
              <?php echo do_shortcode('[contact type="email_address"]') ?>
            </a>
            <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
              <i class="fab fa-skype"></i>
              <?php echo do_shortcode('[contact type="email_address"]') ?>
            </a>
            <div class="clearfix"></div>
          </address>
          <div class="clearfix"></div>
        </div>

      </div>
      <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
        <div class="footer-add">
          <div class="float-icon">
            <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
          </div>
          <address class="address-footer">
            <h5>
              <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
            </h5>
            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
              <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
              <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
            </a>
          </address>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
        <div class="footer-add">
          <div class="float-icon">
            <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
          </div>
          <address class="address-footer">
            <h5>
              <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
            </h5>
            <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
              <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
              <?php echo do_shortcode('[contact type="other_mobile"]') ?>
            </a>
          </address>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <div class="row copy-footer">
      <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
        <p class="copy-right">
          &copy;
          <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
        </p>
      </div>
      <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
        <ul>
          <li>
            <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank" title="facebook"> <i
                class="fab fa-facebook-f"></i></a>
          </li>
          <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
          <li>
            <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank" title="linkedin"><i
                class="fab fa-linkedin-in"></i></a>
          </li>
          <li>
            <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank" title="instagram"><i
                class="fab fa-instagram"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>