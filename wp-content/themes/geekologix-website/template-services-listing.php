<?php
  // Template Name:Sevices Listing Page
  the_post();
  /**
  * The main template file
  *
  * This is the most generic template file in a WordPress theme
  * and one of the two required files for a theme ( the other being style.css ).
  * It is used to display a page when nothing more specific matches a query.
  * E.g., it puts together the home page when no home.php file exists.
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package geekologix_website
  */
  get_header(); ?>
<!-- Start section 1 -->
<section class="sectionss" id="">
  <div id="" class="service_listing">
    <?php include 'header2.php'; ?>
    <div class="container px-0 common_heading">
      <div class="row">
        <div class="col-lg-7 col-12 col-md-9 left_heading">
          <h1 class="banner-heading ">
            <?php  echo the_title();?>
          </h1>
          <p class="pr-0"><strong>We are creative designers, expert developers, efficient project managers and
              articulate writers serving our clients with impeccable services.</strong></p>
          <p>Our services are client centred and our teams tend to adapt to the requirements of our clients to
            provide them the best possible solutions for their business needs. We serve you using high tech
            tools and advanced technology, combined with the vast experience of our team, ensuring you a
            great experience with us.</p>
          <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
          <!--   <a href="" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End section 1 -->

<section class="section service_sections" 2>
  <div class="container px-0">
    <div class="row">
      <div class="col-lg-6 col-md-6 left_section_part">
        <div class="listing_box  ">
          <a href="javascript:void(0)" title="Web Development" class="listing_heading">
            <img src="<?php echo get_template_directory_uri(); ?>/images/web-development.png">
          </a>
          <a href="javascript:void(0)" title="Web Development" class="text-uppercase listing_heading">
            Web Development</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Creative Custom Websites for branding and building effective digital presence.</p>
          <!-- /wp:paragraph -->

          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>PHP </span>
            </li>
            <li>
              <span>ASP.NET</span>
            </li>
            <li>
              <span>MEAN STACK</span>
            </li>
            <li>
              <span>REACT JS</span>
            </li>
            <li>
              <span>PYTHON</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- Mobile Developments Start -->
      <div class="col-lg-6 col-md-6 right_section_part">
        <div class="listing_box services2">
          <a href="" class="listing_heading" title="Mobile Wearables">
            <img src="<?php echo get_template_directory_uri(); ?>/images/mobile_wearaables-1.png">
          </a>
          <a href="javascript:void(0)" title="Mobile &amp; Wearables" class="text-uppercase listing_heading">
            Mobile &amp; Wearables</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Building Powerful Applications compatible with wearables for smartphone users.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>Android</span>
            </li>
            <li>
              <span>ios</span>
            </li>
            <li>
              <span>React</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!--  Mobile Developments End -->
      <!-- Digital Marketing Start -->
      <div class="col-lg-6 col-md-6 left_section_part">
        <div class="listing_box">
          <a href="javascript:void(0)" class="listing_heading">
            <img src="<?php echo get_template_directory_uri(); ?>/images/digital-market-1.png">
          </a>
          <a href="javascript:void(0)" title="Digital Marketing" class="text-uppercase listing_heading">
            Digital Marketing</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Managing your sales strategy online, creating new funnels and sales channels for you; Multiplying
            your business opportunities.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>SEO</span>
            </li>
            <li>
              <span>PPC</span>
            </li>
            <li>
              <span>SMO</span>
            </li>
            <li>
              <span>SMM</span>
            </li>
            <li>
              <span>MOBILE APP MARKETING</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- Digital Marketing Start -->
      <!-- ecommerce Start -->
      <div class="col-lg-6 col-md-6 right_section_part">
        <div class="listing_box services2">
          <a href="" class="listing_heading" title="Ecommerce Development">
            <img src="<?php echo get_template_directory_uri(); ?>/images/ecoomerce-1.png">
          </a>
          <a href="" title="Ecommerce Development" class="text-uppercase listing_heading">
            Ecommerce Development</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Establishing and empowering businesses online with e-com websites.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>SHOPIFY</span>
            </li>
            <li>
              <span>MAGENTO </span>
            </li>
            <li>
              <span>OPENCART </span>
            </li>
            <li>
              <span> WORDPRESS</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- ecommerce end -->
      <!-- devops Consulting Start -->
      <div class="col-lg-6 col-md-6 left_section_part">
        <div class="listing_box">
          <a href="" class="listing_heading" title="Devops Consulting">
            <img src="<?php echo get_template_directory_uri(); ?>/images/devops-consulting.png">
          </a>
          <a href="" title="Devops Consulting" class="text-uppercase listing_heading">
            Devops Consulting</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Helping you automate and shorten your SOPs with smart technology and personalised IT Solutions.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>DEVOPS ASSESSMENT</span>
            </li>
            <li>
              <span>DEVOPS AUTOMATION</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- devops Consulting End -->
      <!-- cloud Based Services Start -->
      <div class="col-lg-6 col-md-6 right_section_part">
        <div class="listing_box services2">
          <a href="" class="listing_heading" title=" Cloud Based Services">
            <img src="<?php echo get_template_directory_uri(); ?>/images/cloud-based-services-1.png">
          </a>
          <a href="" title="Cloud Based Services" class="text-uppercase listing_heading">
            Cloud Based Services</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Helping businesses work smarter and become more efficient with smart and affordable technology.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>AMAZON WEB SERVICES</span>
            </li>
            <li>
              <span>MICROSOFT AZURE</span>
            </li>
            <li>
              <span>DIGITALOCEAN</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- cloud Based Services End -->
      <!-- industry Solution Start -->
      <div class="col-lg-6 col-md-6 left_section_part">
        <div class="listing_box">
          <a href="" class="listing_heading" title="Industry Solutions">
            <img src="<?php echo get_template_directory_uri(); ?>/images/industry-solution.png">
          </a>
          <a href="" title="Industry Solutions" class="text-uppercase listing_heading">
            Industry Solutions</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Providing efficient business solutions for everyday problems tailored for your industry or business
            category.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>CONSULTATION </span>
            </li>
            <li>
              <span>ENTERPRISE MOBILITY</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- industry Solution end -->
      <!-- business Consulting Start -->
      <div class="col-lg-6 col-md-6 right_section_part">
        <div class="listing_box services3">
          <a href=" class=" listing_heading" title="Business Consulting">
            <img src="<?php echo get_template_directory_uri(); ?>/images/business-consulting.png">
          </a>
          <a href="javascript:void(0)" title="Business Consulting" class="text-uppercase listing_heading">
            Business Consulting</a>
          <p>
            <!-- wp:paragraph -->
          </p>
          <p>Assisting your business growth, sales strategies and marketing channels with smart technology.</p>
          <!-- /wp:paragraph -->
          <p></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>
              <span>BUSINESS STRUCTURING</span>
            </li>
            <li>
              <span>GROWTH STRATEGY</span>
            </li>
          </ul>
          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <!-- business Consulting End -->
    </div>
  </div>
</section>

<!-- start Section 2  Services -->
<!-- <section class="section service_sections" id="">
  <div class="container">
    <div class="row">
       <?php
    $geekologixservices = array(
      "post_type"   => "geekologixservices",
      "post_status"   => "publish",
      "posts_per_page" => 8,
      "order"    => "ASC",
    );
    $geekologixservicesData = new WP_Query($geekologixservices);
    $count = 1;
    while ($geekologixservicesData->have_posts()) : $geekologixservicesData->the_post();
            $get_services_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
              $servicesimage = $get_services_image[0];
    
    if ($count == 1){
                  $addClass='';
                }
                if ($count == 2){
    
                    $addClass='services2';
                }
                 if ($count == 3){
    
                    $addClass='';
                }
                   if ($count == 4){
    
                    $addClass='services2';
                }
                   if ($count == 5){
    
                    $addClass='';
                }
                   if ($count == 6){
    
                    $addClass='services2';
                }
                   if ($count == 7){
    
                    $addClass='';
                }
               if ($count == 8){
    
                    $addClass='services2';
                }
    
    ?>
      <div class="col-lg-6 col-md-6">
        <div class="listing_box <?php echo $addClass; ?> ">
          <a href="" class="listing_heading">
          <img src="<?php echo $servicesimage; ?>" alt="<?php echo the_title(); ?>">
          </a> 
          <a href="" title="<?php echo the_title(); ?>" class="text-uppercase listing_heading"> <?php echo the_title(); ?></a>
          <p>                         <?php 
    echo the_content();
     ?></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
          
                        <?php 
    while( have_rows('service_tag') ): the_row(); 
    
    ?>
                        <li>
                           <span><?php echo get_sub_field('add_tag_name'); ?></span> 
                        </li>
                             <?php  
    endwhile; 
    ?>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
           <?php  
    $count++; endwhile;
    wp_reset_query();
    ?>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/mobile_wearaables.png" alt="MOBILE & WEARABLES">
          </a> 
          <a href="" title="MOBILE & WEARABLES" class="text-uppercase listing_heading">MOBILE & WEARABLES</a>
          <p>We speak the language of creativity; building smart mobile applications for smart users.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Android</li>
            <li>Ios</li>
            <li>REACT</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Digital Marketing">
          </a> 
          <a href="" title="Web Development" class="text-uppercase listing_heading">DIGITAL MARKETING</a>
          <p>Make the most of your digital presence; Custom websites at affordable prices.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Seo</li>
            <li>ppc</li>
            <li>sem</li>
            <li>smo</li>
            <li>smm</li>
            <li>Mobile Marketing</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Ecommerce Development">
          </a> 
          <a href="" title="Web Development" class="text-uppercase listing_heading">ECOMMERCE DEVELOPMENT</a>
          <p>Establishing and empowering businesses online with e-com websites.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Shopify</li>
            <li>Magento</li>
            <li>Opencart</li>
            <li>Wordpress</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Devops Consulting">
          </a> 
          <a href="" title="DEVOPS CONSULTING" class="text-uppercase listing_heading">Devops Consulting</a>
          <p>Make the most of your digital presence; Custom websites at affordable prices.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Devops Assessment</li>
            <li>Devops Automation</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Cloud Based Sevices">
          </a> 
          <a href="" title="Cloud Based Sevices" class="text-uppercase listing_heading">Cloud Based Sevices</a>
          <p>Work Smarter with higher efficiency; Pay as you use.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Amazon Web Services</li>
            <li>Microsoft Azure</li>
            <li>Digitalocean</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Industry Solutions">
          </a> 
          <a href="" title="Industry Solutions" class="text-uppercase listing_heading">Industry Solutions</a>
          <p>Providing you the best in class solutions in quality and trust.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Consultation</li>
            <li>Enterprise Mobility</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Business Consulting">
          </a> 
          <a href="" title="Business Consulting" class="text-uppercase listing_heading">Business Consulting</a>
          <p>Smart Ideas for a better future, better growth, better marketing.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Business Structuring</li>
            <li>Growth Strategy</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
    </div>
  </div>
  </section> -->
<!-- End section 2  Services-->
<!--End contact  section7-->
<!-- start Inner footer -->
<!-- end Inner Footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
  <div class="footer-overlay"></div>
  <div class="container p-0">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="page-title text-uppercase text-center">
          <?php echo  $conatct_title ?>
        </h4>
      </div>
      <!-- Let's talk about you, -->
      <div class="col-lg-12 p-lg-0">
        <div class="let-talk-about">
          <?php echo $conatct_content; ?>
          <!-- <a href="" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
        </div>
      </div>
      <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
        <div class="footer-responsive">
          <div class="float-icon">
            <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
          </div>
          <address class="address-footer address-footer2">
            <h5>
              <?php echo do_shortcode('[contact type="office_name"]') ?>
            </h5>
            <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
              <i class="fal fa-phone"></i>
              <?php echo do_shortcode('[contact type="india mobile"]') ?>
            </a>
            <i class="fal fa-map-marker-alt"></i>
            <p>
              <?php echo do_shortcode('[contact type="office_address"]') ?>
            </p>
            <div class="clearfix"></div>
            <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
              <i class="fal fa-envelope"></i>
              <?php echo do_shortcode('[contact type="email_address"]') ?>
            </a>
            <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
              <i class="fab fa-skype"></i>
              <?php echo do_shortcode('[contact type="email_address"]') ?>
            </a>
            <div class="clearfix"></div>
          </address>
          <div class="clearfix"></div>
        </div>

      </div>
      <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
        <div class="footer-add">
          <div class="float-icon">
            <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
          </div>
          <address class="address-footer">
            <h5>
              <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
            </h5>
            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
              <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
              <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
            </a>
          </address>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
        <div class="footer-add">
          <div class="float-icon">
            <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
          </div>
          <address class="address-footer">
            <h5>
              <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
            </h5>
            <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
              <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
              <?php echo do_shortcode('[contact type="other_mobile"]') ?>
            </a>
          </address>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <div class="row copy-footer">
      <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
        <p class="copy-right">
          &copy;
          <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
        </p>
      </div>
      <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
        <ul>
          <li>
            <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank" title="facebook"> <i
                class="fab fa-facebook-f"></i></a>
          </li>
          <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
          <li>
            <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank" title="linkedin"><i
                class="fab fa-linkedin-in"></i></a>
          </li>
          <li>
            <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank" title="instagram"><i
                class="fab fa-instagram"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>