<?php
  // Template Name:iOS App Development
  the_post();

  get_header(); 
  ?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xl-7">
                    <h1 class="banner-heading ">
                        Spectacular IOS App Development
                        <!-- <?php 

              //$about_ID = 168;
             // $about_title = get_page($about_ID);
             //echo the_title();?> 
             -->
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>We are proud to deliver powerful and flawless native and cross platform IOS mobile apps </p>
                    <?php echo the_content(); ?>
                    <a href="<?php echo get_permalink('304'); ?>/#showcase" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6 col-xl-5">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/wearable-banner.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start iOS Services -->
<section class="ios-services">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-6">
                <div class="ios-service-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ios-services-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ios-service-content">
                    <h2 class="comman_h">Custom IOS App Development Services</h2>
                    <ul class="ios-service-list">
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/compatibility-ios-icon.png"
                                alt="">
                            <div class="ml-15">
                                <h5>Compatibility</h5>
                                <p>Highly adaptive apps compatible on global IOS Platforms.</p>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/standards-ios-icon.png" alt="">
                            <div class="ml-15">
                                <h5>Standards</h5>
                                <p>Designing & Development on the basis of Apple Recommendations.</p>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/reliability-ios-icon.png"
                                alt="">
                            <div class="ml-15">
                                <h5>Reliability</h5>
                                <p>Highly reliable development models and testing process.</p>
                            </div>

                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/optimization-ios-icon.png"
                                alt="">
                            <div class="ml-15">
                                <h5>Optimization</h5>
                                <p>Apps Optimised for appropriate data and battery consumption.</p>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/support-ios-icon.png" alt="">
                            <div class="ml-15">
                                <h5>Support</h5>
                                <p>Debugging and Maintenance even after closing of project.</p>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/flawless-graphics.png" alt="">
                            <div class="ml-15">
                                <h5>Flawless Graphics</h5>
                                <p>Apps Designing with perfect graphics that stand out and justify your business.</p>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/security-ios-icon.png" alt="">
                            <div class="ml-15">
                                <h5>Security</h5>
                                <p>Enabled encryption and web service technologies for secure use.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End iOS Services -->
<!-- Start Geekologix iOS Advantage -->
<section class="geekologix-ios-advantage">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Geekologix as your IOS Development Partner</h3>
                <p class="comman_p text-center">Our Creative Designers and Expert Developers work together to create the
                    best IOS Apps for you.</p>
            </div>
        </div>
        <div class="ios-advantage-cards">
            <div class="row">
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Experienced IOS Developers">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/apple-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Experienced IOS Developers</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Confidential Development">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/information-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Confidential Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Intense Quality Testing">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/quality-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Intense Quality Testing</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Pocket-Friendly IOS Development Services">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/competitive-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Pocket-Friendly IOS Development Services</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Attractive UI/UX Designs">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/continuous-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Attractive UI/UX Designs</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Delivery before Deadlines">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/compelling-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Delivery before Deadlines</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Development with Latest High Tech & Tools">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/on-time-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Development with Latest High Tech & Tools</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Support & Maintenance post Project Handover">
                        <div class="ios-advantage-card">
                            <div class="ios-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/latest-ios-advantage.png"
                                    alt="">
                            </div>
                            <h5>Support & Maintenance post Project Handover</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Geekologix iOS Advantage -->
<!-- Start ios Key Offering -->
<section class="ios-key-offering">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">IOS App Development Services by Geekologix</h3>
            </div>
        </div>
        <div class="ios-offering-cards">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Financial and e-Commerce iOS Applications">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/financial-ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>Financial and e-Commerce iOS Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Travel and Booking iOS Solutions">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/travel-ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>Travel and Booking iOS Solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="iOS ERP Solutions">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/erp-ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>iOS ERP Solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="iOS e-Learning Application Development">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>iOS e-Learning Application Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Social Media iOS Applications">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/social-media-ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>Social Media iOS Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Location-Based iOS Applications">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/location-ios-key-offring.png"
                                    alt="">
                            </div>
                            <h5>Location-Based iOS Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="iOS Media Applications">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/media-ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>iOS Media Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Maintenance & Backend Support">
                        <div class="ios-offering-card">
                            <div class="ios-offering-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/maintenance-ios-key-offering.png"
                                    alt="">
                            </div>
                            <h5>Maintenance & Backend Support</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End ios Key Offering -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start section 9 Technology Framework-->
<section class="technology_framework">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class=" inner_heading mx-auto">Key Tech Expertise</h4>
            </div>
        </div>
        <div class="framework-slider">
            <div class="row">
                <div class="col-12">
                    <!-- start loop -->
                    <div class="owl-carousel owl-theme" id="framework_slider">
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Objective-c">
                                <div class="framework_block" title="Objective-c">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/objective-c.png"
                                        alt="Objective-c" class="mx-auto">
                                    <h5>Objective-c</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Native SDK">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/native-sdk.png"
                                        alt="Native SDK" class="mx-auto">
                                    <h5>Native SDK</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Xcode">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/xcode.png" alt="Xcode"
                                        class="mx-auto">
                                    <h5>Xcode</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="SQLite">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/sqlite.png"
                                        alt="SQLite" class="mx-auto">
                                    <h5>SQLite</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Core Data">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/core-data.png"
                                        alt="Core Data" class="mx-auto">
                                    <h5>Core Data</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Swift">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/swift.png"
                                        alt="swift.png" class="mx-auto">
                                    <h5>Swift</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 9 Technology Framework -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="inner_heading text-center">Client's Reviews</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Expert IOS App Developers at Geekologix</h4>
                <h5>Innovative and Powerful IOS Apps tailored for your business requirements</h5>
                <p>Your idea will be soon in your hands, on your IOS Smartphone now. Now Execute your custom IOS App
                    idea with our expert IOS App Developers in meticulous quality and pocket-friendly prices.</p>
                <div class="hire-dedicated-btn">
                    <a href="javascript:void(0)" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner ">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>/" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
                <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
						<div class="row">
							<div class="col-lg-6">
								<label> Your Name</label>
								<input type="text" class="form-control" name="" placeholder="Johan Smith">
							</div>
							<div class="col-lg-6">
									<label>Email Adress</label>
								<input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
							</div>
							<div class="col-lg-6">
									<label>Phone Number</label>
								<input type="text" class="form-control" placeholder="91+123456789" name="">
							</div>
							<div class="col-lg-6">
									<label>I'am Interested in</label>
								<input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
							</div>
							<div class="col-lg-12">
									<label>Messge</label>
								<input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
							</div>
								<div class="col-lg-12">
							<button class="common_btns">Send Now</button>
						</div>
						</div>
						
					</form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->

<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>