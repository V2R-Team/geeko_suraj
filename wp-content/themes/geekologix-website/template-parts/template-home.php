<?php
// Template Name:Home Page One
the_post();
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme ( the other being style.css ).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package geekologix_website
*/

get_header(); ?>
<!-- Start section 1 -->
<section class="section" id="section1">
    <div id="particles-js">

        <div class="header-top-blank-div"></div>
        <header class="header-top header-fixed"">
          <div class=" container-fluid">
            <div class="row">
                <div class="col-6">
                    <?php // Logo
                      $header_image = get_header_image();
                      if (!empty($header_image)) : ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php  echo get_bloginfo(); ?>"
                        class="logo web-btn-banner">
                        <img src="<?php echo esc_url($header_image); ?>" class="" />
                    </a>
                    <?php endif; ?>
                </div>
                <div class="col-6 text-right align-self-center">
                    <a href="javascript:void(0)" class="" id="menu_open">
                        <img src="<?php echo get_template_directory_uri() ?>/images/nav.png" alt="" />
                    </a>
                </div>
            </div>
    </div>
    </header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="banner-heading pt-225">
                    <div id="text-type"></div>
                </h1>
                <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
            </div>
        </div>
    </div>
    </div>
</section>
<!-- End section 1 -->
<!-- start Section 2  Services -->
<section class="section" id="section2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    Geeky solutions for your business growth
                </h4>
            </div>

            <?php
    $mobile_wear_ID = 168;
    $mobile_wear_title = get_page($mobile_wear_ID);
    $mobile_wears_title = $mobile_wear_title->post_title;
    $mobile_wear_content = $mobile_wear_title->post_content;

    $mobile_wear_img_url = get_the_post_thumbnail_url($mobile_wear_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(168) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',168) ?>" alt="<?php echo the_title(); ?>" />

                        <?php echo $mobile_wears_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',168) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo $mobile_wear_content ;
                     ?>
                    </p>
                </div>
            </div>
            <?php
    $mobile_develop_ID = 230;
    $mobile_develop_title = get_page($mobile_develop_ID);
    $mobile_develops_title = $mobile_develop_title->post_title;
    $mobile_develop_content = $mobile_develop_title->post_content;

    $mobile_develop_img_url = get_the_post_thumbnail_url($mobile_develop_ID);
?>

            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(230) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',230) ?>"
                            alt="<?php echo $mobile_develops_title; ?>" />

                        <?php echo $mobile_develops_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',230) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo $mobile_develop_content ;
                     ?>
                    </p>
                </div>
            </div>
            <!--  Mobile Developments End -->
            <!-- Digital Marketing Start -->
            <?php
    $mobile_digital_ID = 232;
    $mobile_digital_title = get_page($mobile_digital_ID);
    $mobile_digitals_title = $mobile_digital_title->post_title;
    $mobile_digital_content = $mobile_digital_title->post_content;

    $mobile_digital_img_url = get_the_post_thumbnail_url($mobile_digital_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(232) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',232) ?>"
                            alt="<?php echo $mobile_digitals_title; ?>" />

                        <?php echo $mobile_digitals_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',232) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo $mobile_digital_content ;
                     ?>
                    </p>
                </div>
            </div>
            <!-- Digital Marketing Start -->
            <!-- ecommerce Start -->
            <?php
    $mobile_ecommerce_ID = 234;
    $mobile_ecommerce_title = get_page($mobile_ecommerce_ID);
    $mobile_ecommerces_title = $mobile_ecommerce_title->post_title;
    $mobile_ecommerce_content = $mobile_ecommerce_title->post_content;

    $mobile_ecommerce_img_url = get_the_post_thumbnail_url($mobile_ecommerce_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(234) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',234) ?>"
                            alt="<?php echo $mobile_ecommerces_title; ?>" />

                        <?php echo $mobile_ecommerces_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',234) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo $mobile_ecommerce_content ;
                     ?>
                    </p>
                </div>
            </div>
            <!-- ecommerce end -->
            <!-- devops Consulting Start -->
            <?php
    $mobile_consulting_ID = 237;
    $mobile_consulting_title = get_page($mobile_consulting_ID);
    $mobile_consultings_title = $mobile_consulting_title->post_title;
    $mobile_consulting_content = $mobile_consulting_title->post_content;

    $mobile_consulting_img_url = get_the_post_thumbnail_url($mobile_consulting_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(237) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',237) ?>"
                            alt="<?php echo $mobile_consultings_title ?>" />

                        <?php echo $mobile_consultings_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',237) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo  $mobile_consulting_content ;
                     ?>
                    </p>
                </div>
            </div>
            <!-- devops Consulting End -->
            <!-- cloud Based Services Start -->
            <?php
    $mobile_cloud_ID = 239;
    $mobile_cloud_title = get_page($mobile_cloud_ID);
    $mobile_clouds_title = $mobile_cloud_title->post_title;
    $mobile_cloud_content = $mobile_cloud_title->post_content;

    $mobile_cloud_img_url = get_the_post_thumbnail_url($mobile_cloud_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(239) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon', 239) ?>"
                            alt="<?php echo $mobile_clouds_title ?>" />

                        <?php echo $mobile_clouds_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',239) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo  $mobile_cloud_content ;
                     ?>
                    </p>
                </div>
            </div>
            <!-- industry Solution Start -->
            <?php
    $mobile_industry_ID = 241;
    $mobile_industry_title = get_page($mobile_industry_ID);
    $mobile_industrys_title = $mobile_industry_title->post_title;
    $mobile_industry_content = $mobile_industry_title->post_content;

    $mobile_cloud_img_url = get_the_post_thumbnail_url($mobile_industry_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(241) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',241) ?>"
                            alt="<?php echo  $mobile_industrys_title ?>" />

                        <?php echo  $mobile_industrys_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',241) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo  $mobile_industry_content ;
                     ?>
                    </p>
                </div>
            </div>
            <!-- industry Solution end -->
            <!-- business Consulting Start -->
            <?php
    $mobile_business_ID = 243;
    $mobile_business_title = get_page($mobile_business_ID);
    $mobile_businesss_title = $mobile_business_title->post_title;
    $mobile_business_content = $mobile_business_title->post_content;

    $mobile_business_img_url = get_the_post_thumbnail_url($mobile_business_ID);
?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 wow fadeInUp" data-wow-duration="0.<?php echo $counter?>s"
                data-wow-delay="0.<?php echo $counter?>s">
                <div class="service-block <?php echo $addClass;?>">
                    <a href="<?php echo the_permalink(243) ?>" class="service-title text-uppercase">
                        <img src="<?php echo get_field('upload_icon',243) ?>"
                            alt="<?php echo  $mobile_businesss_title ?>" />

                        <?php echo  $mobile_businesss_title ?>
                    </a>
                    <ul class="tag-list">

                        <?php 
                          while( have_rows('service_tag',243) ): the_row(); 

                          ?>
                        <li>
                            <span><?php echo get_sub_field('add_tag_name'); ?></span>
                        </li>

                        <?php  
                        endwhile; 
                    ?>
                    </ul>
                    <p>
                        <?php 
                        
                    //      $contentPage= substr(get_the_content(),0,150);

                    //   if (strlen($contentPage) <150) { 

                    //      $readmore ='...';
                    //   }else {
                    //   echo '';
                    //   }

                    //   echo $contentPage.$readmore;
                    echo   $mobile_business_content ;
                     ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 2  Services-->
<!-- Section 3 Our Work -->

<section class="section" id="section3">
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    Designing a better future for our clients
                </h4>
            </div>

            <div class="col-12">
                <div class="owl-carousel owl-theme" id="ourwork">
                    <?php 

            $geekologixWork = array(
            "post_type"   => "ourwork",
            "post_status"   => "publish",
            "posts_per_page" => 8,
            "order"    => "ASC",
          );
          $geekologixWorkData = new WP_Query($geekologixWork);
          $counter = 1;
          while ($geekologixWorkData->have_posts()) : $geekologixWorkData->the_post();
            $our_deskimg1 = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
          
            $mobile_img1 = $image_arr1[0]; 

            ?>
                    <div class="item">
                        <div class="hover-effect">
                            <div class="our_work-block" style="
                  background-image: url(<?php echo $our_deskimg1 ?>);">
                                <div class="overlay-block">
                                    <a href="" class="work-title" title="<?php echo the_title(); ?>">

                                        <?php echo the_title(); ?>
                                    </a>
                                    <?php echo the_content(); ?>

                                    <img src="<?php echo get_field('portfolio_thumbnails') ?>" alt="" />

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  endwhile;
          wp_reset_query();  ?>

                </div>
            </div>

            <?php 

            $geekologixWork = array(
            "post_type"   => "ourwork",
            "post_status"   => "publish",
            "posts_per_page" => 8,
            "order"    => "ASC",
          );
          $geekologixWorkData = new WP_Query($geekologixWork);
          $counter = 1;
          while ($geekologixWorkData->have_posts()) : $geekologixWorkData->the_post();
            $our_deskimg1 = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            $image_arr1 = wp_get_attachment_image_src(get_post_thumbnail_id($our_processData->ID), 'medium');
            $mobile_img1 = $image_arr1[0]; 

            ?>
            <div class="col-lg-4 col-6 pl-lg-2 pr-lg-2 desktopWork  wow fadeInUp" data-wow-duration="0.<?php echo $i?>s"
                data-wow-delay="0.<?php echo $i?>s">

                <?php  if(get_field('url') =="") { ?> <div class="hover-effect">
                    <div class="our_work-block" style="
                  background-image: url(<?php echo $our_deskimg1 ?>);">
                        <div class="overlay-block">
                            <span class="work-title" title="<?php echo the_title(); ?>">

                                <?php the_title(); ?>
                            </span>
                            <?php echo the_content(); ?>
                            <picture>
                                <source srcset="
                                  data:image/gif;base64,
                                  R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=
                                " media="(max-width: 767px)">
                                <img src="<?php echo get_field('portfolio_thumbnails') ?>" alt=""
                                    class="without_link" />
                            </picture>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div><?php }else { ?>
                <a href="<?php echo get_field('url') ?>" title="<?php echo the_title(); ?>" target="_blank">
                    <div class="hover-effect">
                        <div class="our_work-block" style="
                  background-image: url(<?php echo $our_deskimg1 ?>);">
                            <div class="overlay-block">
                                <span href="" class="work-title" title="<?php echo the_title(); ?>">

                                    <?php echo the_title(); ?>
                                </span>
                                <?php echo the_content(); ?>
                                <picture>
                                    <source srcset="
                                  data:image/gif;base64,
                                  R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=
                                " media="(max-width: 767px)">
                                    <img src="<?php echo get_field('portfolio_thumbnails') ?>" alt="" />
                                </picture>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>


                </a>
            </div>
            <?php  endwhile;
          wp_reset_query();  ?>
        </div>
    </div>
</section>
<!-- End Section 3 Our Work -->
<!-- section Top Rated section4 -->
<!-- Dynamic Page Data  -->
<?php
      // $toprated_ID = 28;
      // $toprated_data = get_page($toprated_ID);
      // $fetch_title = $toprated_data->post_title;
      // $fetch_content = $toprated_data->post_content;
    ?>
<section class="section" id="section4">
    <!-- <div class="top-rated">
        <div class="container p-0">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-title text-uppercase text-center">
                        <?php //echo  $fetch_title; ?>
                    </h4>
                </div>
                <div class="col-lg-6 right-top-rated wow fadeInLeft" data-wow-duration="0.5s" data-wow-delay="0.4s">
                    <ul>
                        <?php

                        $pageId //=28;
                          //while( have_rows('upload_logo',$pageId) ): the_row(); 

                          ?>
                        <li>
                            <picture>
                                <source srcset="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=1" media="(max-width: 767px)">
                                <img src="<?php //echo get_sub_field('upload_icon', $pageId); ?>" alt="<?php  //echo  $fetch_title ?>" />
                            </picture>
                        </li>
                        <?php  
                        //endwhile; 
                    ?>
                    </ul>
                </div>
                <div 
                    class="col-lg-6 top-rated-content pl-lg-4 pr-lg-0 wow fadeInRight"
                    data-wow-duration="0.5s"
                    data-wow-delay="0.4s">
                  <?php //echo $fetch_content; ?>
                </div>
            </div>
        </div>
    </div> -->
    <?php
    $career_ID = 53;
    $career_data = get_page($career_ID);
    $career_title = $career_data->post_title;
    $career_content = $career_data->post_content;
  ?>
    <div class="top-career">
        <div class="container p-0">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-title text-uppercase text-center">
                        We are hiring
                    </h4>
                    <div class="career_content"> <?php echo $career_content; ?></div>
                    <p class="text-center"> <b class="text-uppercase">You are in the right place.</b> Kindly drop your
                        updated resume at <a href="mailto:hr@geekologix.com">hr@geekologix.com.</a></p>
                    <!--                      <a href="#" title="Exprore Careers" class="web-btn text-uppercase">Join our team</a> 
 -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section Top Rated section4 -->
<!-- Our Process section5 -->
<section class="section" id="section5">
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    Working Process
                </h4>
            </div>
            <?php
          $ourprocess = array(
            "post_type"   => "geekologix-process",
            "post_status"   => "publish",
            "posts_per_page" => 3,
            "order"    => "ASC",
          );
          $our_processData = new WP_Query($ourprocess);;
          while ($our_processData->have_posts()) : $our_processData->the_post();
            $our_deskimg = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            $image_arr = wp_get_attachment_image_src(get_post_thumbnail_id($our_processData->ID), 'medium');
            $mobile_img = $image_arr[0]; // $image_url is your URL.
          ?>
            <div class="col-lg-4 col-12 col-sm-12 col-md-4 p-0 wow fadeInUp" data-wow-duration="0.5s"
                data-wow-delay="0.4s">
                <div class="process-block">
                    <div class="card">
                        <div class="front">
                            <picture>
                                <source srcset="
                                  data:image/gif;base64,
                                  R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=
                                " media="(max-width: 767px)">
                                <img src="<?php echo $our_deskimg; ?>" alt="" />
                            </picture>
                            <!-- Responsive Case 300px -->
                            <picture>
                                <source srcset="
                                  data:image/gif;base64,
                                  R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=
                                " media="(min-width: 767px)">
                                <img src="<?php echo $our_deskimg ?>" alt="" />
                            </picture>
                            <!-- Responsive Case 300px -->
                            <div class="process-block-overlay text-left">
                                <a href="#" title="Design"> <?php echo the_title(); ?></a>
                            </div>
                        </div>
                        <div class="back-flip">
                            <a href="#" title="Design"> <?php echo the_title(); ?></a>
                            <?php 

                                echo the_content();
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <?php endwhile;
          wp_reset_query();
          ?>
        </div>
    </div>
</section>
<!-- End Our Process section5 -->
<!-- About Us Section5 -->
<?php
    $about_ID = 42;
    $about_title = get_page($about_ID);
    $aboutts_title = $about_title->post_title;
    $about_content = $about_title->post_content;
    $featured_img_url = get_the_post_thumbnail_url($about_ID);
  ?>
<section class="section" id="section6">
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $aboutts_title; ?>
                </h4>
            </div>
            <div class="col-lg-6 pl-lg-0 abbout-content-left wow fadeInLeft" data-wow-duration="0.5s"
                data-wow-delay="0.4s">



                <?php echo $about_content; ?>
            </div>
            <div class="col-lg-6 pr-lg-0 wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.4s">
                <div class=" about-right">
                    <picture>
                        <source srcset="
                          data:image/gif;base64,
                          R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=
                        " media="(max-width: 767px)">
                        <img src="<?php echo $featured_img_url; ?>" alt="" class="about-banner">
                        <img src="<?php echo get_field('upload_image', $about_ID) ?>" alt=""
                            class="about-banner-bottom">
                    </picture>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Us Section5  -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india_mobile2"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="skype"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <!--<div class="footer-add">-->
                <!--    <div class="float-icon">-->
                <!--        <img src="<?php  //echo get_template_directory_uri() ?>/images/office-icon.png">-->
                <!--    </div>-->
                <!--    <address class="address-footer">-->
                <!--        <h5>-->
                <!--            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>-->
                <!--        </h5>-->
                <!--        <a href="tel:<?php //echo do_shortcode('[contact type="other_mobile2"]') ?>">-->
                <!--            <img src="<?php //echo get_template_directory_uri() ?>/images/call.png">-->
                <!--            <?php // echo do_shortcode('[contact type="other_mobile2"]') ?>-->
                <!--        </a>-->
                <!--    </address>-->
                <!--    <div class="clearfix"></div>-->
                <!--</div>-->
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--End contact  section7-->
<?php get_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/custome.js"></script>