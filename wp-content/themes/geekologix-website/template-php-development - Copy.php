<?php 
//Template Name: PHP Development
echo get_header();
?>
<!-- Start section 1 -->
<section class="mobile_section">
	<div class="mobile_wearables">
		<?php include 'header2.php'; ?>
		<div class="container px-0 common_heading  detail_heading">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-xl-6">
					<h1 class="banner-heading">
						<?php 
              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
						<!--  <div id="text-type"></div> -->
					</h1>
					<p>
						PHP Innovations for boosting your sales!
					</p>

					<a href="<?php echo get_permalink('288'); ?>" class="text-uppercase requst_quote common_btns"
						title="Request a quote">Request a
						quote</a>
					<!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
					<!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
				</div>
				<div class="col-lg-6 col-md-6 col-xl-6">
					<img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
						class="img-fluid mx-auto banner_img">
					<!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End section 1 -->

<!-- Start Web Application  -->
<section class="web-application">
	<div class="container px-0">
		<div class="row">
			<div class="col-12">
				<h2 class="comman_h text-center">PHP Development Service</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="web-application-img text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/images/php-development-left.jpg" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h5>
					Our Team of Highly Skilled PHP Developers Enables Us to Deliver Creative and Result Oriented PHP Web
					Development Services to Serve Your Businesses.
				</h5>
				<p class="comman_p">
					PHP was released in 1994, and even after 26 years, it is still among the most popularly used
					programming languages, all thanks to its exceptional and inclusive plug & play features. PHP
					furnishes your website with amazing flexibility and maintenance along with the ability to execute
					complex functions.
				</p>
				<p class="comman_p">
					Our expert and experienced PHP programmers at Geekologix are pioneers of the PHP development
					industry and will fulfil your project requirements upto a T. Our skilled PHP developers are
					proficient at using best-in-class PHP technologies to offer state-of-art development services. Not
					just this, but they also possess great technical expertise in HTML5, CSS, jQuery, mySQL and more,
					offering an advantage to your development.
				</p>
			</div>
		</div>
	</div>
</section>
<!-- End Web Application  -->
<!-- Start Geekologix iOS Advantage -->
<section class="geekologix-ios-advantage">
	<div class="container px-0">
		<div class="row">
			<div class="col-12">
				<h3 class="comman_h text-center">Why Choose Geekologix?</h3>
			</div>
		</div>
		<div class="php-development-cards">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="High Engagement Rate">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/high-engagement-rate.jpg"
									alt="">
							</div>
							<h5>High Engagement Rate</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="High Secure Web Development">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/high-secure-web-development.jpg"
									alt="">
							</div>
							<h5>High Secure Web Development</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="Cohesive Environment">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/cohesive-environment.jpg"
									alt="">
							</div>
							<h5>Cohesive Environment</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="Latest Tools and Technologies">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/latest-tools.jpg" alt="">
							</div>
							<h5>Latest Tools and Technologies</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="Agile Development Methodology">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/agile-development-php.jpg"
									alt="">
							</div>
							<h5>Agile Development Methodology</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="Quality Assurance">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/quality-assurance.jpg"
									alt="">
							</div>
							<h5>Quality Assurance</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="Strict Deadlines">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/quick-turnaround.jpg"
									alt="">
							</div>
							<h5>Strict Deadlines</h5>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<a href="javascript:void(0)" title="Flexible Engagement Models">
						<div class="php-development-card">
							<div class="php-development-card-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/flexible-engagement.jpg"
									alt="">
							</div>
							<h5>Flexible Engagement Models</h5>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Geekologix iOS Advantage -->
<!-- Start What makes Php  -->
<section class="what-makes-php">
	<div class="container px-0">
		<div class="row">
			<div class="col-12">
				<h3 class="comman_h text-center">What Makes PHP Best For Your Web App?</h3>
			</div>
		</div>
		<div class="php-content-cards">
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<div class="php-content-card">
						<h5>Cost-Effective</h5>
						<p>PHP is open-source which means it is free to use. This greatly decreases the development
							costs associated with it.</p>
					</div>
					<div class="php-content-card">
						<h5>Easy to Integrate</h5>
						<p>PHP has multiple open source MVC development frameworks that reduce development time as well
							as time to market for businesses.</p>
					</div>
					<div class="php-content-card">
						<h5>Stable</h5>
						<p>Since PHP has been in the market since a long time, it has had several updates and fixes
							making it a highly stable programming language.</p>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 php-center-img">

					<img src="<?php echo get_template_directory_uri(); ?>/images/php-laptop-center.jpg" alt="">

				</div>
				<div class="col-md-6 col-lg-4">
					<div class="php-content-card">
						<h5>Secure</h5>
						<p>PHP offers a multiple layers architecture that provides data security to the end user while
							preventing any malicious attacks. This makes PHP a very safe and secure choice for your
							projects.</p>
					</div>
					<div class="php-content-card">
						<h5>Supports All the Significant Databases</h5>
						<p>PHP efficiently supports all the popular databases like MySQL, MSSQL, PostGres, SQLite and
							NoSQL.</p>
					</div>
					<div class="php-content-card">
						<h5>Efficient Scripting language</h5>
						<p>PHP is a highly robust scripting language as it can be used to design responsive & dynamic
							web pages that can handle high load.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End What makes Php  -->
<!-- Start Section 3 we serve -->
<section class="we_serve php-expertise">
	<div class="container px-0">
		<div class="row text-center ">
			<div class="col-12">
				<h4 class="inner_heading">PHP Expertise for any Web Endeavor</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Real Estate">
					<div class="industry_block real_state_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images real_state mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Real Estate</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Education">
					<div class="industry_block education_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images education mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Education</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Healthcare">
					<div class="industry_block healthcare_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images healthcare mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Healthcare</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Finance">
					<div class="industry_block finance_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images finance mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Finance</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Automotives">
					<div class="industry_block automotives_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images automotives mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Automotives</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Gaming">
					<div class="industry_block gaming_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images gaming mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Gaming</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Event & Tickets">
					<div class="industry_block event_tickets_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png"
								alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images event_tickets mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Event & Tickets</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Travel & Tours">
					<div class="industry_block travel_tours_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images travel_tours mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Travel & Tours</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Ecommerce">
					<div class="industry_block ecommerce_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images ecommerce mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Ecommerce</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Entertainment">
					<div class="industry_block entertainment_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- 	<div class="industry_images entertainment mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Entertainment</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Food & Beverage">
					<div class="industry_block food_beverage_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png"
								alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png"
								alt="" class="img_hover mx-auto">
						</div>
						<!-- <div class="industry_images food_beverage mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Food & Beverage</h5>
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-6 col-md-3">
				<a href="" title="Fitness">
					<div class="industry_block  fitness_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt=""
								class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt=""
								class="img_hover mx-auto">
						</div>
						<!-- 	<div class="industry_images fitness mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
						</div> -->
						<h5 class="text-capitalize">Fitness</h5>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<!-- End Section 3 we serve -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Tech Stack -->
<section class="our-tech-stack">
	<div class="container px-0">
		<div class="row">
			<div class="col-12">
				<h3 class="comman_h text-center">Our Tech Stack</h3>
			</div>
		</div>
		<div class="tech-cards tech-tabing">
			<div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-pills" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="pills-languages-tab" data-toggle="pill"
								href="#pills-languages" role="tab" aria-controls="pills-languages" aria-selected="true"
								title="Languages">Languages</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-frameworks-tab" data-toggle="pill" href="#pills-frameworks"
								role="tab" aria-controls="pills-frameworks" aria-selected="false"
								title="Frameworks">Frameworks</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-orms-tab" data-toggle="pill" href="#pills-orms" role="tab"
								aria-controls="pills-orms" aria-selected="false" title="ORMs">ORMs </a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-databases-tab" data-toggle="pill" href="#pills-databases"
								role="tab" aria-controls="pills-databases" aria-selected="false"
								title="Databases">Databases </a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-architecture-tab" data-toggle="pill"
								href="#pills-architecture" role="tab" aria-controls="pills-architecture"
								aria-selected="false" title="Architecture">Architecture </a>
						</li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-languages" role="tabpanel"
							aria-labelledby="pills-languages-tab">

							<div class="row">
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
													alt="">
											</div>
											<h5>Visual C# .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual Basic .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
													alt="">
											</div>
											<h5>Visual Basic .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C++ .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
													alt="">
											</div>
											<h5>Visual C++ .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual J# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
													alt="">
											</div>
											<h5>Visual J# .Net</h5>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-frameworks" role="tabpanel"
							aria-labelledby="pills-frameworks-tab">
							<div class="row">
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
													alt="">
											</div>
											<h5>Visual C# .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual Basic .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
													alt="">
											</div>
											<h5>Visual Basic .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C++ .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
													alt="">
											</div>
											<h5>Visual C++ .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual J# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
													alt="">
											</div>
											<h5>Visual J# .Net</h5>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-orms" role="tabpanel" aria-labelledby="pills-orms-tab">
							<div class="row">
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
													alt="">
											</div>
											<h5>Visual C# .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual Basic .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
													alt="">
											</div>
											<h5>Visual Basic .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C++ .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
													alt="">
											</div>
											<h5>Visual C++ .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual J# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
													alt="">
											</div>
											<h5>Visual J# .Net</h5>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-databases" role="tabpanel"
							aria-labelledby="pills-databases-tab">
							<div class="row">
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
													alt="">
											</div>
											<h5>Visual C# .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual Basic .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
													alt="">
											</div>
											<h5>Visual Basic .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C++ .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
													alt="">
											</div>
											<h5>Visual C++ .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual J# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
													alt="">
											</div>
											<h5>Visual J# .Net</h5>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-architecture" role="tabpanel"
							aria-labelledby="pills-architecture-tab">
							<div class="row">
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c.jpg"
													alt="">
											</div>
											<h5>Visual C# .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual Basic .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-basic.jpg"
													alt="">
											</div>
											<h5>Visual Basic .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual C++ .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-c-plus-plus.jpg"
													alt="">
											</div>
											<h5>Visual C++ .Net</h5>
										</div>
									</a>
								</div>
								<div class="col-6 col-lg-3">
									<a href="javascript:void(0)" title="Visual J# .Net">
										<div class="tech-card">
											<div class="tech-logo-img">
												<img src="<?php echo get_template_directory_uri(); ?>/images/visual-j.jpg"
													alt="">
											</div>
											<h5>Visual J# .Net</h5>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Start Tech Stack -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
	<div class="container px-0">
		<div class="row">
			<h4 class=" inner_heading mx-auto">Client's Reviews</h4>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-theme" id="client_slider">
					<!-- start loop -->
					<?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
					<div class="item">
						<div class="client_block">
							<div class="client_img">
								<img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
							</div>


							<?php echo the_content(); ?>

						</div>

					</div>

					<?php 
                    endwhile;
                } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
	<div class="container px-0">
		<div class="row">
			<div class="col-lg-7 col-xl-7">
				<h4 class="comman_h">Hire PHP Developers</h4>
				<h5>Geekologix is one of the globally acclaimed providers of cost-effective PHP solutions. </h5>
				<p>Our engineers are well-versed with the advanced tools and technologies that transform your ideas into
					solutions, with perfection. If you have a project idea that has potential to bring great disruptions
					in industry tomorrow, get in touch with our innovators today.</p>
				<div class="hire-dedicated-btn">
					<a href="<?php echo get_permalink('288'); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
				</div>
			</div>
			<div class="col-lg-5 col-xl-5">
				<div class="hire-app-developer-img">
					<img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Hire Android App Developers -->
<section class="lets_banner">
	<div class="container px-0">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
				<h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
				<a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
			</div>
		</div>
	</div>
</section>
<!-- start Inner footer -->
<section class="common_footer">
	<div class="container px-0">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
				<p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7  col-md-7 pr-0">
				<div class="form_part">
					<div class="row">
						<?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
					</div>
					<!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
				</div>
			</div>
			<div class="col-lg-5  col-md-5 pl-0">
				<div class="right_side">
					<h5 class="text-uppercase">Reach Us</h5>
					<ul>
						<li>
							<i class="fal fa-map-marker-alt float-left"></i>
							<p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
							<div class="clearfix"></div>
						</li>
						<li>
							<i class="fal fa-envelope"></i>
							<a class="float-left"
								href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
								<?php echo do_shortcode('[contact type="email_address"]') ?></a>
							<div class="clearfix"></div>
						</li>
						<li>
							<i class="fab fa-skype"></i>
							<a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
								<?php echo do_shortcode('[contact type="skype"]') ?></a>
							<div class="clearfix"></div>
						</li>
						<li>
							<img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
								class="fa_phone">
							<a class="float-left"
								href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
								India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
							<a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
								<?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
							<br>
							USA:
							<a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
								<?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
							<div class="clearfix"></div>
						</li>
					</ul>
					<div class="offices">
						<div class="float-left">
							Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
								class="ml-2"><span class="india_div">India</span>
						</div>
						<div class="float-left usa-div">

							<img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
								class="">USA</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
	<div class="footer-overlay"></div>
	<div class="container p-0">
		<div class="row">
			<div class="col-lg-12">
				<h4 class="page-title text-uppercase text-center">
					<?php echo  $conatct_title ?>
				</h4>
			</div>
			<!-- Let's talk about you, -->
			<div class="col-lg-12 p-lg-0">
				<div class="let-talk-about">
					<?php echo $conatct_content; ?>
					<!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
				</div>
			</div>
			<div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
				<div class="footer-responsive">
					<div class="float-icon">
						<img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
					</div>
					<address class="address-footer address-footer2">
						<h5>
							<?php echo do_shortcode('[contact type="office_name"]') ?>
						</h5>
						<a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
							<i class="fal fa-phone"></i>
							<?php echo do_shortcode('[contact type="india mobile"]') ?>
						</a>
						<i class="fal fa-map-marker-alt"></i>
						<p>
							<?php echo do_shortcode('[contact type="office_address"]') ?>
						</p>
						<div class="clearfix"></div>
						<a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
							<i class="fal fa-envelope"></i>
							<?php echo do_shortcode('[contact type="email_address"]') ?>
						</a>
						<a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
							<i class="fab fa-skype"></i>
							<?php echo do_shortcode('[contact type="email_address"]') ?>
						</a>
						<div class="clearfix"></div>
					</address>
					<div class="clearfix"></div>
				</div>

			</div>
			<div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
				<div class="footer-add">
					<div class="float-icon">
						<img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
					</div>
					<address class="address-footer">
						<h5>
							<?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
						</h5>
						<a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
							<img src="<?php echo get_template_directory_uri() ?>/images/call.png">
							<?php echo do_shortcode('[contact type="other_mobile2"]') ?>
						</a>
					</address>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
				<div class="footer-add">
					<div class="float-icon">
						<img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
					</div>
					<address class="address-footer">
						<h5>
							<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
						</h5>
						<a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
							<img src="<?php echo get_template_directory_uri() ?>/images/call.png">
							<?php echo do_shortcode('[contact type="other_mobile"]') ?>
						</a>
					</address>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row copy-footer">
			<div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
				<p class="copy-right">
					&copy;
					<?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
				</p>
			</div>
			<div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
				<ul>
					<li>
						<a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
							title="facebook"> <i class="fab fa-facebook-f"></i></a>
					</li>
					<!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
					<li>
						<a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
							title="linkedin"><i class="fab fa-linkedin-in"></i></a>
					</li>
					<li>
						<a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
							title="instagram"><i class="fab fa-instagram"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>