<?php
  // Template Name:Digital Marketing  Page
  the_post();
  /**
  * The main template file
  *
  * This is the most generic template file in a WordPress theme
  * and one of the two required files for a theme ( the other being style.css ).
  * It is used to display a page when nothing more specific matches a query.
  * E.g., it puts together the home page when no home.php file exists.
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package geekologix_website
  */
  
  get_header(); ?>

  <div id="" class="mobile_wearables">
<?php include 'header2.php'; ?>
    <div class="container-fluid common_heading  detail_heading">
      <div class="row">
        <div class="col-lg-6 col-md-6 pr-0">
          <h1 class="banner-heading ">
            <?php 

 $about_ID = 168;
    $about_title = get_page($about_ID);
             echo the_title();?>
            <!--  <div id="text-type"></div> -->
          </h1>
          <?php echo the_content(); ?>
          <a href="" class="text-uppercase requst_quote common_btns">Request a quote</a>
    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
          <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
        </div>
             <div class="col-lg-6 col-md-6">
 <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>" class="img-fluid mx-auto banner_img">             </div>
      </div>
    </div>
  </div>
</section>
<!-- End section 1 -->
<!-- start Section 2  Top Mobile -->
<section class="top_mobile common_sections">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading">Top Mobile App Development Services</h4>
        <p class="inner_content">Our software development agency develops iOS and Android mobile apps using the same tools and techniques applied for the top apps on the Apple App Store and Google Play Store.  We ensure that your custom mobile app meets all the requirements we’ve agreed on, and we’ll help you add new features to your app once we launch it. Meanwhile we work in your own repository, so your code will always stay with you.

</p>
      </div>
    </div>
    <div class="row">
                    <!-- start loop -->
            <?php
                $topMobile =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'top_mobile', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getMobileData = new WP_Query($topMobile);
                   // $count = 1;
                 
                  if($getMobileData->have_posts()) {
                  while ($getMobileData->have_posts()) : $getMobileData->the_post();
                  $get_mobile_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                  
                    $mobileimage = $get_mobile_image[0];
                    ?>
      <div class="col-lg-4 col-md-4">
        <div class="development_block">
          <img src="<?php  echo $mobileimage;?>" alt="<?php echo the_title(); ?>">
          <h5 class=""><?php echo the_title(); ?></h5>
        </div>
      </div>
      <?php
    endwhile;
  }
      ?>
  <!--          <div class="col-lg-4">
              <div class="development_block">
          <img src="<?php  echo get_template_directory_uri() ?>/images/ios-develop.png" alt="">
          <h5 class="text-capitalize">iOS App Development</h5>
        </div>
            </div>
  <div class="col-lg-4">
    <div class="development_block">
          <img src="<?php  echo get_template_directory_uri() ?>/images/ios-develop.png" alt="">
          <h5 class="text-capitalize">Cross Platform Apps</h5>
        </div>
  </div> -->
    </div>
  </div>
</section>
<!-- End section 2  Services-->
<!-- Start Section 3 we serve -->
<section class="we_serve common_sections">
  <div class="container">
    <div class="row text-center ">
      <h4  class="text-uppercase inner_heading mx-auto">Industries we serve</h4>
    </div>
    <div class="row">
    
      <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block real_state_outer text-center">
          <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images real_state mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Real Estate</h5>
        </div>
      </div>
  <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block education_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images education mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Education</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block healthcare_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images healthcare mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Healthcare</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block finance_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images finance mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Finance</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block automotives_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images automotives mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Automotives</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block gaming_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images gaming mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Gaming</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block event_tickets_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images event_tickets mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Event & Tickets</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block travel_tours_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images travel_tours mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Travel & Tours</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block ecommerce_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images ecommerce mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Ecommerce</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block entertainment_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png" alt="" class="img_hover mx-auto">
          </div>
        <!--  <div class="industry_images entertainment mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Entertainment</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block food_beverage_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png" alt="" class="img_hover mx-auto">
          </div>
          <!-- <div class="industry_images food_beverage mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Food & Beverage</h5>
        </div>
      </div>
        <div class="col-lg-2 col-6 col-md-3">
        <div class="industry_block  fitness_outer text-center">
            <div class=" mx-auto">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt="" class="img_desktop">
              <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt="" class="img_hover mx-auto">
          </div>
        <!--  <div class="industry_images fitness mx-auto">
            <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
          </div> -->
          <h5 class="text-capitalize">Fitness</h5>
        </div>
      </div>
      <a href="" class="common_btns let_started text-uppercase mx-auto">Let's Get Started</a>
    </div>
  </div>
</section>
<!-- End Section 3 we serve -->
<!-- Start Section 4 building app -->
<section class="building_app common_sections">
  <div class="container">
    <div class="row ">
      <h4 class="text-uppercase inner_heading mx-auto">Building Perfact Apps For Your Success</h4>
    </div>
    <div class="row">
            <!-- start loop -->
            <?php
                $features =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'building_perfact', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getfeaturesData = new WP_Query($features);
                  if($getfeaturesData->have_posts()) {
                  while ($getfeaturesData->have_posts()) : $getfeaturesData->the_post();
                  $get_features_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                    $featuresimage = $get_features_image[0];
                    ?>
      <div class="col-lg-6 col-md-6">
        <div class="building_block">
          <img src="<?php  echo  $featuresimage;?>" alt="<?php echo the_title(); ?>">
        <h5><?php echo the_title(); ?></h5>
<?php echo the_content(); ?>
        </div>
      </div>
<?php endwhile; }?>
    </div>
  </div>
</section>
<!-- End Section 4 building app -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start section 6 app development -->
<section class="mobile_devlop common_sections ">
  <div class="container">
    <div class="row">
    <h4 class="text-uppercase inner_heading mx-auto">Our App Development Process</h4>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 mobile_steps pr-0">
        <div class="mobile_develops_inner">
      
          <img src="<?php echo get_template_directory_uri()?>/images/research.png" >
<img src="<?php echo get_template_directory_uri()?>/images/icon/shape1.png" class="shapes1 shapes">
      
        <img class="steps_div step_1" src="<?php echo get_template_directory_uri()?>/images/1.png">
        <div class="step_box">

        <h5>Research</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
      </div>
      </div>
      </div>
      <div class="col-lg-4 col-md-4">
          <div class="mobile_develops_inner">
      
          <img src="<?php echo get_template_directory_uri()?>/images/wireframing.png">

<img src="<?php echo get_template_directory_uri()?>/images/icon/shape-copy.png" class="shapes2 shapes">
        
        <img class="steps_div step_2" src="<?php echo get_template_directory_uri()?>/images/2.png">
        <div class="step_box">

        <h5>Wireframing</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
      </div>
      </div>
      </div>
      <div class="col-lg-4 col-md-4">
              <div class="mobile_develops_inner">
      
          <img src="<?php echo get_template_directory_uri()?>/images/ui-design.png" alt="ui & ux Design">
<img src="<?php echo get_template_directory_uri()?>/images/icon/shape2.png" class="shapes3 shapes">
       
        <img class="steps_div step_4" src="<?php echo get_template_directory_uri()?>/images/3.png">
        <div class="step_box">
        <h5>UI/UX design</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
      </div>
      </div>
      </div>
    
      <div class="col-lg-4 col-md-4">
                  <div class="mobile_develops_inner">
              <img src="<?php echo get_template_directory_uri()?>/images/launch & maintenance.png" alt="">
<img src="<?php echo get_template_directory_uri()?>/images/icon/shape1.png" class="shapes3 shapes">
        
        <img class="steps_div step_4" src="<?php echo get_template_directory_uri()?>/images/6.png">
            <div class="step_box">

        <h5>Launch & Maintenance</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
        </div>
      </div>
      </div>
      <div class="col-lg-4 col-md-4">
                        <div class="mobile_develops_inner">
      
          <img src="<?php echo get_template_directory_uri()?>/images/testing-qa.png" alt="Testing and QA">
<img src="<?php echo get_template_directory_uri()?>/images/icon/shape-copy.png" class="shapes3 shapes">
        
        <img class="steps_div step_5" src="<?php echo get_template_directory_uri()?>/images/5.png">
        <div class="step_box">
        <h5>Testing and QA</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
      </div>
      </div>
      </div>
      <div class="col-lg-4 col-md-4">
                        <div class="mobile_develops_inner">
        
          <img src="<?php echo get_template_directory_uri()?>/images/development-img.png" alt="Development">
        
        <img class="steps_div step_4 step_6" src="<?php echo get_template_directory_uri()?>/images/4.png">
        <div class="step_box">
        <h5>Development</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
      </div>
      </div>
      </div>
        <a href="" title="LET’S DISCUSS YOUR PROJECT" class="common_btns let_started text-uppercase mx-auto">Let's Discuss Your Project</a>
    </div>
  </div>
</section>
<!-- Start Section 7 client review -->
<section class="client_review common_sections common_sliders ">
  <div class="container">
    <div class="row">
    <h4 class="text-uppercase inner_heading mx-auto">Client's Reviews</h4>
    </div>
      <div class="row">
                      <div class="owl-carousel owl-theme" id="client_slider">
                                     <!-- start loop -->
                <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                        <div class="item">
                                        <div class="client_block">
                                          <div class="client_img">
                                               <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                                          </div>
                                          
                                          
                                              <?php echo the_content(); ?>
                                            
                                        </div>
                     
                     </div>
                    
                  <?php 
                    endwhile;
                } ?>
               
                      </div>
    </div>
  </div>
</section>
<!-- End Section 7 client review -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner common_sections">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Ready to discuss your project?</h4>
        <a href="" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
      </div>
    </div>
  </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- Start section 9 Technology Framework-->
<section class="technology_framework common_sections">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Technologies & Frameworks</h4>
      </div>
    </div>
    <div class="row">
            <!-- start loop -->
               <div class="owl-carousel owl-theme" id="framework_slider">
            <?php
                $features =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'frameworks', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getfeaturesData = new WP_Query($features);
                  if($getfeaturesData->have_posts()) {
                  while ($getfeaturesData->have_posts()) : $getfeaturesData->the_post();
                  $get_features_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                    $featuresimage = $get_features_image[0];
                    ?>
          <div class="item text-center">
        <div class="framework_block">
               <img src="<?php  echo $featuresimage;?>" alt="<?php echo the_title(); ?>" class="mx-auto">
          <h5><?php echo the_title(); ?></h5>
        </div>
      </div>
        <?php 
               endwhile;
            } ?>
    
    </div>
  </div>
</section>
<!-- End Section 9 Technology Framework -->
<!-- start Inner footer -->
<section class="common_footer common_sections">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
        <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-7  col-md-7 pr-0">
        <div class="form_part">
          <div class="row">
            <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
          </div>
      <!--    <form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
        </div>
      </div>
      <div class="col-lg-5  col-md-5 pl-0">
        <div class="right_side">
          <h5 class="text-uppercase">Reach Us</h5>
          <ul>
            <li>
              <i class="fal fa-map-marker-alt float-left"></i>
              <p class="float-left">  <?php echo do_shortcode('[contact type="office_address"]') ?></p>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fal fa-envelope"></i>
              <a class="float-left" href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>"> <?php echo do_shortcode('[contact type="email_address"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fab fa-skype"></i>
              <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>"> <?php echo do_shortcode('[contact type="skype"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fal fa-phone"></i>
              <a class="float-left" href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>"> India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
              <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>"> <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
              <br>
              USA:
                <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>"> <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                <div class="clearfix"></div>
            </li>
          </ul>
          <div class="offices">
            <div class="float-left">
              Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india" class="ml-2"><span class="india_div">India</span>
            </div>
            <div  class="float-left usa-div">
            
            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span class="">USA</span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- End Inner footer -->
<!--End contact  section7-->
<?php get_footer(); ?> 
<script type="text/javascript">

     $('#project_slider').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    center:true,
    lazyLoad: true,
     dots:true,
  responsive:{
        0:{
            items:1,
            stagePadding: 60
        },
        600:{
            items:1,
            stagePadding: 100
        },
        1000:{
            items:2,
            // stagePadding: 100
        },

    }
})
     
         $('#framework_slider').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    // center:true,
    // lazyLoad: true,
     dots:false,
  responsive:{
        0:{
            items:2,
                    autoplay:true,
autoplayTimeout:3000,
        },
        600:{
              autoplay:true,
autoplayTimeout:3000,
            items:3,
        },
        1000:{
            items:6,
            // stagePadding: 100
        },

    }
})
         $('#client_slider').owlCarousel({
    loop:true,
    margin:30,
    autoplay:false,
    dots:true,
autoplayTimeout:3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
})
</script>