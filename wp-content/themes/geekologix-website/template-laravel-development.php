<?php
//Template Name:Laravel Development
the_post();
get_header();
?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h1 class="banner-heading">
                        <?php 

              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>Build Powerful Web Apps and Applications with expert Laravel Developers at Geekologix.</p>
                    <!-- <?php echo the_content(); ?> -->
                    <a href="" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">Custom Laravel Web & App Development Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-application-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    Developing robust Laravel solutions with innovation and passion, for your business requirements.
                </h5>
                <p class="comman_p">
                    Laravel allows the developers to create web apps and applications with ease, in a short time, given
                    its superior code foundations, resilient features, low complexity and robust structure. Laravel
                    Developers at Geekologix are passionate about programming and are glad to build custom Laravel
                    solutions to help you empower your business with digital assets.
                </p>
                <p class="comman_p">
                    Our Laravel developers are experienced, not just in building from scratch, but also in migrating to
                    Laravel framework from other frameworks smoothly. Having the knowledge and expertise in various
                    fields and industries allows our developers to think with a wider perspective and come up with the
                    most innovative solutions for you, excelling at the respective industry standards giving and you an
                    edge over your competitors.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start What Makes Laravel -->
<section class="what-makes-larawel">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Why Choose Laravel for Web & App Development?</h3>
                <p class="comman_p text-center">
                    Laravel gives you several advantages over other PHP frameworks, making it a superior and one of the
                    most popular frameworks.
                </p>
            </div>
        </div>
        <div class="laravel-cards">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Artisan">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/artisen-laravel.png"
                                    alt="">
                            </div>
                            <h5>Artisan</h5>
                            <p>Laravel facilitates an accelerated development with the help of its CLI function which
                                makes it easier for the developers to implement programs that are repetitive and long.
                                This feature is called Artisan.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Modular">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/security-laravel.png"
                                    alt="">
                            </div>
                            <h5>Modular</h5>
                            <p>Laravel is made on the modular structure. This means that the programs developed in
                                Laravel will be highly responsive, user-friendly and easy to maintain.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="MVC Architecture Support">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/module-laravel.png" alt="">
                            </div>
                            <h5>MVC Architecture Support</h5>
                            <p>Laravel also follows the MVC model of architecture, which makes the programs developed in
                                Laravel perform better, and provides better documentation when it comes to built-in
                                functions.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Libraries">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/libraries-laravel.png"
                                    alt="">
                            </div>
                            <h5>Libraries</h5>
                            <p>Laravel is a superior framework and consists of a wider range of libraries, making it
                                more development friendly and contributing to a higher scope of applications for
                                Laravel.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Migration System">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/migration-laravel.png"
                                    alt="">
                            </div>
                            <h5>Migration System</h5>
                            <p>One of the best things about Laravel is that it allows an easy migration of databases,
                                which means that you don’t have to recreate them every time you need to use them.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Eloquent ORM">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/eloquent-laravel.png"
                                    alt="">
                            </div>
                            <h5>Eloquent ORM</h5>
                            <p>Laravel provides an amazing and easy way to implement ActiveRecord while working with
                                databases, making the process quicker than any other frameworks.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Template Engine">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/mvc-laravel.png" alt="">
                            </div>
                            <h5>Template Engine</h5>
                            <p>Laravel also provides a variety of in-built templates which can be used with slight
                                modifications and are compatible with majority of JS frameworks.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Template Engine">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/template-engine-laravel.png"
                                    alt="">
                            </div>
                            <h5>Template Engine</h5>
                            <p>Laravel also provides a variety of in-built templates which can be used with slight
                                modifications and are compatible with majority of JS frameworks.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="javascript:void(0)" title="Unit Testing">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/unit-testing-laravel.png"
                                    alt="">
                            </div>
                            <h5>Unit Testing</h5>
                            <p>Laravel offers unit testing of the codes, which makes it easier to find bugs, and makes
                                the execution smooth and seamless.</p>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- End What Makes Laravel -->
<!-- Start Laravel Development Edge -->
<section class="laravel-development-edge">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comma_h text-center">Geekologix as your Laravel Development Partner</h3>
                <p class="comma_p text-center">Offering a wide range of Laravel Solutions to fulfil your web & App
                    development requirements with smooth experience.</p>
            </div>
        </div>
        <div id="accordion" class="myaccordion laravel-accordian">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne" title="Flexible Hiring">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/experienced-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Flexible Hiring
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Geekologix is where you can choose the developers that you want to work with by
                                    going through their resumes or interviewing them. Your comfort is our priority.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="Pocket-Friendly App Development">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/top-notch-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Pocket-Friendly App Development
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Geekologix promises to provide you the best deals for your Laravel development
                                    needs. We provide scalable solutions fitting your budget criterion.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree" title="Industry Experience">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/focus-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Industry Experience
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Laravel developers at Geekologix have been working for a long time on various
                                    different projects across industries and geographical regions, contributing to their
                                    knowledge and expertise.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure" title="Creativity, Skills and Expertise">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/scalable-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Creativity, Skills and Expertise
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Our developers programme with passion and creativity, to bring you the most
                                    innovative and top-notch solutions tailored to your needs and requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                    aria-controls="collapseFive" title="Working on prerequisites">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/cost-effective-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Working on prerequisites
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We value your time and money, and hence work within the limits of constraints set by
                                    you in terms on deadlines, timelines and budget of the overall project.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseSix" aria-expanded="false"
                                    aria-controls="collapseSix" title="Support and Maintenance">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/client-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Support and Maintenance
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Our team managers are always available to resolve your queries and assist you with
                                    your doubts regarding your project, or about our Laravel developers, team or
                                    services, even after the project is officially closed.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card">
                        <div class="card-header" id="headingSeven">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false"
                                    aria-controls="collapseSeven" title="Enhanced Security Procedures">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/enhanced-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Enhanced Security Procedures
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We offer repid ecommerce development to helps our clients leverage high-performing
                                    B2B and B2C solutions.
                                </p>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="headingEight">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseEight" aria-expanded="false"
                                    aria-controls="collapseEight" title="Fully Transparent Procedure">
                                    <div class="laravel-faq-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/fully-laravel-faq.png"
                                            alt="">
                                    </div>
                                    Fully Transparent Procedure
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    We offer repid ecommerce development to helps our clients leverage high-performing
                                    B2B and B2C solutions.
                                </p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Development Edge -->
<!-- Start Tech Stack -->
<section class="our-tech-stack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Our Tech Stack</h3>
            </div>
        </div>
        <div class="tech-cards">
            <div class="row">
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Artisan">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/artisan-logo.jpg" alt="">
                            </div>
                            <h5>Artisan</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Elixir">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/elixir-logo.jpg" alt="">
                            </div>
                            <h5>Elixir</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Lumen">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/lumen.jpg" alt="">
                            </div>
                            <h5>Lumen</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="PHP Storm">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/php-storm-logo.jpg" alt="">
                            </div>
                            <h5>PHP Storm</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Codelobster">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/codelobster-logo.jpg"
                                    alt="">
                            </div>
                            <h5>Codelobster</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Sublime Text">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/sublime-text-logo.jpg"
                                    alt="">
                            </div>
                            <h5>Sublime Text</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Netbeans">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/netbeans-logo.jpg" alt="">
                            </div>
                            <h5>Netbeans</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="ATOM">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/atom-logo.jpg" alt="">
                            </div>
                            <h5>ATOM</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start Tech Stack -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Laravel Key offerings -->
<section class="laravel-key-offerings">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Laravel Development Services by Geekologix</h3>
            </div>
        </div>
        <div class="laravel-offering-cards">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Custom Laravel Web Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-1">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-custom-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Custom Laravel Web Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Custom Laravel App Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-3">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-restful-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Custom Laravel App Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Laravel Template Design & Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-template-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Laravel Template Design & Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Multilingual & SaaS App Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-6">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/multilingual-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Multilingual & SaaS App Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Laravel Extension Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-2">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-extension-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Laravel Extension Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Laravel Package Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-5">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-package-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Laravel Package Development</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Key offerings -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Expert Laravel Developers at Geekologix</h4>
                <h5>Delivering seamless Laravel Development experience to clients across industries.</h5>
                <p>
                    At Geekologix, we use advanced technologies and tools to build powerful web & app development
                    services that helps you to enrich and empower your business goals, and contribute in your business
                    growth and expansion.</p>
                <div class="hire-dedicated-btn">
                    <a href="javascript:void(0)" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let’s empower each other with powerful digital assets
                    built in Laravel.</h4>
                <p class="inner_content">Our Laravel developers will meet all your expectations with their passion,
                    creativity and instincts.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>