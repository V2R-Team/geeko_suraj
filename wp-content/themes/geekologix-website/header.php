<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head>

* * @link
https://developer.wordpress.org/themes/basics/template-files/#template-partials
* * @package vidhyashram_public_school */ $site_url = site_url(); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-UTF-8" />
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <link rel="icon" href="<?php echo get_template_directory_uri() ?>/images/fav.png" type="image/gif" sizes="16x16" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet preload" as="style" href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css">
    <!--     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css" />
 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/layouts/style.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/layouts/responsive.css" />
    <link
        href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800&display=swap"
        rel="stylesheet" />

    <link rel="stylesheet preload" as="style"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" />
    <link rel="stylesheet preload" as="style"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
    <div id="fullpage">
       