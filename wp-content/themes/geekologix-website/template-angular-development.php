<?php
//Template Name:Angular Development
  the_post();
  get_header();
?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xl-7">
                    <h1 class="banner-heading">
                        <?php 

              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>Spectacular front-end development with Angular experts at Geekologix. </p>
                    <!-- <?php echo the_content(); ?> -->
                    <a href="<?php echo get_permalink('319'); ?>/#showcase" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6 col-xl-5">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">Custom AngularWeb Development Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/angular-development-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    Google developed Angular as an open source framework for web development to allow programmers to
                    build Single page applications (SPAs) with ease and faster development.
                </h5>
                <p class="comman_p">
                    Angular allows a seamless experience of developing front-end for web apps and browser based
                    application using the MVC capability. Angular is designed in such a way that it facilitates
                    developers in faster and easier development of front end through command line tools, web templates
                    and IDEs.
                </p>
                <p class="comman_p">
                    Angular also works across most platforms used today, hence helps the developers to improvise the
                    user experience by controlling speed, performance, display and functionality of the front end for
                    any web applications.
                </p>
                <p class="comman_p">
                    Angular Experts at Geekologix, one the best Angular Development Companies in India, focus on a
                    flawless user experience, while keeping in mind the requirements of our clients to highlight
                    sections and keeping the functionality high with accelerated and compact front-end development.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start What Makes Laravel -->
<section class="what-makes-larawel">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Why Choose Angular for Front-end Development?</h3>
                <p class="comman_p text-center">
                    Angular is the most popular framework for front-end development worldwide. It is popular for a
                    reason, for all the advantages that it gives over other competitive frameworks.
                </p>
            </div>
        </div>
        <div class="laravel-cards">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="Cross Platform Support">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/artisen-laravel.png"
                                    alt="">
                            </div>
                            <h5>Cross Platform Support</h5>
                            <p>
                                Angular is capable of supporting the development of various web applications for native
                                as well as cross platform with the same efficiency.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="Rich User Experience">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/libraries-laravel.png"
                                    alt="">
                            </div>
                            <h5>Rich User Experience</h5>
                            <p>Angular uses the caching mechanism which eventually reduces the load on server as well as
                                improvises the performance greatly.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="Server Side Rendering">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/mvc-laravel.png" alt="">
                            </div>
                            <h5>Server Side Rendering</h5>
                            <p>Angular facilitates server side rendering, due to which it is easier to build SEO
                                friendly web applications using Angular Universal.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="High Reliability">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/unit-testing-laravel.png"
                                    alt="">
                            </div>
                            <h5> High Reliability</h5>
                            <p>The fact that Angular has been developed by Google makes it highly popular and reliable
                                for developers all across the world.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="Cross Browser Compatibility">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/eloquent-laravel.png"
                                    alt="">
                            </div>
                            <h5>Cross Browser Compatibility</h5>
                            <p>Angular facilitates the implementation of ActiveRecord while working with database, which
                                makes the process quick and hassle-free.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="Single Page Application (SPA)">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/module-laravel.png" alt="">
                            </div>
                            <h5>Single Page Application (SPA)</h5>
                            <p>Angular facilitates the development of single page applications in a very organised way
                                making it very preferable for the same.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="MVC & MVVM Support">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/security-laravel.png"
                                    alt="">
                            </div>
                            <h5>MVC & MVVM Support</h5>
                            <p>Angular allows the developers to manage the client side application development with
                                ease, using the MVC and MVVM models through the process of two-way data binding.</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="javascript:void(0)" title="Integrated Unit Testing">
                        <div class="laravel-card">
                            <div class="laravel-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/migration-laravel.png"
                                    alt="">
                            </div>
                            <h5>Integrated Unit Testing</h5>
                            <p>
                                Angular offers an integrated unit testing of the codes, with the help on in-build tools
                                like Jasmine, Protractor, Karma, etc
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End What Makes Laravel -->
<!-- Start Laravel Development Edge -->
<section class="laravel-development-edge ang-development-edge">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comma_h text-center">Geekologix as your Angular Development Partner</h3>
                <p class="comma_p text-center">Decked with experience and expertise to fulfil your app development
                    necessities with ease and passion.</p>
            </div>
        </div>
        <div class="ang-development-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Flexible Hiring">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/experienced-ang.jpg"
                                    alt="">
                            </div>
                            <h5>Flexible Hiring</h5>
                            <p>At Geekologix, you can choose who you want t work with. We allow you to choose you
                                developer or build a team of developers with our employees to help you with your angular
                                development.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Pocket-Friendly App Development">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/focus-ang.jpg" alt="">
                            </div>
                            <h5>Pocket-Friendly App Development</h5>
                            <p>
                                Geekologix is known for providing the best deals and the most affordable packages when
                                it comes to Web development with Angular.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Scalable and Robust Solutions">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/scalable-ang.jpg" alt="">
                            </div>
                            <h5>Scalable and Robust Solutions</h5>
                            <p>We foster the web applications that are future-ready to meet the expanding industry
                                requirements without compromising the quality standards.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Industry Experience">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/client-centric-ang.jpg"
                                    alt="">
                            </div>
                            <h5>Industry Experience</h5>
                            <p>Our Angular Developers have been working on various projects across industries and
                                geographical regions, allowing them a profound knowledge of front-end development.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Creativity, Skills and Expertise">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/flexible-engagement-ang.jpg"
                                    alt="">
                            </div>
                            <h5>Creativity, Skills and Expertise</h5>
                            <p>We don’t exceed any constraints put forth by our clients, be it time or money. We deliver
                                all our project on time and within the promised budgets.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Working on prerequisites">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/fully-transparent-ang.jpg"
                                    alt="">
                            </div>
                            <h5>Working on prerequisites</h5>
                            <p>We don’t exceed any constraints put forth by our clients, be it time or money. We deliver
                                all our project on time and within the promised budgets.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Support and Maintenance">
                        <div class="ang-development-card">
                            <div class="ang-development-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/support-maintenance-ang.jpg"
                                    alt="">
                            </div>
                            <h5>Support and Maintenance</h5>
                            <p>Our team managers are always available to resolve your queries or doubts from start to
                                end, or even after the project is closed. We help you through it all until you are fully
                                satisfied.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Development Edge -->
<!-- Start Tech Stack -->
<section class="our-tech-stack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Our Tech Stack</h3>
            </div>
        </div>
        <div class="tech-cards">
            <div class="row">
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="WebStorm">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/webstorm-logo.jpg" alt="">
                            </div>
                            <h5>WebStorm</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Aptana">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/aptana-logo.jpg" alt="">
                            </div>
                            <h5>Aptana</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Lumen">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/lumen.jpg" alt="">
                            </div>
                            <h5>Lumen</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Visual Studio Code">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/visual-studio-logo.jpg"
                                    alt="">
                            </div>
                            <h5>Visual Studio Code</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Sublime Text">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/sublime-text-logo.jpg"
                                    alt="">
                            </div>
                            <h5>Sublime Text</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="Netbeans">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/netbeans-logo.jpg" alt="">
                            </div>
                            <h5>Netbeans</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-lg-3">
                    <a href="javascript:void(0)" title="ATOM">
                        <div class="tech-card">
                            <div class="tech-logo-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/atom-logo.jpg" alt="">
                            </div>
                            <h5>ATOM</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start Tech Stack -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Laravel Key offerings -->
<section class="laravel-key-offerings">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Angular Development Services by Geekologix</h3>
            </div>
        </div>
        <div class="laravel-offering-cards">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Custom Angular Front-end Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-1">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-custom-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Custom Angular Front-end Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Custom Angular Web Development">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-2">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-extension-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Custom Angular Web Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="SPA Development with Angular">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-3">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-restful-offering.jpg"
                                    alt="">
                            </div>
                            <h5>SPA Development with Angular</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="PHP to Angular Migration">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-template-offering.jpg"
                                    alt="">
                            </div>
                            <h5>PHP to Angular Migration</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Attractive UI/UX design with Angular">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-5">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/laravel-package-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Attractive UI/UX design with Angular</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-4">
                    <a href="javascript:void(0)" title="Effective Display Solution using Angular">
                        <div class="laravel-offering-card">
                            <div class="laravel-offering-card-img icon-border-6">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/multilingual-offering.jpg"
                                    alt="">
                            </div>
                            <h5>Effective Display Solution using Angular</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Key offerings -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Expert Angular Developers at Geekologix</h4>
                <h5>Building high quality and SEO friendly web pages and apps with Angular development.</h5>
                <p>Now showcase your business online, or supplement your web development by collaborating with our
                    Angular experts, for a smooth experience and flawless front-end development services.</p>
                <div class="hire-dedicated-btn">
                    <a href="<?php echo get_permalink('288'); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Empowering businesses to showcase better and stand out
                    of the crowd.</h4>
                <p class="inner_content">Meet your project requirements with bug-free Angular development solutions by
                    Geekologix.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>