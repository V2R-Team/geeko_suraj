<?php 
// Template Name:Portfolio
get_header();
?>
<!-- Start Our Portfolio -->
<section class="mobile_section our-portfolio" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <h1 class="banner-heading">
                        Our Portfolio
                    </h1>
                    <p>Here are some examples of the excellent products that we’ve helped to send out into the world.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Portfolio -->
<section class="portfolio-list">
    <!-- Start Play Glam Bg  -->
    <div class="play-glam-bg">
        <div class="container px-0">
            <div class="row">
                <div class="col-12 col-lg-7 col-lg-7 d-lg-none d-xl-none">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/play-glam-left.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-5 col-xl-5">
                    <div class="portfolio-list-heading">
                        <h3>Play Glam</h3>
                        <p class="pt-8">
                            <strong>
                                PlayGlam is a Professional Beauty Salon providing a spectrum of beauty services at
                                the doorsteps.
                            </strong>
                        </p>
                        <p>
                            It delivers beauty masters at the client’s doorsteps rendering services that can range from
                            mere blow dry, to make-up applications, or just a simple nail paint change.
                        </p>
                        <ul class="view-study-btns">
                            <li>
                                <a href="<?php echo get_permalink(276); ?>" class="view-study-link" title="View Case Study">View Case
                                    Study
                                </a>
                            </li>
                            <li>
                                <a href="" title="Android" class="portfolio-icon-link">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/android-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Iphone">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apple-icon.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Website">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-7 col-lg-7 d-none d-lg-block">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/play-glam-left.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Play Glam Bg  -->
    <!-- Start PursueIt Bg  -->
    <div class="play-glam-bg pursueit-bg">
        <div class="container px-0">
            <div class="row">
                <div class="col-12 col-lg-7 col-xl-7">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/pursueit-left.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-5 col-xl-5">
                    <div class="portfolio-list-heading">
                        <h3>PursueIt</h3>
                        <p class="pt-8">
                            <strong>
                                PursueIt is an online Platform across UAE that assists individuals in discovering and
                                experiencing their interests.
                            </strong>
                        </p>
                        <p>
                            It offers various interests and allows to make a selection from it; learning Kite Surfing,
                            or dancing skills, or language or any other skillset.
                        </p>
                        <ul class="view-study-btns">
                            <li>
                                <a href="javascript:void(0)" class="view-study-link" title="View Case Study">View Case
                                    Study
                                </a>
                            </li>
                            <li>
                                <a href="" title="Android" class="portfolio-icon-link">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/android-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Iphone">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apple-icon.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Website">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PursueIt Bg  -->
    <!-- Start Jodhpuri Funrniture Bg  -->
    <div class="play-glam-bg jodhpuri-furniture-bg">
        <div class="container px-0">
            <div class="row">
                <!-- Start only show mobile -->
                <div class="col-12 col-lg-7 col-xl-7 d-lg-none d-xl-none">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/jodhpuri-furniture-right.png"
                            alt="">
                    </div>
                </div>
                <!-- End only show mobile -->
                <div class="col-12 col-lg-5 col-xl-5">
                    <div class="portfolio-list-heading">
                        <h3>Jodhpuri Furniture</h3>
                        <p class="pt-8">
                            <strong>
                                Jodhpuri Furniture is a One-Stop-Shop E-commerce website for all furniture requirements.
                            </strong>
                        </p>
                        <p>
                            It offers online services to buy elegant and creative furniture products to enable a free
                            lifestyle. It offers hassle-free shopping experience with free Shipping or Handling fees.
                        </p>
                        <ul class="view-study-btns">
                            <li>
                                <a href="javascript:void(0)" class="view-study-link" title="View Case Study">View Case
                                    Study
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Website">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Start only Show Desktop -->
                <div class="col-12 col-lg-7 col-lg-7 d-none d-lg-block">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/jodhpuri-furniture-right.png"
                            alt="">
                    </div>
                </div>
                <!-- End only Show Desktop -->
            </div>
        </div>
    </div>
    <!-- End Jodhpuri Funrniture Bg  -->
    <!-- Start Airline Bg  -->
    <div class="play-glam-bg airline-bg">
        <div class="container px-0">
            <div class="row">
                <div class="col-12 col-lg-7 col-xl-7">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/airline-left.png">
                    </div>
                </div>
                <div class="col-12 col-lg-5 col-xl-5">
                    <div class="portfolio-list-heading">
                        <h3>The Airline Reviewer</h3>
                        <p class="pt-8">
                            <strong>
                                The Airline Reviewer is an independent customer forum and a platform for interpreting
                                the airline services of every flight.
                            </strong>
                        </p>
                        <p>
                            It provides genuine reviews of in-flight experiences. Compare the flights, class of travel,
                            or even the perfect seat number for the journey.
                        </p>
                        <ul class="view-study-btns">
                            <li>
                                <a href="javascript:void(0)" class="view-study-link" title="View Case Study">View Case
                                    Study
                                </a>
                            </li>
                            <li>
                                <a href="" title="Android" class="portfolio-icon-link">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/android-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Iphone">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apple-icon.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Website">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Airline Bg  -->
    <!-- Start Mesmo Bg  -->
    <div class="play-glam-bg mesmo-bg">
        <div class="container px-0">
            <div class="row">
                <div class="col-12 col-lg-7 col-xl-7 d-lg-none d-xl-none">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/mesmo-right.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-5 col-xl-5">
                    <div class="portfolio-list-heading">
                        <h3>Mesmo</h3>
                        <p class="pt-8">
                            <strong>
                                Mesmo is an online community that provides local people experiences with
                                recommendations.
                            </strong>
                        </p>
                        <p>
                            Book the exclusive & handpicked hotels and save up to 25% discount along with access to the
                            museums, events, and shows with e-tickets online. Also, the user can save & publish their 10
                            favorite places.
                        </p>
                        <ul class="view-study-btns">
                            <li>
                                <a href="javascript:void(0)" class="view-study-link" title="View Case Study">View Case
                                    Study
                                </a>
                            </li>
                            <li>
                                <a href="" title="Android" class="portfolio-icon-link">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/android-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Iphone">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/apple-icon.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Website">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-7 col-xl-7 d-none d-lg-block">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/mesmo-right.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Mesmo Bg  -->
    <!-- Start Airline Bg  -->
    <div class="play-glam-bg bagtesh-bg">
        <div class="container px-0">
            <div class="row">
                <div class="col-12 col-lg-7 col-xl-7">
                    <div class="portfolio-left-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/bagtesh-left.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-5 col-xl-5">
                    <div class="portfolio-list-heading">
                        <h3>Bagtesh Fashion</h3>
                        <p class="pt-8">
                            <strong>
                                It is an e-commerce website that offers the largest online lifestyle brand.
                            </strong>
                        </p>
                        <p>
                            in categories of Designer Apparels, Accessories, Wedding Clothing’s, Horse Riding Jodhpurs,
                            and more.
                            It uses several payment gateway options such as PayPal, ccavenue, Visa, MasterCard, and
                            Discover.
                        </p>
                        <ul class="view-study-btns">
                            <li>
                                <a href="javascript:void(0)" class="view-study-link" title="View Case Study">View Case
                                    Study
                                </a>
                            </li>
                            <li>
                                <a href="" class="portfolio-icon-link" title="Website">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Airline Bg  -->
</section>
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>