<?php
  // Template Name:Hire Full Stack Developer
  the_post();
  get_header(); 
  ?>
<!-- Start Hire Dedicated -->
<section class="mobile_section our-portfolio hire-dedicated angularjs-bg" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <h1 class="banner-heading ">
                        Hire Full Stack Developer at Geekologix
                    </h1>
                    <p>
                        Hire a Full Stack Developer or a dedicated team of Full Stack Developers for your next web or
                        app development project. Providing accelerated Full Stack Development with ensured on-time
                        delivery and bug-free execution.
                    </p>
                    <!-- <ul class="comman_list ">
                        <li>A dedicated in-house team of certified developers</li>
                        <li>Weekly client meetings via mail or skype</li>
                        <li>Implementation of the latest technologies</li>
                        <li>Following international standards</li>
                    </ul> -->
                    <div class="hire-dedicated-btn">
                        <a href="<?php echo the_permalink(288) ?>" class="let-disuss-btn" title="Let's Discuss">Let's Discuss</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="our-experts-form">
                        <div class="form-heading">
                            <h4>Request A Free Quote</h4>
                        </div>
                        <?php echo do_shortcode('[contact-form-7 id="357" title="REQUEST A FREE QUOTE"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Dedicated -->
<!-- Start Doveloper Hiring -->
<section class="developer-hiring">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <h2>
                    Hire Full Stack Developers in India
                </h2>
                <p>
                    Full Stock developers at Geekologix are your one stop solution to all your development requirements.
                    With a Full Stock Development, you don’t need to hire different developers for variable development
                    methods. Our Full-Stack developers can assure you an all-inclusive development:
                </p>
                <ul class="comman_list">
                    <li>Develop or program a browser</li>
                    <li>Develop or program a server</li>
                    <li>Develop or program a database</li>
                </ul>
                <p>
                    Our Full Stack Developers are master at various programming languages, including, JavaScript,
                    jQuery, Angular, Vue, PHP, ASP, Python, Node, SQL, SQLite, MongoDB, etc.
                </p>
                <p>
                    Ever wondered if one team could fulfil all your development needs? With our trained full-stack
                    developers, it is now possible and affordable too.
                </p>
                <div class="developer-hiring-btn">
                    <a href="<?php echo the_permalink(288) ?>" title="Consult with Full Stack  Experts"
                        class="read_more text-uppercase">Consult with
                        Full Stack  Experts</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="developer-hiring-blocks">
                    <div class="row">
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Budget-Friendly Pricing">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/lesser-capital-cost.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Budget-Friendly Pricing</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Flawless Coding">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/source-code.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Flawless Coding</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Complete Confidentiality">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/non-disclosure.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Complete Confidentiality</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="On-time Delivery">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/less-time.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>On-time Delivery</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Hiring -->
<!-- Start Doveloper Timeing  -->
<section class="doveloper-timing">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Full-Time Hiring">
                    <div class="time-card full-time-bg">
                        <h4>Full-Time Hiring</h4>
                        <p class="min-h-38">Works only for you.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Communication</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Part-Time Hiring">
                    <div class="time-card part-time-bg">
                        <h4>Part-Time Hiring</h4>
                        <p class="min-h-38">Prefer better half for your
                            business.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Communication</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Hourly Hiring">
                    <div class="time-card hourly-time-bg">
                        <h4>Hourly Hiring</h4>
                        <p class="min-h-38">Need a few hours of attention
                            to the work.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Communication</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Contractual Hiring">
                    <div class="time-card contractual-time-bg">
                        <h4>Contractual Hiring</h4>
                        <p class="min-h-38">Agree on your condition when
                            needed.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Communication</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Timeing  -->
<!-- Start What Is Fullstack -->
<section class="what-fullstack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">Custom Full Stack Application Development Services</h3>
                <p class="comman_p text-center">
                    The Full Stack Developers at Geekologix are multi-talented. They are
                    able to provide excellent services of development in both front end and back end. Our experts excel
                    at their coding capabilities with a splash of creativity when it comes to the designing segment.
                    They provide you full support from prototyping, designing and development to deployment and
                    implementation of your products.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End What Is Fullstack -->
<!-- Start Fullstack Content -->
<section class="fullstack-content">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="shap-fullstack-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/shap-fullstack.png" alt="">
                </div>
                <ul class="fullstack-content-list">
                    <li>Creative and Innovative UI/UX Designing</li>
                    <li>High Quality Graphics</li>
                    <li>Latest Features and Functionality</li>
                    <li>Bug-free Development</li>
                    <li>Database Configuration Enabled</li>
                    <li>API and Third Party Integration</li>
                    <li>PSD to Design Conversion</li>
                    <li>Full Stack Consultation</li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-6">
                <h4>Hire a dedicated Full Stack Developer for your project</h4>
                <p> Full Stack Developers at Geekologix are ready to collaborate with your team for any web or app
                    development project or to take it up on their own. Our developers are passionate about coding and
                    designing, with an urge to finish each task with a full satisfaction of the clients. </p>

                <p>Our developers are trained with an experience in various industries and have a vast knowledge of
                    various trends and industrial standards when it comes to designing and development of a public web
                    platform or application, or an internal functioning portal, software or resource management system.
                    With our developers, you can hold great expectations about your web development projects and get
                    them fulfilled without failure.</p>
            </div>
        </div>
    </div>
</section>
<!-- End Fullstack Content -->
<!-- Start Angular Steps -->
<section class="angular-steps">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Full Stack Developers in 5 Simple Steps</h4>
                <p class="comman_p text-center">We value your time and money. Hiring developers at Geekologix is easy
                    and fast. We make sure that our services match your project requirements.</p>
            </div>
        </div>
        <div class="ang-step-cards">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="step-card-list">
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="Request Hiring">
                                <h5>01</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-01.png" alt="">
                                </div>
                                <h6>Request Hiring</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="View and Select Full Stack Developers">
                                <h5>02</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-02.png" alt="">
                                </div>
                                <h6>View and Select Full Stack Developers</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Interview and Approve selected Developers">
                                <h5>03</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-03.png" alt="">
                                </div>
                                <h6>Interview and Approve selected Developers</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Get the best Deal for your Full Stack Project">
                                <h5>04</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-04.png" alt="">
                                </div>
                                <h6>Get the best Deal for your Full Stack Project</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Start your Development with our team instantly">
                                <h5>05</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-05.png" alt="">
                                </div>
                                <h6>Start your Development with our team instantly</h6>
                            </a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Angular Steps -->
<!-- Start Three Tier Architecture -->
<section class="three-tier-architecture">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Three-Tier Architecture for your Business Growth</h4>
            </div>
        </div>
        <div class="three-tier-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Presentation Layer">
                        <div class="three-tier-card">
                            <div class="card-content">
                                <h5>Presentation Layer</h5>
                                <p>Our developers make sure that you we present your business with an attractive design
                                    that suits your business category and meets the industry standards, giving you an
                                    edge over your competitors, while showcasing your products and services effectively.
                                </p>
                            </div>
                            <ul class="card-content-list">
                                <li>UI Design and Development</li>
                                <li>Attractive Designs</li>
                                <li>Quality Designing</li>
                                <li>Multi-level testing</li>
                                <li>Custom Design and Development</li>
                                <li>User Experience Analysis</li>
                            </ul>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Business Layer">
                        <div class="three-tier-card">
                            <div class="card-content">
                                <h5>Business Layer</h5>
                                <p>Our developers consider your business and industry specific requirements, as well as
                                    your products and services, to develop an adaptive, long-lasting and user-friendly
                                    product for your business needs, effectively elevating your everyday stress.</p>
                            </div>
                            <ul class="card-content-list">
                                <li>MVC Integration</li>
                                <li>Market Research</li>
                                <li>Business Analysis</li>
                                <li>Custom Back-End Design</li>
                                <li>Data Modelling</li>
                                <li>Secure Back-End Architecture Development</li>
                            </ul>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Database Layer">
                        <div class="three-tier-card">
                            <div class="card-content">
                                <h5>Database Layer</h5>
                                <p>Our developers design very effective database management tools tailored especially
                                    for your business to give you a secure, hassle free source of data management which
                                    is adaptive and performance-optimised regularly.</p>
                            </div>
                            <ul class="card-content-list">
                                <li>Database Integration</li>
                                <li>API Integration</li>
                                <li>Server Hosting</li>
                                <li>Third-Party Synchronisation</li>
                                <li>Custom Database Developments</li>
                            </ul>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Three Tier Architecture -->
<!-- Start Hire Mean Stack -->
<section class="hire-mean-stack">
    <div class="container px-0">
        <div class="benefits-hire">
            <div class="owl-carousel owl-theme hire-mean-slider" id="hire_mean_slider">
                <div class="item">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">
                            <ul class="benefits-list">
                                <li>
                                    <a href="javascript:void(0)" title="Javascript">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Javascript
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="MongoDB as Database Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/mongo-db.png"
                                                alt="">
                                            MongoDB as Database Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="ExpressJS as Back-End Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            ExpressJS as Back-End Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="AngularJS as Front-End Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/angular-js.png"
                                                alt="">
                                            AngularJS/React JS as Front-End Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Node.js as Server Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/node-js.png"
                                                alt="">
                                            Node.js as Server Technology
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-lg-6 offset-lg-1">
                            <h4 class="comman_h">Hire MEAN Stack Developers</h4>
                            <p class="comman_p">
                                To develop exclusive web applications hire MEAN Stack Developer as MEAN Stack
                                development
                                offers
                                the most advanced technology stack by implementing the latest technologies.
                            </p>
                            <p class="comman_p">
                                MEAN stands of MongoDB, ExpressJS, AngularJS, and Node.js. To add value to your business
                                and
                                enhance your web application productivity incorporate MEAN Stack Development.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">
                            <ul class="benefits-list">
                                <li>
                                    <a href="javascript:void(0)" title="Javascript">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Javascript
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="MongoDB as Database Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/mongo-db.png"
                                                alt="">
                                            MongoDB as Database Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="ExpressJS as Back-End Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            ExpressJS as Back-End Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="AngularJS as Front-End Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/angular-js.png"
                                                alt="">
                                            AngularJS as Front-End Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Node.js as Server Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/node-js.png"
                                                alt="">
                                            Node.js as Server Technology
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-lg-6 offset-lg-1">
                            <h4 class="comman_h">Hire METEOR STACK Developers</h4>
                            <p class="comman_p">
                                Hire Meteor Stack Developers at Geekologix for your next web development project. Meteor
                                stack developers are capable of building web apps and mobile apps in the fastest and
                                shortest timelines. Various features of meteor.js allows for a steady and accelerated
                                web development.
                            </p>
                            <p class="comman_p">
                                Meteor.js is free and open-source. The main objectives for its usage are enabling speed
                                and simplicity in web development.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">
                            <ul class="benefits-list">
                                <li>
                                    <a href="javascript:void(0)" title="-	Javascript">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Javascript
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Linux">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Linux
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Nginx as Server Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/node-js.png"
                                                alt="">
                                            Nginx as Server Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="MySQL/MangoDB as Database Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/angular-js.png"
                                                alt="">
                                            MySQL/MangoDB as Database Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="PHP as Front-end Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/mongo-db.png"
                                                alt="">
                                            PHP as Front-end Technology
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-lg-6 offset-lg-1">
                            <h4 class="comman_h">Hire LEMP STACK Developers</h4>
                            <p class="comman_p">
                                Hire LEMP Stack Developers at Geekologix and engage with them to develop amazing dynamic
                                web and mobile apps. LEMP Stack allows developers to download high-performance and
                                powerful dynamic web development products.
                            </p>
                            <p class="comman_p">
                                MEAN stands of MongoDB, ExpressJS, AngularJS, and Node.js. To add value to your business
                                and
                                enhance your web application productivity incorporate MEAN Stack Development.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">
                            <ul class="benefits-list">
                                <li>
                                    <a href="javascript:void(0)" title="-	Javascript">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Javascript
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Linux">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Linux
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Nginx as Server Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/node-js.png"
                                                alt="">
                                            Nginx as Server Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="MySQL/MangoDB as Database Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/angular-js.png"
                                                alt="">
                                            MySQL/MangoDB as Database Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="PHP as Front-end Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/mongo-db.png"
                                                alt="">
                                            PHP as Front-end Technology
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-lg-6 offset-lg-1">
                            <h4 class="comman_h">Hire LAMP STACK Developers</h4>
                            <p class="comman_p">
                                Hire LEMP Stack Developers at Geekologix and engage with them to develop amazing dynamic
                                web and mobile apps. LEMP Stack allows developers to download high-performance and
                                powerful dynamic web development products.
                            </p>
                            <p class="comman_p">
                                MEAN stands of MongoDB, ExpressJS, AngularJS, and Node.js. To add value to your business
                                and
                                enhance your web application productivity incorporate MEAN Stack Development.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">
                            <ul class="benefits-list">
                                <li>
                                    <a href="javascript:void(0)" title="-	Javascript">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Javascript
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Linux">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/expressjs-icon.png"
                                                alt="">
                                            Linux
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Nginx as Server Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/node-js.png"
                                                alt="">
                                            Nginx as Server Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="MySQL/MangoDB as Database Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/angular-js.png"
                                                alt="">
                                            MySQL/MangoDB as Database Technology
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="PHP as Front-end Technology">
                                        <div class="mobility-apps">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/mongo-db.png"
                                                alt="">
                                            PHP as Front-end Technology
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-lg-6 offset-lg-1">
                            <h4 class="comman_h">Django Stack Developers</h4>
                            <p class="comman_p">
                                Hire Django Full Stack Developers at Geekologix to supplement your web development
                                project. Our Django stack developers can support your projects with high-performance web
                                and mobile apps, built in your preferred deadlines.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Hire Mean Stack -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Appointment Guide</h4>
            </div>
        </div>
        <div class="experts-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Site">
                        <div class="experts-card">
                            <div class="expert-img on-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/on-site.png" alt="">
                            </div>
                            <h5>ON-SITE</h5>
                            <p>Hire Full Stack Developers to work with you at your office or preferred Job Location, so
                                that you can work with more transparency and speed.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Off-Site">
                        <div class="experts-card">
                            <div class="expert-img off-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/off-site.png" alt="">
                            </div>
                            <h5>OFF-SITE</h5>
                            <p>Hire Full Stack Developers to work from our development office at your preferred time. We
                                ensure full transparency, total reporting and on-time delivery.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hybrid">
                        <div class="experts-card">
                            <div class="expert-img hybrid-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/hybrid.png" alt="">
                            </div>
                            <h5>HYBRID</h5>
                            <p>Hire Full Stack developers who are ready to work at your flexibility. Get a mix of the
                                onsite and offsite working to match your requirements.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Why To Hire -->
<section class="why-to-hire">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">
                Benefits of Choosing Geekologix as your Full Stack Development Partner
                </h4>
            </div>
        </div>
        <div class="hire-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Passionate Full Stack Coders">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                1
                            </div>
                            <h5>Passionate Full Stack Coders</h5>
                            <p>
                                At Geekologix, We hire coders and programmers who are really passionate about what they
                                do and how they do it. They come up with the most creative and quick solutions for each
                                project, and it is a pleasant sight to see them bringing your idea into reality.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Dedicated and Diligent Team">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                2
                            </div>
                            <h5>Dedicated and Diligent Team</h5>
                            <p>
                                An advantage of hiring passionate developers is that they work with complete honesty and
                                discipline to their work. Our teams are very dedicated and work diligently on the task
                                assigned to them, to turn it in as per the given deadline.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Experienced Team Managers">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                3
                            </div>
                            <h5>Experienced Team Managers</h5>
                            <p>
                                Geekologix Teams are hierarchy based. They are headed by experienced team managers who
                                schedule and monitor each task as well as double check it before delivery. Such team
                                managers enable our teams to work on big projects smoothly.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Project Transparency">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                4
                            </div>
                            <h5>Project Transparency</h5>
                            <p>
                                Projects at Geekologix are carried out with full transparency with the clients. Our team
                                managers will be eager to provide you with all the minute details of your project, with
                                accurate task completion schedules and project status for your satisfaction.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Live Reporting">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                5
                            </div>
                            <h5>Live Reporting</h5>
                            <p>
                                Along with the transparency schedules, our team are also used to for live reporting
                                projects. We give our clients the liberty to ask for current project status or put any
                                live query whenever they want, and always revert to them with sincerity.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="No Hidden Charges">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                6
                            </div>
                            <h5>No Hidden Charges</h5>
                            <p>
                                We do not put any hidden charges in the projects during the billing process. We make
                                sure that everything is discussed at the commencement of the project and followed
                                throughout as per your considerations, on your terms and conditions.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Kepp eye on your project">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                7
                            </div>
                            <h5>Kepp eye on your project</h5>
                            <p>
                                For all our projects, we maintain complete confidentiality on our ends. We ensure that
                                there are no data leaks. No information goes out from our development centre without the
                                instructions from our clients.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Secured Full Stack Web/App">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                8
                            </div>
                            <h5>Secured Full Stack Web/App</h5>
                            <p>
                                All the apps and websites built at our development centres are completely secure and
                                free of data leaks. Our teams are trained about the critical importance of
                                confidentiality during the development process and abide by it.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Time Project Delivery">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                9
                            </div>
                            <h5>On-Time Project Delivery</h5>
                            <p>
                                We value our project delivery promises. Our development teams are true to their words.
                                They ensure that the projects are finished within the promised deadlines through
                                thorough planning and scheduling.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Best Pricing Deals">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                10
                            </div>
                            <h5>Best Pricing Deals</h5>
                            <p>
                                At Geekologix, you can get your requirements fulfilled in the most affordable and fairly
                                priced deals with assured project quality. We deliver what we promise, and we charge
                                fairly keeping in mind your project requirements.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Thorough Testing Before Delivery">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                11
                            </div>
                            <h5>Thorough Testing Before Delivery</h5>
                            <p>
                                Before handing over each project to our clients, we run various testing rounds to make
                                sure that everything is in place and working exactly how you expect it to work. We leave
                                no scope for no complaints after the project delivery.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Queries, Bug-fixes, and Maintenance">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                12
                            </div>
                            <h5>Queries, Bug-fixes, and Maintenance</h5>
                            <p>
                                Even after the completion of the projects, we make sure that our clients do not face any
                                difficulties in handling the project. We are glad to solve their queries, bugs and
                                maintenance issues even after the official project closure.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Why To Hire -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Flexible Full Stack Developers at Geekologix</h4>
            </div>
        </div>
        <div class="experts-cards flexible-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Fixed Charge hiring for Start-ups">
                        <div class="experts-card">
                            <div class="expert-img fixed-rate-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Fixed Charge hiring for Start-ups</h5>
                            <p>If you are looking to hire developers for your start-up, we suggest you to convey your
                                requirements to us, so that we can provide you with the best possible fixed price
                                quotation which suits your business venture.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hourly hiring for Entrepreneurs">
                        <div class="experts-card">
                            <div class="expert-img dedicated-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Hourly hiring for Entrepreneurs</h5>
                            <p>If you are looking for developers to work on your individual or personal ventures, it is
                                best to hire developers on a hourly basis which would prove to be more pocket-friendly
                                for you, while fulfilling all your development needs.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hourly hiring for Entrepreneurs">
                        <div class="experts-card">
                            <div class="expert-img hourly-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Hourly hiring for Entrepreneurs</h5>
                            <p>If you are looking for developers to work on your individual or personal ventures, it is
                                best to hire developers on a hourly basis which would prove to be more pocket-friendly
                                for you, while fulfilling all your development needs.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                        $testimonialSlider =  array(
                            'post_status'    => 'publish', 
                            'post_type'      => 'client_slider', 
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                          );
                          $getTestimonialData = new WP_Query($testimonialSlider);
                          if($getTestimonialData ->have_posts()) {
                          while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                          $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                            $testimonial_img = $testimonialimg[0];
                        ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                        endwhile;
                    } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Faq  -->
<section class="faq">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Frequently Asked Questions</h4>
            </div>
            <div class="col-lg-7">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne"
                                    title="How can I hire a Full Stack Developer/Developers according to my preferences?">
                                    How can I hire a Full Stack Developer/Developers according to my preferences?
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    At Geekologix, you choose whom you want to hire. Before we finalise the project
                                    details with you, we send you developer resumes to select and filter. You can choose
                                    your preferred developers or form a team of developers as per your requirements for
                                    the project. You can also interview the developers if needed. For smaller project,
                                    our project managers select and provides you the best suited developer for your
                                    project requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="What are your pricing specifications?">
                                    What are your pricing specifications?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Our team managers convey you the price specifications of your projects before our
                                    teams start working on them. The idea is to provide the best deals tailored for your
                                    projects as well as to allow you to scale and manage your budget according to the
                                    service.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree"
                                    title="How long will it take to finish my Full Stack project?">
                                    How long will it take to finish my Full Stack project?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Each project needs a different timeline, based on the complexity, requirements and
                                    availability of the developers. At the beginning of your project, we discuss your
                                    project details with you and decide a timeline for each stage as per your
                                    requirements, and start only after your approval.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure"
                                    title="What is the process for hiring Full Stack developers at Geekologix?">
                                    What is the process for hiring Full Stack developers at Geekologix?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Hiring Full Stack Developers at Geekologix is very easy. You can hire your preferred
                                    developer in just 3 simple steps:
                                </p>
                                <ul class="comman_list">
                                    <li>Request Hiring by conveying your requirements</li>
                                    <li>View, Select and Interview Full Stack Developers</li>
                                    <li>Sign the Deal and Start Development instantly</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="faq-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/frequently-question.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Faq  -->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>