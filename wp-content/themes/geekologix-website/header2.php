<div class="header-top-blank-div"></div>
<header class="header-top header-fixed"">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6">
        <?php // Logo
              $header_image = get_header_image();
              if (!empty($header_image)) : ?>
        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php  echo get_bloginfo(); ?>"
          class="logo web-btn-banner fixed-logo">
          <img src="<?php echo esc_url($header_image); ?>" class="" />
        </a>
        <?php endif; ?>
      </div>
      <div class="col-6 text-right align-self-center">
        <a href="javascript:void(0)" class="fixed-burger-icon " id="menu_open">
           <!-- <i class="fal fa-bars"></i> -->
          <img src="<?php echo get_template_directory_uri() ?>/images/burger-menu.svg" alt="" width="35" class="burger-icon">
          <img src="<?php echo get_template_directory_uri(); ?>/images/x.png" alt="" class="x-icon">
        </a>
      </div>
    </div>
  </div>
</header>
 <!-- Start Geeko menu -->
 <div class="geeko-menu">
            <!-- <div class="container-fluid">
                <div class="row">
                    <div class="col-6">
                        <?php // Logo
                        $header_image = get_header_image();
                        if (!empty($header_image)) : ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php  echo get_bloginfo(); ?>"
                            class="logo web-btn-banner">
                            <img src="<?php echo esc_url($header_image); ?>" class="" />
                        </a>
                        <?php endif; ?>
                    </div>
                    <div class="col-6 text-right align-self-center">
                        <a href="javascript:void(0)" class="web-btn-banner cross-button" id="menu_close">
                            <i class="fal fa-times"></i>
                        </a>
                    </div>
                </div>
            </div> -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="menu-headings">
                            <li class="menu-heading-item">
                                <a href="<?php echo get_permalink(6); ?>/#about-us"  title="About Us">About Us</a>
                            </li>
                            <li class="menu-heading-item">
                                <a href="<?php echo get_permalink(270); ?>" title="Services">Services</a>
                                <div class="mega-menu-block">
                                    <ul class="inner-menu-list">
                                        <h5>Services</h5>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(230); ?>" title="Web Development">
                                                Web Development 
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(272); ?>" title="Mobile & Wearables">
                                              Mobile & Wearables
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(304); ?>" title=" IOS  Development">
                                            IOS  Development 
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(327); ?>" title="PHP Development">
                                            PHP Development
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(302); ?>" title="Android  Development">
                                            Android  Development
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item ">
                                            <a href="<?php echo get_permalink(313); ?>" title="Flutter  Development">
                                                Flutter  Development
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item ">
                                            <a href="<?php echo get_permalink(319); ?>" title="Angular Development">
                                                Angular Development
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item ">
                                            <a href="<?php echo get_permalink(321); ?>" title=".NET Development">
                                                .NET Development
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item ">
                                            <a href="<?php echo get_permalink(323); ?>" title="WordPress Development">
                                                WordPress Development 
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item ">
                                            <a href="<?php echo get_permalink(330); ?>" title="Java Development">
                                               Java Development
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item ">
                                            <a href="<?php echo get_permalink(334); ?>" title="Java Development">
                                            Ecommerce Development
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-heading-item">
                                <a href="javascript:void(0)" title="Hire Developers">Hire Developers</a>
                                <div class="mega-menu-block">
                                    <ul class="inner-menu-list">
                                        <h5>Hire Developer</h5>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(279); ?>" title="Hire Dedicated Developers">
                                            Hire Dedicated Developer
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(290); ?>" title=" AngularJS Developer">
                                                AngularJS Developer
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(292); ?>" title=" Flutter Developer">
                                                Flutter Developer
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(294); ?>" title=" React Native Developer">
                                              React Native Developer
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(300); ?>" title="DevOps Developer & Engineer">
                                              DevOps Developer & Engineer
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(298); ?>" title="Full Stack Developer">
                                              Full Stack Developer
                                            </a>
                                        </li>
                                        <li class="inner-menu-list-item">
                                            <a href="<?php echo get_permalink(308); ?>" title="Wordpress Developer">
                                             Wordpress Developer
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-heading-item">
                                <a href="<?php echo get_permalink(6); ?>/#process" title="Process">Process</a>
                            </li>
                            <!-- <li class="menu-heading-item">
                                <a href="<?php echo get_permalink(274); ?>" title="Our Portfolio">Our Portfolio</a>
                            </li> -->
                            <li class="menu-heading-item">
                                <a href="<?php echo get_permalink(6); ?>/#career" title="Career">Career</a>
                            </li>
                            <li class="menu-heading-item">
                                <a href="<?php echo get_permalink(288); ?>" title="Contact Us">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Geeko menu -->