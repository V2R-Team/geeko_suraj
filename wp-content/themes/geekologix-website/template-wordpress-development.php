<?php 
//Template Name: Wordpress Development
echo get_header();
?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xl-7">
                    <h1 class="banner-heading">
                        <?php 

              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>Delivering excellent business solutions through tailor-made WordPress Development.</p>
                    <!-- <?php echo the_content(); ?> -->
                    <a href="<?php echo get_permalink('323'); ?>/#showcase"
                        class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6 col-xl-5">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/web-development-right.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">WordPress Development Service</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/wordpress-development-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    WordPress is one of the most popular CMS platforms used for building stunning, feature-rich and
                    highly functional websites.
                </h5>
                <p class="comman_p">
                    Wordpress is a wonderful bundle of joy which comes free of cost, is self hosted, and can be
                    installed very easily. It comes free, is self-hosted and easy to install, deploy & upgrade. It comes
                    loaded with several stunning themes along with ready-to-use plugins which offer extended
                    functionality as well as additional features to a WordPress website development.
                </p>
                <p class="comman_p">
                    We keep track of the industry trends and know how your competitors are moving. Our experts have
                    full-blown knowledge of the current market to give you an edge over your competitors and how they
                    impact your brand value and position. We are known and trusted for making the most beautiful and
                    highly scalable wordpress websites tailor-made for our client’s needs and budget. We believe in
                    delivering the best quality website and make no compromise on quality. We integrate the best
                    WordPress themes plugins to create a customized website for your business.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start What Makes Wordpress  -->
<section class="what-makes-wordpress">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">
                    What makes Wordpress the best choice for your Web App ?
                </h3>
            </div>
        </div>
        <div class="wordpress-web-app-content">
            <div class="row">
                <div class="col-md-6 col-lg-5">
                    <div class="wordpress-web-app-card">
                        <div class="wordpress-web-app-text text-right">
                            <h5>Easy Customization</h5>
                            <p>
                                WordPress offers countless free, customizable themes that allows you to give your
                                website the look that you desire.
                            </p>
                        </div>
                        <div class="wordpress-web-app-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/extendable-wordpress-web-app.png"
                                alt="" class="extendable-wordpress-icon">
                        </div>
                    </div>
                    <div class="wordpress-web-app-card">
                        <div class="wordpress-web-app-text text-right">
                            <h5>Secure Development</h5>
                            <p>
                                Wordpress is considered one of the safest and most secure platforms to run any web app.
                                It is safe enough to hold any critical information of your business without any leakage.
                            </p>
                        </div>
                        <div class="wordpress-web-app-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/safe-wordpress-web-app.png"
                                alt="" class="safe-wordpress-icon">
                        </div>
                    </div>
                    <div class="wordpress-web-app-card">
                        <div class="wordpress-web-app-text text-right">
                            <h5>Quick Development Capability</h5>
                            <p>
                                Using WordPress is not just about having textual information. It also comes with
                                built-in support to handle images, audio, and video content as well.
                            </p>
                        </div>
                        <div class="wordpress-web-app-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/provides-wordpress-web-app.png"
                                alt="" class="provides-wordpress-icon">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 d-md-none d-lg-block">
                    <div class="worpress-center-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/wordpres-center-img.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="wordpress-web-app-card responsive-direction">
                        <div class="wordpress-web-app-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/handle-wordpress-web-app.png"
                                alt="" class="handle-wordpress-icon">
                        </div>
                        <div class="wordpress-web-app-text text-left">
                            <h5>Supports Multiple Media Types</h5>
                            <p>
                                Wordpress is designed to support various media types such as images, videos, audios, and
                                texts without any issue. No matter what is the type of your data, Wordpress can handle
                                it without any difficulty
                            </p>
                        </div>
                    </div>
                    <div class="wordpress-web-app-card responsive-direction">
                        <div class="wordpress-web-app-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/easy-wordpress-web-app.png"
                                alt="" class="easy-wordpress-icon">
                        </div>
                        <div class="wordpress-web-app-text text-left">
                            <h5>Convenient CMS</h5>
                            <p>
                                WordPress can be updated with a single click. It utilizes built-in updater to
                                auto-update the plugins and themes from your WordPress admin dashboard making it very
                                easy to manage..
                            </p>
                        </div>
                    </div>
                    <div class="wordpress-web-app-card responsive-direction">
                        <div class="wordpress-web-app-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/search-wordpress-web-app.png"
                                alt="" class="search-wordpress-icon">
                        </div>
                        <div class="wordpress-web-app-text text-left">
                            <h5>SEO Friendly</h5>
                            <p>
                                WordPress has been developed utilizing the standard compliance code that produces
                                semantic markup making your website highly attractive to the SEO processes.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End What Makes Wordpress  -->
<!-- Start Laravel Development Edge -->
<section class="laravel-development-edge wordpress-advantage">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comma_h text-center">Perks of working with Geekologix Developers</h3>
                <p class="comma_p text-center">Here is how choosing Geekologix java development services gives you an
                    edge.</p>
            </div>
        </div>
        <div class="wordpress-advantage-cards">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/experienced-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Experienced & Certified Developers</h5>
                        <p>Our highly experienced and certified developers leverage their skills and experience in
                            advanced technologies to deliver high-performing wordpress websites across multiple
                            verticals.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/search-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>SEO Friendly</h5>
                        <p>Our web apps are not just user-friendly and easy to navigate but also provide SEO friendly
                            features.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/extensible-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Extensible & Reliable Solutions</h5>
                        <p>We assure efficient traffic response by considering the aspects of reliability and
                            extensibility with great precision.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/satisfaction-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Assured Customer Satisfaction
                        </h5>
                        <p>For us, customer satisfaction is the ultimate aim. We strive to deliver the best products and
                            provide much more worth than the client’s money.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tactical-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>High-end Solutions</h5>
                        <p>We employ smart strategies and implement far-sighted plans of action to develop optimum
                            results in terms of high-end WordPress solutions.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cost-effective-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Affordable Solutions</h5>
                        <p>We deliver our clients with best quality without them having to burn a hole in their pocket.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/bug-free-wordpress-advantage.png"
                                alt="">
                        </div>
                        <h5>Complete Debugging</h5>
                        <p>Our Quality Assurance team makes sure that your WP website runs smoothly across the devices
                            and platforms without any bugs, errors, or glitches.

                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="wordpress-advantage-card">
                        <div class="wordpress-advantage-card-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/maintenance-ios-key-offering.png"
                                alt="">
                        </div>
                        <h5>24*7 Support</h5>
                        <p>Our experts are available for your assistance round the clock to answer your queries promptly
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Laravel Development Edge -->
<!-- Start Platforms Industries -->
<section class="platforms-industries">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">WordPress Development Expertise Across Industries</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Real Estate">
                    <div class="industry_block real_state_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate-copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images real_state mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Real Estate</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Education">
                    <div class="industry_block education_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images education mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Education</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Healthcare">
                    <div class="industry_block healthcare_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images healthcare mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Healthcare</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Finance">
                    <div class="industry_block finance_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images finance mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Finance</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Automotives">
                    <div class="industry_block automotives_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images automotives mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Automotives</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Gaming">
                    <div class="industry_block gaming_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images gaming mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Gaming</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Event & Tickets">
                    <div class="industry_block event_tickets_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png"
                                alt="" class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images event_tickets mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Event & Tickets</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Travel & Tours">
                    <div class="industry_block travel_tours_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images travel_tours mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Travel & Tours</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Ecommerce">
                    <div class="industry_block ecommerce_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images ecommerce mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Ecommerce</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Entertainment">
                    <div class="industry_block entertainment_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- 	<div class="industry_images entertainment mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Entertainment</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Food & Beverage">
                    <div class="industry_block food_beverage_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png"
                                alt="" class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png"
                                alt="" class="img_hover mx-auto">
                        </div>
                        <!-- <div class="industry_images food_beverage mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Food & Beverage</h5>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-6 col-md-3">
                <a href="" title="Fitness">
                    <div class="industry_block  fitness_outer text-center">
                        <div class=" mx-auto">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt=""
                                class="img_desktop">
                            <img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt=""
                                class="img_hover mx-auto">
                        </div>
                        <!-- 	<div class="industry_images fitness mx-auto">
                      <img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
                  </div> -->
                        <h5 class="text-capitalize">Fitness</h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Platforms Industries -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start Our Key Offerings -->
<section class="our-key-offerings">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Key Offerings</h4>
            </div>
        </div>
        <div class="Offerings-cards">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="iOS e-Learning Application Development">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ios-offering.png" alt="">
                            </div>
                            <h5>iOS e-Learning Application Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Location-Based Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/location-offering.png"
                                    alt="">
                            </div>
                            <h5>Location-Based Android App</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Travel & Booking Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/travel-offering.png"
                                    alt="">
                            </div>
                            <h5>Travel & Booking Android App</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Social Media Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/social-offering.png"
                                    alt="">
                            </div>
                            <h5>Social Media Android App</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Financial & E-Commerce Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/financial-offering.png"
                                    alt="">
                            </div>
                            <h5>Financial & E-Commerce Android App</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="ERP Solutions Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/erp-offering.png" alt="">
                            </div>
                            <h5>ERP Solutions Android App</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Gaming & Media Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/gaming-offering.png"
                                    alt="">
                            </div>
                            <h5>Gaming & Media Android App</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Backend & Support Android App">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/backend-offering.png"
                                    alt="">
                            </div>
                            <h5>Backend & Support Android App</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Key Offerings -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Wordpress Development</h4>
                <h5>Geekologix is one of the globally acclaimed providers of cost-effective Wordpress solutions.</h5>
                <p>Our engineers are well-versed with the advanced tools and technologies that transform your ideas into
                    solutions, with perfection. If you have a project idea that has potential to bring great disruptions
                    in industry tomorrow, get in touch with our innovators today.</p>
                <div class="hire-dedicated-btn">
                    <a href="<?php echo get_permalink('288'); ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="<?php echo get_permalink('288'); ?>" class="common_btns text-uppercase requst_quote"
                    title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
                <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left">
                                <?php echo do_shortcode('[contact type="office_address"]') ?>
                            </p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left" href="mailto:<?php echo do_shortcode('[contact type="
                                email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?>
                            </a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type=" skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?>
                            </a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left" href="tel:<?php echo do_shortcode('[contact type=" india mobile"]')
                                ?>">
                                India:
                                <?php echo do_shortcode('[contact type="india mobile"]') ?>
                            </a>,
                            <a href="tel:<?php echo do_shortcode('[contact type=" india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?>
                            </a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                            </a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
      $contact_ID = 51;
      $contact_data = get_page($contact_ID);
      $conatct_title = $contact_data->post_title;
      $conatct_content = $contact_data->post_content;
    ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                          <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                      </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>