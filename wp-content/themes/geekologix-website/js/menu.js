<?php
global $wp;
	/**
	 * The template for displaying the footer
	 *
	 * Contains the closing of the #content div and all content after.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package geekologix_website
	 */
	
	$site_url = site_url();

	?>
footer
<!-- fullpage -->
</div>

</html> <?php wp_footer(); ?> <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.js"> </script>
<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

  // Scroll js for full page 
  $('#fullpage').fullpage({
    anchors: ['home-page'],

    fadingEffect: true,
    scrollingSpeed: 1000,
    scrollingSpeed: 1000,
    responsiveWidth: 980,
    lazyLoading: true,
    loopHorizontal: true,
    responsiveWidth: 992,
    animateAnchor: true,
    verticalCentered: false,
    scrollBar: true,
    // autoScrolling: true,
    afterRender: function() {
      new WOW().init();
    },
  });

});


new WOW().init();
$(function() {
	var num = $(this).scrollTop() !== 0;
	$(window).bind('scroll', function() {
		if (pageUrl !== 'home') {
			if ($(window).scrollTop() > num) {
				$('.scrollbg-show').addClass('show-strip');
				$('.menu-bg').addClass('reverseMenu');
				$('.burger > .icon-bar1').addClass('icon-bar11');
				$('.burger > .icon-bar2').addClass('icon-bar21');
				$('.burger > .icon-bar3').addClass('icon-bar31');
			} else {
				num = $(this).scrollTop() !== 0;
				$('.scrollbg-show').removeClass('show-strip');
				$('.menu-bg').removeClass('reverseMenu');
				$('.burger > .icon-bar1').removeClass('icon-bar11');
				$('.burger > .icon-bar2').removeClass('icon-bar21');
				$('.burger > .icon-bar3').removeClass('icon-bar31');
			}
		}
	});
	$(document).on("click", ".burger", function() {
		if (!$(this).hasClass('open')) {
			$('.user-icon').css({ 'opacity': '0', 'z-index': '0' });
			openMenu();
		} else {
			$('.circle').removeClass('bg-trans');
			closeMenu();
		}
	});
	function openMenu() {
		$('.circle').addClass('expand');
		$('.burger').addClass('open');
		$('.icon-bar1, .icon-bar2, .icon-bar3').addClass('collapse');
		$('.menu').fadeIn();
		setTimeout(function() {
			$('.icon-bar2').hide();
			$('.icon-bar1').addClass('rotate30');
			$('.icon-bar3').addClass('rotate150');
		}, 70);
		setTimeout(function() {
			$('.icon-bar1').addClass('rotate45');
			$('.icon-bar3').addClass('rotate135');
		}, 120);
	}
	function closeMenu() {
		$('.burger').removeClass('open');
		$('.icon-bar1').removeClass('rotate45').addClass('rotate30');
		$('.icon-bar3').removeClass('rotate135').addClass('rotate150');
		$('.circle').removeClass('expand');
		$('.menu').fadeOut();

		setTimeout(function() {
			$('.icon-bar1').removeClass('rotate30');
			$('.icon-bar3').removeClass('rotate150');
		}, 50);
		setTimeout(function() {
			$('.icon-bar2').show();
			$('.icon-bar1, .icon-bar2, .icon-bar3').removeClass('collapse');
		}, 70);
	}
});
</script>
</body>

</html>