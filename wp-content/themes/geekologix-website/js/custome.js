$(document).ready(function () {
     // Start Menu Js
     $('#menu_open').on('click', function () {
        $('.geeko-menu').slideToggle();
        $('.burger-icon').toggle();
        $('.x-icon').toggle();
        $('body').toggleClass('overflow-h');
         //$('.fixed-burger-icon .fal').toggleClass('fa-times');
    });
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 400) {
          $('.burger-icon').addClass('onscroll-color');
         }
        else {
            $('.burger-icon').removeClass('onscroll-color');
         }
      });
    // End Menu Js
    // Start Accordian js
    $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
        $(e.target).prev().find("i:last-child").toggleClass("fa-chevron-up fa-chevron-down");
    });
    // End Accordian js
    //
    $('#project_slider').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        center: true,
        lazyLoad: true,
        dots: true,
        stagePadding: 0,
        responsive: {
            0: {
                items: 1,
                stagePadding: 15
            },
            576: {
                items: 2,
                // stagePadding: 15
            },
            768: {
                items: 2,
            },
            1000: {
                items: 2,
                // stagePadding: 100
            },

        }
    })

    $('#framework_slider').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        // lazyLoad: true,
        dots: false,
        items: 6,
        responsive: {
            0: {
                items: 2,
                autoplay: true,
                autoplayTimeout: 3000,
            },
            600: {
                autoplay: true,
                autoplayTimeout: 3000,
                items: 3,
            },
            1000: {
                items: 6,
                // stagePadding: 100
            },

        }
    })
    $('#client_slider').owlCarousel({
        loop: true,
        margin: 44,
        autoplay: true,
        dots: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1,
                dots: false,
                nav: true
            },
            600: {
                items: 2,
                dots: false,
                nav: true
            },
            768: {
                items: 2,
            },
            1000: {
                items: 2,
            }
        }
    })
    $('#technology_stack').owlCarousel({
        loop: true,
        margin: 30,
        responsiveClass: true,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 3,
            },
            481: {
                items: 4,
            },
            600: {
                items: 5
            },
            768: {
                items: 6,
            },
            1000: {
                items: 10,
            }
        }
    });
    $('#industry_slider').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        dots: false,
        items: 1,
        center: true,
    });
    $('#see_card_slider').owlCarousel({
        loop: true,
        margin: 20,
        responsiveClass: true,
        nav: false,
        dots: false,
        items: 3,
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1,
                center: true,
            },
            600: {
                items: 2,
                center: true,
            },
            1000: {
                items: 3,
            }
        }
    });
    $('#hire_mean_slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        items: 1,

    })
})
