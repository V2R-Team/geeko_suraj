<?php
  // Template Name:Hire React Native Developer
  the_post();

  get_header(); ?>
<!-- Start Hire Dedicated -->
<section class="mobile_section our-portfolio hire-dedicated angularjs-bg" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <h1 class="banner-heading ">
                        Hire React Native Developer at Geekologix
                    </h1>
                    <p>
                        Hire a React Native Developer or a team of React Native Developers to work on your next web or
                        app development project. Providing fast React Native Development with dedicated and highly
                        productive developers.
                    </p>
                    <div class="hire-dedicated-btn">
                        <a href="<?php echo the_permalink(288) ?>" class="let-disuss-btn" title="Let's Discuss">Let's
                            Discuss</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="our-experts-form">
                        <div class="form-heading">
                            <h4>Request A Free Quote</h4>
                        </div>
                        <?php echo do_shortcode('[contact-form-7 id="357" title="REQUEST A FREE QUOTE"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Dedicated -->
<!-- Start Doveloper Hiring -->
<section class="developer-hiring">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <h2>Hire React Native Developers in India</h2>
                <p>
                    Ever wondered if you can have your own app running across various platforms without any bugs or
                    loading issues? With React Native, it is now possible to develop a super-adaptive and multi-platform
                    responsive app.

                </p>
                <p>
                    React Native Developers at Geekologix are experts at using the React in all its capabilities and
                    creating powerful mobile and web applications with it. Discuss your project with our developers to
                    get an idea about the development complexity and the time required to develop it. Our developers
                    will make sure that you are fully satisfied with the end product that is delivered to you.
                </p>
                <div class="developer-hiring-btn">
                    <a href="<?php echo get_permalink('288') ?>" title="Consult with React Native Experts"
                        class="read_more text-uppercase">Consult with
                        React Native Experts</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="developer-hiring-blocks">
                    <div class="row">
                        <div class="col-6">
                            <a href="<?php echo the_permalink(288) ?>" title="Budget-Friendly Pricing">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/lesser-capital-cost.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Budget-Friendly Pricing</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Flawless Coding">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/source-code.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Flawless Coding</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="Complete Confidentiality">
                                <div class="developer-hiring-block">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/non-disclosure.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>Complete Confidentiality</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:void(0)" title="On-time Delivery">
                                <div class="developer-hiring-block mt-30">
                                    <div class="hiring-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/less-time.svg"
                                            alt="" width="60">
                                    </div>
                                    <h5>On-time Delivery</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Hiring -->
<!-- Start Doveloper Timeing  -->
<section class="doveloper-timing">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Full-Time Hiring">
                    <div class="time-card full-time-bg">
                        <h4>Full-Time Hiring</h4>
                        <p class="min-h-38">If dedicated full-time development needed.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Part-Time Hiring">
                    <div class="time-card part-time-bg">
                        <h4>Part-Time Hiring</h4>
                        <p class="min-h-38">If required for only few hours each day.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>4 hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting</h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Hourly Hiring">
                    <div class="time-card hourly-time-bg">
                        <h4>Hourly Hiring</h4>
                        <p class="min-h-38">If required only for a few hours.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>2 hours/day, 5days/week (negotiable)</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Weekly</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3">
                <a href="javascript:void(0)" title="Contractual Hiring">
                    <div class="time-card contractual-time-bg">
                        <h4>Contractual Hiring</h4>
                        <p class="min-h-38">If required on project-basis.</p>
                        <ul class="time-card-list">
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-clock"></i>
                                </div>
                                <h6>Working Hours</h6>
                                <p>8hours/day, 5days/week</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-comment"></i>
                                </div>

                                <h6>Reporting </h6>
                                <p>Skype, Email, Phone</p>
                            </li>
                            <li>
                                <div class="time-card-icon">
                                    <i class="fas fa-file"></i>
                                </div>

                                <h6>Billing</h6>
                                <p>Monthly or end-term</p>
                            </li>
                        </ul>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Timeing  -->
<!-- Start Doveloper Expertise -->
<section class="doveloper-expertise">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3 class="comman_h text-center">
                    Custom React Native Application Development Services
                </h3>
                <p class="text-center comman_p">
                    React Developers at Geekologix excel at their coding capabilities, programming with a mix of modern
                    tools and high tech equipment to develop the best apps that support your business growth and
                    productivity across platforms and industries.
                </p>
            </div>
        </div>
        <div class="benefits-hire">
            <div class="row">
                <div class="col-lg-5">
                    <div class="shap-expertise-img ">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/shape-expertise.png" alt="">
                    </div>
                    <ul class="benefits-list">
                        <li>
                            <a href="javascript:void(0)" title="App Maintenance and Support">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-maintance.png"
                                        alt="">
                                    App Maintenance and Support
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="API Integration">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-intergration.png"
                                        alt="">
                                    API Integration
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="UI/UX App Design">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-ui.png" alt="">
                                    UI/UX App Design
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="App Maintenance and Support">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-maintance.png"
                                        alt="">
                                    App Maintenance and Support
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="React Native Web Development">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-web.png" alt="">
                                    React Native Web Development
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="React Native Mobile App Development">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-services.png"
                                        alt="">
                                    React Native Mobile App Development
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="React Native Consultation">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-tailored.png"
                                        alt="">
                                    React Native Consultation
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="React Native Game Development">
                                <div class="mobility-apps">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-services.png"
                                        alt="">
                                    React Native Game Development
                                </div>
                            </a>
                        </li>

                    </ul>

                </div>
                <div class="col-lg-6 offset-lg-1">
                    <h4 class="comman_h">Leverage the Power of React Native for your business</h4>
                    <p class="comman_p">
                        Our developers are ready to help you boost your business growth with powerful React Native
                        Mobile apps and web services tailored especially for you. With our services you can:
                    </p>
                    <ul class="comman_list">
                        <li>Make your products and services available and visible on all platforms with your own
                            mobile app and website</li>
                        <li>Buildyour digital branding profile with creative display solutions</li>
                        <li>Get affordable multi-platform development and Save more with Hybrid App Development</li>
                        <li>Relax and enjoy the process while we create stylish portfolios for you</li>
                    </ul>
                    <p class="comman_p">
                        Our React Native App Development services can be highly customised according to your
                        requirements and industry standards, ensuring your max benefit from our services. We ensure you
                        the best deals and most cost-efficient react native development which is fast, comprehensible
                        and scalable.
                    </p>
                    <div class="benefits-btn">
                        <a href="<?php echo the_permalink(288) ?>" title="Let’s Talk"
                            class="read_more text-uppercase">Let’s
                            Talk</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Doveloper Expertise -->
<!-- Start Angular Steps -->
<section class="angular-steps">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire React Native Developers in 5 Simple Steps</h4>
                <p class="comman_p text-center">We value your time and money. Hiring developers at Geekologix is easy
                    and fast. We make sure that our services match your project requirements.</p>
            </div>
        </div>
        <div class="ang-step-cards">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="step-card-list">
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="Request Hiring">
                                <h5>01</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-01.png" alt="">
                                </div>
                                <h6>Request Hiring</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="View and Select Developers">
                                <h5>02</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-02.png" alt="">
                                </div>
                                <h6>View and Select Developers</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link" title="Interview and Approve">
                                <h5>03</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-03.png" alt="">
                                </div>
                                <h6>Interview and Approve</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Get the best Deal for your Project Terms">
                                <h5>04</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-04.png" alt="">
                                </div>
                                <h6>Get the best Deal for your Project Terms</h6>
                            </a>
                        </li>
                        <li class="ang-step-card">
                            <a href="javascript:void(0)" class="step-card-link"
                                title="Start your Development with our team">
                                <h5>05</h5>
                                <div class="step-card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/step-05.png" alt="">
                                </div>
                                <h6>Start your Development with our team</h6>
                            </a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Angular Steps -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire React Native Developers on the basis of Work Location</h4>
            </div>
        </div>
        <div class="experts-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Site">
                        <div class="experts-card">
                            <div class="expert-img on-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/on-site.png" alt="">
                            </div>
                            <h5>ON-SITE</h5>
                            <p>Hire React Native Developers to work with you at your office or preferred Job Location,
                                so that you can work with more transparency and speed.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Off-Site">
                        <div class="experts-card">
                            <div class="expert-img off-site-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/off-site.png" alt="">
                            </div>
                            <h5>OFF-SITE</h5>
                            <p>Hire React Native Developers to work from our development office at your preferred time.
                                We ensure full transparency, total reporting and on-time delivery.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hybrid">
                        <div class="experts-card">
                            <div class="expert-img hybrid-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/hybrid.png" alt="">
                            </div>
                            <h5>Hybrid</h5>
                            <p>Hire React Native developers who are ready to work at your flexibility. Get a mix of the
                                onsite and offsite working to match your requirements.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Why To Hire -->
<section class="why-to-hire react-hire">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">
                    Benefits of Choosing Geekologix as your React Native Development Partner
                </h4>
            </div>
        </div>
        <div class="hire-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Passionate React Native Coders">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                1
                            </div>
                            <h5>Passionate React Native Coders</h5>
                            <p>At Geekologix, We hire coders and programmers who are really passionate about what they
                                do and how they do it. They come up with the most creative and quick solutions for each
                                project, and it is a pleasant sight to see them bringing your idea into reality.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Dedicated and Diligent Team">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                2
                            </div>
                            <h5>Dedicated and Diligent Team</h5>
                            <p>
                                An advantage of hiring passionate developers is that they work with complete honesty and
                                discipline to their work. Our teams are very dedicated and work diligently on the task
                                assigned to them, to turn it in as per the given deadline.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Experienced Team Managers">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                3
                            </div>
                            <h5>Experienced Team Managers</h5>
                            <p>
                                Geekologix Teams are hierarchy based. They are headed by experienced team managers who
                                schedule and monitor each task as well as double check it before delivery. Such team
                                managers enable our teams to work on big projects smoothly.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Project Transparency">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                4
                            </div>
                            <h5>Project Transparency</h5>
                            <p>
                                Projects at Geekologix are carried out with full transparency with the clients. Our team
                                managers will be eager to provide you with all the minute details of your project, with
                                accurate task completion schedules and project status for your satisfaction.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Live Reporting">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                5
                            </div>
                            <h5>Live Reporting</h5>
                            <p>
                                Along with the transparency schedules, our team are also used to for live reporting
                                projects. We give our clients the liberty to ask for current project status or put any
                                live query whenever they want, and always revert to them with sincerity.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="No Hidden Charges">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                6
                            </div>
                            <h5>No Hidden Charges</h5>
                            <p>
                                We do not put any hidden charges in the projects during the billing process. We make
                                sure that everything is discussed at the commencement of the project and followed
                                throughout as per your considerations, on your terms and conditions.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="100% Confidentiality">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                7
                            </div>
                            <h5>100% Confidentiality</h5>
                            <p>
                                For all our projects, we maintain complete confidentiality on our ends. We ensure that
                                there are no data leaks. No information goes out from our development centre without the
                                instructions from our clients.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Secured React Native Web/App">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                8
                            </div>
                            <h5>Secured React Native Web/App</h5>
                            <p>
                                All the apps and websites built at our development centres are completely secure and
                                free of data leaks. Our teams are trained about the critical importance of
                                confidentiality during the development process and abide by it.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="On-Time Project Delivery">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                9
                            </div>
                            <h5>On-Time Project Delivery</h5>
                            <p>
                                We value our project delivery promises. Our development teams are true to their words.
                                They ensure that the projects are finished within the promised deadlines through
                                thorough planning and scheduling.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Best Pricing Deals">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                10
                            </div>
                            <h5>Best Pricing Deals</h5>
                            <p>
                                At Geekologix, you can get your requirements fulfilled in the most affordable and fairly
                                priced deals with assured project quality. We deliver what we promise, and we charge
                                fairly keeping in mind your project requirements.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="
                    Thorough Testing Before Delivery
                    ">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                11
                            </div>
                            <h5>
                                Thorough Testing Before Delivery
                            </h5>
                            <p>
                                Tell us, in case you want to add or decrease the number of hired developers. We will
                                take action at its quick
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Queries, Bug-fixes, and Maintenance">
                        <div class="hire-card">
                            <div class="hire-card-img">
                                12
                            </div>
                            <h5>Queries, Bug-fixes, and Maintenance</h5>
                            <p>
                                Even after the completion of the projects, we make sure that our clients do not face any
                                difficulties in handling the project. We are glad to solve their queries, bugs and
                                maintenance issues even after the official project closure.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Why To Hire -->
<!-- Start Ang Experts -->
<section class="ang-experts">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Hire Flexible React Native Developers at Geekologix</h4>
            </div>
        </div>
        <div class="experts-cards flexible-cards">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Fixed Charge hiring for Start-ups">
                        <div class="experts-card">
                            <div class="expert-img fixed-rate-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Fixed Charge hiring for Start-ups</h5>
                            <p>If you are looking to hire developers for your start-up, we suggest you to convey your
                                requirements to us, so that we can provide you with the best possible fixed price
                                quotation which suits your business venture.</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Dedicated team hiring for Enterprises">
                        <div class="experts-card">
                            <div class="expert-img dedicated-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Dedicated team hiring for Enterprises</h5>
                            <p>If you are looking to hire for your enterprise, it is better to hire dedicated teams to
                                work for you with full attention, giving your project more time on a day-to-day basis.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="javascript:void(0)" title="Hourly hiring for Entrepreneurs">
                        <div class="experts-card">
                            <div class="expert-img hourly-bg">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/step-right.png" alt="">
                            </div>
                            <h5 class="text-capitalize">Hourly hiring for Entrepreneurs</h5>
                            <p>If you are looking for developers to work on your individual or personal ventures, it is
                                best to hire developers on a hourly basis which would prove to be more pocket-friendly
                                for you, while fulfilling all your development needs.</p>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Ang Experts -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                        $testimonialSlider =  array(
                            'post_status'    => 'publish', 
                            'post_type'      => 'client_slider', 
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                          );
                          $getTestimonialData = new WP_Query($testimonialSlider);
                          if($getTestimonialData ->have_posts()) {
                          while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                          $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                            $testimonial_img = $testimonialimg[0];
                        ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                        endwhile;
                    } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Faq  -->
<section class="faq">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>Frequently Asked Question</h4>
            </div>
            <div class="col-lg-7">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne"
                                    title="How can I hire a React Native Developer/Developers according to my preferences?">
                                    How can I hire a React Native Developer/Developers according to my preferences?
                                    <span class="fa-stack fa-sm">
                                        <i class="fas fa-chevron-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    At Geekologix, you only pay for what you like. At the commencement of each project,
                                    we send you resumes of our developers from which you can select and filter the ones
                                    that you want on your team. You can also interview the developers individually if
                                    you want. For short projects, our team manager himself chooses the best suited
                                    developer to work on your project, so as to save your time and efforts.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo" title="What are your pricing specifications?">
                                    What are your pricing specifications?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Before we start working on your React Native Project, me discuss all the pricing
                                    details with you which are based on your specific project requirements and
                                    preferences. The idea is to provide you the best deals tailored as per your
                                    individual projects.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree"
                                    title="How long will it take to finish my React Native project?">
                                    How long will it take to finish my React Native project?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Before our team starts working on your React Native project, we discuss the project
                                    details and some suggested timelines with you, as per the time required by our
                                    developers to finish your project. The suggested timelines are always negotiable.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFoure">
                            <h6 class="mb-0">
                                <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                    data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false"
                                    aria-controls="collapseFoure"
                                    title="What is the process for hiring React Native developers at Geekologix?">
                                    What is the process for hiring React Native developers at Geekologix?
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-chevron-down fa-stack-1x fa-inverse"></i>
                                    </span>
                                </button>
                            </h6>
                        </div>
                        <div id="collapseFoure" class="collapse" aria-labelledby="headingFoure"
                            data-parent="#accordion">
                            <div class="card-body">
                                <p class="myaccordion-text">
                                    Geekologix is where you can hire React Native Developers in minimum time and
                                    efforts. Your hiring is finished in 3 easy steps:
                                </p>
                                <ul class="comman_list">
                                    <li>Request Hiring by conveying your requirements</li>
                                    <li>View, Select and Interview Developers</li>
                                    <li>Sign the Deal and Start Development instantly</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="faq-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/frequently-question.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Faq  -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type=" email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type=" other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type=" instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>