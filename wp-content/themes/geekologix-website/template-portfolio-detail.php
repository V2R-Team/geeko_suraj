<?php 
// Template Name:Portfolio Detail
  get_header();
?>
<!-- Start Our Portfolio -->
<section class="mobile_section our-portfolio" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-7 col-md-9 ">
                    <h1 class="banner-heading">
                        Play Glam
                    </h1>
                    <p>
                        PlayGlam is a Professional Beauty Salon providing a spectrum of beauty services at the
                        doorsteps.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Portfolio -->
<!-- Start Play Gam Details -->
<section class="play-gam-details">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <div class="play-glam-logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/play-glam-logo.jpg" alt=""
                        class="img-fluid">
                </div>
            </div>
            <div class="col-md-6 col-lg-7 col-xl-7">
                <div class="portfolio-detail-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/play-glam-left.png" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-5 col-xl-5">
                <div class="portfolio-list-heading portfolio-detail-heading">
                    <h3>Client's Profile</h3>
                    <p class="pt-8">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.
                    </p>
                    <p>
                        Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                    <ul class="view-study-btns">
                        <li>
                            <a href="" title="Android" class="portfolio-icon-link">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/android-icon.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="" class="portfolio-icon-link" title="Iphone">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/apple-icon.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="" class="portfolio-icon-link" title="Website">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.jpg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Play Gam Details -->
<!-- Start Technology Stack -->
<section class="technology-stack">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2>Technology Stack</h2>
            </div>
        </div>
        <div class="technology-stack-list">
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel owl-theme" id="technology_stack">
                        <a href="javascript:void(0)" title="Android">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-android.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="Kotlin">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-kotlin.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="React">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-react.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="Java">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-java.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="Flutter">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-flutter.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="swift">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-swift.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="laravel">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-larawel.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="Php">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-php.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="Html">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-html.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" title="Css">
                            <div class="technology-stack-item item">
                                <div class="techology-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/tecnology-css.jpg"
                                        alt="">
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Technology Stack -->
<!-- Start Industry Intro -->
<section class="industry-intro">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3>Industry Introduction</h3>
                <p>
                    PlayGlam is a Professional Beauty Salon providing a spectrum of beauty services at the doorsteps.
                    It delivers beauty masters at the client’s doorsteps rendering services that can range from mere
                    blow dry, to make-up applications, or just a simple nail paint change.
                </p>
                <div class="text-center industry-slider">
                    <div class="owl-carousel owl-theme" id="industry_slider">
                        <div class="item">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/play-gam-slider.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/play-gam-slider.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
</section>
<!-- End Industry Intro -->

<!-- Start Features Play Glam -->
<section class="features-play-glam">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h3>Features of Play Glam</h3>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="features-content">
                    <h5>App Features</h5>
                    <ul>
                        <li>It is an easy and convenient, on-demand beauty service App.</li>
                        <li>Book makeup, hair, tanning, nails, and more all in the comfort
                            of your home, hotel, office, gym, or anywhere else.</li>
                        <li>Book services, gift services, book bundles and packages, look
                            via portfolios, also take benefit of our discount codes</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="features-content">
                    <h5>Web Features</h5>
                    <ul>
                        <li>It is an easy and convenient, on-demand beauty service App.</li>
                        <li>Book makeup, hair, tanning, nails, and more all in the comfort
                            of your home, hotel, office, gym, or anywhere else..</li>
                        <li>Book services, gift services, book bundles and packages, look
                            via portfolios, also take benefit of our discount codes</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Features Play Glam -->

<!-- Start Waiting for -->
<section class="waiting-for">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-xl-10 offset-lg-1 offset-xl-1">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="waiting-for-content">
                            <h4>What are you waiting for?</h4>
                            <p>Share your imagination and take home a feature full
                                Blockchain solution.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="our-experts-form">
                            <form action="">
                                <div class="form-group">
                                    <input type="text" name="" id="" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="" id="" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="" id="" class="form-control" placeholder="Massage">
                                </div>
                                <div class="talk-to-btn">
                                    <button type="submit" class="btn" title="Talk To Your Expert">Talk To Your
                                        Expert</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Waiting for -->
<!-- Start See More Case -->
<section class="see-more-case">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4>See more case studies</h4>
                <div class="owl-carousel owl-theme" id="see_card_slider">
                    <div class="item">
                        <a href="javascript:void(0)" title="PursueIt">
                            <div class="card">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/slider-persuit.jpg"
                                    class="card-img-top" alt="PursueIt">
                                <div class="card-body">
                                    <h5 class="card-title">PursueIt</h5>
                                    <p class="card-text">PursueIt is an online Platform across UAE that assists
                                        individuals in
                                        discovering and experiencing their interests.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="javascript:void(0)" title="Jodhpuri Furniture">
                            <div class="card">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/slider-jodhpuri.jpg"
                                    class="" alt="Jodhpuri Furniture">
                                <div class="card-body">
                                    <h5 class="card-title">Jodhpuri Furniture</h5>
                                    <p class="card-text">Jodhpuri Furniture is a One-Stop-Shop E-commerce website for
                                        all
                                        furniture
                                        requirements.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="javascript:void(0)" title="Mesmo">
                            <div class="card">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/slider-mesmo.jpg"
                                    class="card-img-top" alt="Mesmo">
                                <div class="card-body">
                                    <h5 class="card-title">Mesmo</h5>
                                    <p class="card-text">Mesmo is an online community that provides local people
                                        experiences
                                        with recommendations.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End See More Case -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>