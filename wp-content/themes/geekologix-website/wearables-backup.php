<!-- Start Section 3 we serve -->
<section class="we_serve common_sections">
	<div class="container">
		<div class="row text-center ">
			<h4  class="text-uppercase inner_heading mx-auto">Industries we serve</h4>
		</div>
		<div class="row">
			          <!-- start loop -->
            <?php
                $features =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'features', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getfeaturesData = new WP_Query($features);
                   $count = 1;
                 
                  if($getfeaturesData->have_posts()) {
                  while ($getfeaturesData->have_posts()) : $getfeaturesData->the_post();
                  $get_features_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                    $featuresimage = $get_features_image[0];
                    ?>
			<div class="col-lg-2">
				<div class="industry_block real_state_outer text-center">
					<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-Estate.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/real-estate copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images real_state mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Real Estate</h5>
				</div>
			</div>
	<div class="col-lg-2">
				<div class="industry_block education_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/education-copy.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/education.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images education mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Education</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block healthcare_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/healthcare-copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images healthcare mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Healthcare</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block finance_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/finance-copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images finance mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Finance</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block automotives_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/automotives copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images automotives mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Automotives</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block gaming_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/gaming-copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images gaming mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Gaming</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block event_tickets_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/event & Tickets copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images event_tickets mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Event & Tickets</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block travel_tours_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/travel & Tours copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images travel_tours mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Travel & Tours</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block ecommerce_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/ecommerce-copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images ecommerce mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Ecommerce</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block entertainment_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/entertainment-copy.png" alt="" class="img_hover mx-auto">
					</div>
				<!-- 	<div class="industry_images entertainment mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Entertainment</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block food_beverage_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/food & Beverage copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images food_beverage mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Food & Beverage</h5>
				</div>
			</div>
				<div class="col-lg-2">
				<div class="industry_block  fitness_outer text-center">
						<div class=" mx-auto">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness.png" alt="" class="img_desktop">
							<img src="<?php  echo get_template_directory_uri() ?>/images/icon/fitness-copy.png" alt="" class="img_hover mx-auto">
					</div>
				<!-- 	<div class="industry_images fitness mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize">Fitness</h5>
				</div>
			</div>
			<a href="" class="common_btns let_started text-uppercase mx-auto">Let's Get Started</a>
		</div>
	</div>
</section>
<!-- End Section 3 we serve -->
<!-- dynamic -->
<!-- Start Section 3 we serve -->
<section class="we_serve common_sections">
	<div class="container">
		<div class="row text-center ">
			<h4  class="text-uppercase inner_heading mx-auto">Industries we serve</h4>
		</div>
		<div class="row">
			          <!-- start loop -->
            <?php
                $features =  array(
                    'post_status'     => 'publish', 
                    'post_type'       => 'industry_serve', 
                    'posts_per_page'  =>  -1,
                    'order'           => 'ASC',
                  );
                  $getfeaturesData = new WP_Query($features);
                   // $count = 1;
                 
                  if($getfeaturesData->have_posts()) {
                  while ($getfeaturesData->have_posts()) : $getfeaturesData->the_post();
                  $get_features_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );

                    $featuresimage = $get_features_image[0];
                    ?>
			<div class="col-lg-2">
				<div class="industry_block real_state_outer text-center">
					<div class=" mx-auto">
							<img src="<?php echo $featuresimage; ?>" alt="" class="img_desktop">
							<img src="<?php echo get_template_directory_uri() ?>/images/icon/real-estate copy.png" alt="" class="img_hover mx-auto">
					</div>
					<!-- <div class="industry_images real_state mx-auto">
						<img src="<?php  echo get_template_directory_uri() ?>/images/real-state.png" alt="">
					</div> -->
					<h5 class="text-capitalize"><?php echo the_title(); ?></h5>
				</div>
			</div>
         <?php 
               endwhile;
            } ?>
            <div class="col-lg-12 text-center">
            				<a href="" class="common_btns let_started text-uppercase mx-auto">Let's Get Started</a>

            </div>
		</div>
	</div>
</section>
<!-- End Section 3 we serve -->