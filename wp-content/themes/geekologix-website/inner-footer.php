<section class="common_footer common_sections">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
        <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-7  col-md-7 pr-0">
        <div class="form_part">
          <div class="row">
            <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
          </div>
      <!--    <form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
        </div>
      </div>
      <div class="col-lg-5  col-md-5 pl-0">
        <div class="right_side">
          <h5 class="text-uppercase">Reach Us</h5>
          <ul>
            <li>
              <i class="fal fa-map-marker-alt float-left"></i>
              <p class="float-left">  <?php echo do_shortcode('[contact type="office_address"]') ?></p>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fal fa-envelope"></i>
              <a class="float-left" href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>" title="info@geekologix.com
"> <?php echo do_shortcode('[contact type="email_address"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fab fa-skype"></i>
              <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>" title="Geekologix Technologies"> <?php echo do_shortcode('[contact type="skype"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li class="pr-lg-5">
              <!-- <i class="fal fa-phone"></i> -->
              <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt="" class="fa_phone">
              <a class="float-left" href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>" title="+91 - 856 108 9567"> India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
              <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>" title="+91 - 978 241 9155"> <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
              USA:
                <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>" title="+61 408 15 88 02"> <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>  
                <div class="clearfix"></div>
            </li>
          </ul>
          <div class="offices">
            <div class="float-left">
              Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india" class="ml-2"><span class="india_div">India</span>
            </div>
            <div  class="float-left usa-div">
            
            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span class="">USA</span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- End Inner footer -->