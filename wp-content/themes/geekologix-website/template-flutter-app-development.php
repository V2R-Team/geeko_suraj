<?php 
//Template Name: Flutter App Development
the_post();
get_header(); 
?>
<!-- Start section 1 -->
<section class="mobile_section" id="">
    <div id="" class="mobile_wearables">
        <?php include 'header2.php'; ?>
        <div class="container px-0 common_heading  detail_heading">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h1 class="banner-heading">
                        <?php 

              $about_ID = 168;
              $about_title = get_page($about_ID);
             echo the_title();?>
                        <!--  <div id="text-type"></div> -->
                    </h1>
                    <p>Accelerated Development with compact coding, Flutter App Development at its best.</p>
                    <?php echo the_content(); ?>
                    <a href="<?php echo get_permalink('313'); ?>/#showcase" class="text-uppercase requst_quote common_btns" title="Our Showcase">Our Showcase</a>
                    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
                    <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/wearable-banner.png"
                        class="img-fluid mx-auto banner_img">
                    <!-- <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>"
						class="img-fluid mx-auto banner_img"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section 1 -->
<!-- Start Web Application  -->
<section class="web-application">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h2 class="comman_h text-center">Custom Flutter App Development Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="web-application-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/flutter-app-left.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h5>
                    App developers at Geekologix are building Robust Apps with one of the most revolutionary frameworks
                    — Flutter — for building android and IOS Apps in very short timelines.
                </h5>
                <p class="comman_p">
                    The benefit of using Flutter for app development is that it allows its users to develop apps quickly
                    with the help of widgets and a single code base. Flutter also allows users to manage all the
                    features that one would need to improve the app performance on a native platform, such as
                    navigation, scrolling, fonts, icons, etc.
                </p>
                <p class="comman_p">
                    At Geekologix, one of the Top App Development Company, app developers are experts at using Flutter
                    for app development. It is, in fact, one of our majorly used frameworks. Our apps are bug-free,
                    highly functional, platform adaptive and come with attractive UI/UX designs created specifically to
                    suit your business profile.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Web Application  -->
<!-- Start Geekologix Advantages -->
<section class="geekologix-advantages">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Geekologix as your Flutter App Development Partner</h4>
                <p class="comman_p text-center">Find your most suited flutter app development partners at Geekologix and
                    create powerful realities with them.</p>
            </div>
        </div>
        <div class="advantage-cards">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)">
                        <div class="advantage-card" title="Flexible Hiring">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/flexible-modal.png" alt="">
                            <div class="advantage-card-content">
                                <h5>Flexible Hiring</h5>
                                <p>Choose your app development partners by scanning and selecting through resumes, or
                                    interviewing the developers personally. Find your perfect match.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Pocket-Friendly App Development">
                        <div class="advantage-card">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cost-advantage.png" alt="">
                            <div class="advantage-card-content">
                                <h5>Pocket-Friendly App Development</h5>
                                <p>No overly priced deals, no hidden charges, no aftermath charges. At Geekologix, we
                                    promise you the most cost-effective app development services ever.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Industry Experience">
                        <div class="advantage-card">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/global-advantage.png" alt="">
                            <div class="advantage-card-content">
                                <h5>Industry Experience</h5>
                                <p>Our app development team has worked on various app development projects across
                                    industries and business categories contributing to their knowledge and expertise.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Creativity, Skills and Expertise">
                        <div class="advantage-card">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/skills-advantage.png" alt="">
                            <div class="advantage-card-content">
                                <h5>Creativity, Skills and Expertise</h5>
                                <p>Our passionate developers work with creativity and requirements on their mind. They
                                    come up with the most innovative solutions to fulfil your app development needs.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Working on prerequisites">
                        <div class="advantage-card">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/on-time-advantage.png" alt="">
                            <div class="advantage-card-content">
                                <h5>Working on prerequisites</h5>
                                <p>We are concerned about your time and money. We put in our best efforts to make sure
                                    none of it is wasted at our ends, and you are served top-notch quality within the
                                    promised budget and time.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <a href="javascript:void(0)" title="Support and Maintenance">
                        <div class="advantage-card">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/continous-icon.png" alt="">
                            <div class="advantage-card-content">
                                <h5>Support and Maintenance</h5>
                                <p>Our team managers stay in touch with you to cater to your needs and desired right
                                    from the project commencement until the closure. We are glad to resolve your queries
                                    even after the project is closed.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Geekologix Advantages -->
<!-- Start Our Key Offerings -->
<section class="our-key-offerings">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <h4 class="comman_h text-center">Flutter App Development Services by Geekologix</h4>
            </div>
        </div>
        <div class="Offerings-cards">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Financial and e-Commerce Flutter Applications">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/financial-offering.png"
                                    alt="">
                            </div>
                            <h5>Financial and e-Commerce Flutter Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Travel and Booking Flutter Solutions">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/travel-offering.png"
                                    alt="">
                            </div>
                            <h5>Travel and Booking Flutter Solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Flutter ERP Solutions">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/erp-offering.png" alt="">
                            </div>
                            <h5>Flutter ERP Solutions</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Flutter e-Learning Application Development">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ios-offering.png" alt="">
                            </div>
                            <h5>Flutter e-Learning Application Development</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Social Media Flutter Applications">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/social-offering.png"
                                    alt="">
                            </div>
                            <h5>Social Media Flutter Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Location-Based Flutter Applications">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/location-offering.png"
                                    alt="">
                            </div>
                            <h5>Location-Based Flutter Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Flutter Media Applications">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/gaming-offering.png"
                                    alt="">
                            </div>
                            <h5>Flutter Media Applications</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-4 col-lg-3">
                    <a href="javascript:void(0)" title="Maintenance & Backend Support">
                        <div class="Offerings-card">
                            <div class="offerins-card-img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/backend-offering.png"
                                    alt="">
                            </div>
                            <h5>Maintenance & Backend Support</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Key Offerings -->
<!-- Start section 5 Projects -->
<?php include('our-showcse.php') ?>
<!-- End section 5 Projects -->
<!-- Start section 9 Technology Framework-->
<section class="technology_framework">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class=" inner_heading mx-auto">Key Tech Expertise</h4>
            </div>
        </div>
        <div class="framework-slider">
            <div class="row">
                <div class="col-12">
                    <!-- start loop -->
                    <div class="owl-carousel owl-theme" id="framework_slider">
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Android">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/android.png" alt=""
                                        class="mx-auto">
                                    <h5>Android</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Kotlin">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/kotlin.png"
                                        alt="Kotlin" class="mx-auto">
                                    <h5>Kotlin</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="React Native">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/react-native.png"
                                        alt="React Native" class="mx-auto">
                                    <h5>React Native</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Java">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/java.png" alt="Java"
                                        class="mx-auto">
                                    <h5>Java</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Core Data">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/core-data.png"
                                        alt="Core Data" class="mx-auto">
                                    <h5>Core Data</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item text-center">
                            <a href="javascript:void(0)" title="Material Design">
                                <div class="framework_block">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/material-design.png"
                                        alt="Material Design" class="mx-auto">
                                    <h5>Material Design</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 9 Technology Framework -->
<!-- Start Section 7 client review -->
<section class="client_review common_sliders ">
    <div class="container px-0">
        <div class="row">
            <h4 class=" inner_heading mx-auto">Client's Reviews</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="client_slider">
                    <!-- start loop -->
                    <?php
                    $testimonialSlider =  array(
                        'post_status'    => 'publish', 
                        'post_type'      => 'client_slider', 
                        'posts_per_page' => -1,
                        'order'          => 'DESC',
                      );
                      $getTestimonialData = new WP_Query($testimonialSlider);
                      if($getTestimonialData ->have_posts()) {
                      while ($getTestimonialData ->have_posts()) : $getTestimonialData ->the_post();
                      $testimonialimg = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                        $testimonial_img = $testimonialimg[0];
                    ?>
                    <div class="item">
                        <div class="client_block">
                            <div class="client_img">
                                <img src="<?php echo  $testimonial_img; ?>" alt="" class="rounded-circle">
                            </div>


                            <?php echo the_content(); ?>

                        </div>

                    </div>

                    <?php 
                    endwhile;
                } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section 7 client review -->
<!-- Start Hire Android App Developers -->
<section class="hire-android-app-developer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-7 col-xl-7">
                <h4 class="comman_h">Hire Expert Flutter App Developers at Geekologix</h4>
                <h5>Robust Flutter Apps, built with passion and creativity, specifically for your business needs.</h5>
                <p>Your app ideas are not just ideas anymore. They can be the basis of our development, resulting in
                    amazing innovative apps that accelerate and empower your business growth.</p>
                <div class="hire-dedicated-btn">
                    <a href="<?php echo get_permalink('288') ?>" class="let-disuss-btn" title="Hire Now">Hire Now</a>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5">
                <div class="hire-app-developer-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hire-app-developer.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hire Android App Developers -->
<!-- Start section 8 Lets Talk -->
<section class="lets_banner">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Let Us Discuss your Next IOS App Idea?</h4>
                <h4 class="text-uppercase inner_heading mx-auto">Start Innovating.</h4>
                <a href="" class="common_btns text-uppercase requst_quote" title="Let's Talk"> Let's Talk</a>
            </div>
        </div>
    </div>
</section>
<!-- End Section 8 Lets Talk -->
<!-- start Inner footer -->
<section class="common_footer">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
                <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7  col-md-7 pr-0">
                <div class="form_part">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
                    </div>
                    <!-- 		<form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
                </div>
            </div>
            <div class="col-lg-5  col-md-5 pl-0">
                <div class="right_side">
                    <h5 class="text-uppercase">Reach Us</h5>
                    <ul>
                        <li>
                            <i class="fal fa-map-marker-alt float-left"></i>
                            <p class="float-left"> <?php echo do_shortcode('[contact type="office_address"]') ?></p>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fal fa-envelope"></i>
                            <a class="float-left"
                                href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                                <?php echo do_shortcode('[contact type="email_address"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <i class="fab fa-skype"></i>
                            <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>">
                                <?php echo do_shortcode('[contact type="skype"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fa-phone.png" alt=""
                                class="fa_phone">
                            <a class="float-left"
                                href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                                India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
                            <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
                            <br>
                            USA:
                            <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                                <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <div class="offices">
                        <div class="float-left">
                            Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india"
                                class="ml-2"><span class="india_div">India</span>
                        </div>
                        <div class="float-left usa-div">

                            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span
                                class="">USA</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Inner footer -->
<!--Start contact  section7-->
<?php
    $contact_ID = 51;
    $contact_data = get_page($contact_ID);
    $conatct_title = $contact_data->post_title;
    $conatct_content = $contact_data->post_content;
  ?>
<section class="section" id="section7">
    <div class="footer-overlay"></div>
    <div class="container p-0">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title text-uppercase text-center">
                    <?php echo  $conatct_title ?>
                </h4>
            </div>
            <!-- Let's talk about you, -->
            <div class="col-lg-12 p-lg-0">
                <div class="let-talk-about">
                    <?php echo $conatct_content; ?>
                    <!-- <a href="#" title="Need a Help!" class="need-help web-btn text-uppercase">Need a Help!</a> -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0  footer-links footer-links1">
                <div class="footer-responsive">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/footer-icon.png">
                    </div>
                    <address class="address-footer address-footer2">
                        <h5>
                            <?php echo do_shortcode('[contact type="office_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>">
                            <i class="fal fa-phone"></i>
                            <?php echo do_shortcode('[contact type="india mobile"]') ?>
                        </a>
                        <i class="fal fa-map-marker-alt"></i>
                        <p>
                            <?php echo do_shortcode('[contact type="office_address"]') ?>
                        </p>
                        <div class="clearfix"></div>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fal fa-envelope"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <a href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>">
                            <i class="fab fa-skype"></i>
                            <?php echo do_shortcode('[contact type="email_address"]') ?>
                        </a>
                        <div class="clearfix"></div>
                    </address>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 text-center footer-links">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile2"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pl-lg-0 pr-lg-0 footer-links text-center">
                <div class="footer-add">
                    <div class="float-icon">
                        <img src="<?php  echo get_template_directory_uri() ?>/images/office-icon.png">
                    </div>
                    <address class="address-footer">
                        <h5>
                            <?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>
                        </h5>
                        <a href="tel:<?php echo do_shortcode('[contact type="other_ofc_name2"]') ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/images/call.png">
                            <?php echo do_shortcode('[contact type="other_mobile"]') ?>
                        </a>
                    </address>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row copy-footer">
            <div class="col-lg-6 col-12 col-md-6 pl-0 pr-0 text-left">
                <p class="copy-right">
                    &copy;
                    <?php echo date('Y') ?> Geekologix Technologies Pvt Ltd | All Rights Reserved.
                </p>
            </div>
            <div class="col-lg-6 col-md-6 pl-0 pr-0 text-right footer-social">
                <ul>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>" target="_blank"
                            title="facebook"> <i class="fab fa-facebook-f"></i></a>
                    </li>
                    <!--  <li>
                        <a href="<?php //echo do_shortcode('[contact type="twitter"]') ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                    </li> -->
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="linkedin"]') ?>" target="_blank"
                            title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>" target="_blank"
                            title="instagram"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End contact  section7-->
<?php include('inner-footer-2.php'); ?>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>