<?php
  // Template Name:Industry Solutions Page
  the_post();
  /**
  * The main template file
  *
  * This is the most generic template file in a WordPress theme
  * and one of the two required files for a theme ( the other being style.css ).
  * It is used to display a page when nothing more specific matches a query.
  * E.g., it puts together the home page when no home.php file exists.
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package geekologix_website
  */
  
  get_header(); ?>

  <div id="" class="mobile_wearables">
<?php include 'header2.php'; ?>
    <div class="container-fluid common_heading  detail_heading">
      <div class="row">
        <div class="col-lg-6 col-md-6 pr-0">
          <h1 class="banner-heading ">
            <?php 

 $about_ID = 168;
    $about_title = get_page($about_ID);
             echo the_title();?>
            <!--  <div id="text-type"></div> -->
          </h1>
          <?php echo the_content(); ?>
          <a href="" class="text-uppercase requst_quote common_btns">Request a quote</a>
    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
          <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
        </div>
             <div class="col-lg-6 col-md-6">
 <img src=" <?php echo get_field('upload_banner_image',$about_ID); ?>" class="img-fluid mx-auto banner_img">             </div>
      </div>
    </div>
  </div>
</section>
<!-- End section 1 -->

<!-- start Inner footer -->
<section class="common_footer common_sections">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4 class="text-uppercase inner_heading mx-auto">Convert your idea into reality With Our Experts!!</h4>
        <p class="inner_content">Let’s colaborate to improve the world through design & technology.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-7  col-md-7 pr-0">
        <div class="form_part">
          <div class="row">
            <?php echo do_shortcode('[contact-form-7 id="215" title="Convert Idea"]'); ?>
          </div>
      <!--    <form>
            <div class="row">
              <div class="col-lg-6">
                <label> Your Name</label>
                <input type="text" class="form-control" name="" placeholder="Johan Smith">
              </div>
              <div class="col-lg-6">
                  <label>Email Adress</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-6">
                  <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="91+123456789" name="">
              </div>
              <div class="col-lg-6">
                  <label>I'am Interested in</label>
                <input type="text" class="form-control" placeholder="johansmith@gmail.com" name="">
              </div>
              <div class="col-lg-12">
                  <label>Messge</label>
                <input type="text" class="form-control messages" placeholder="Hi, do you have a moment to talk..." name="">
              </div>
                <div class="col-lg-12">
              <button class="common_btns">Send Now</button>
            </div>
            </div>
            
          </form> -->
        </div>
      </div>
      <div class="col-lg-5  col-md-5 pl-0">
        <div class="right_side">
          <h5 class="text-uppercase">Reach Us</h5>
          <ul>
            <li>
              <i class="fal fa-map-marker-alt float-left"></i>
              <p class="float-left">  <?php echo do_shortcode('[contact type="office_address"]') ?></p>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fal fa-envelope"></i>
              <a class="float-left" href="mailto:<?php echo do_shortcode('[contact type="email_address"]') ?>"> <?php echo do_shortcode('[contact type="email_address"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fab fa-skype"></i>
              <a class="float-left" href="skype:<?php echo do_shortcode('[contact type="skype"]') ?>"> <?php echo do_shortcode('[contact type="skype"]') ?></a>
              <div class="clearfix"></div>
            </li>
            <li>
              <i class="fal fa-phone"></i>
              <a class="float-left" href="tel:<?php echo do_shortcode('[contact type="india mobile"]') ?>"> India:<?php echo do_shortcode('[contact type="india mobile"]') ?></a>,
              <a href="tel:<?php echo do_shortcode('[contact type="india_mobile2"]') ?>"> <?php echo do_shortcode('[contact type="india_mobile2"]') ?></a>
              <br>
              USA:
                <a href="tel:<?php echo do_shortcode('[contact type="other_mobile2"]') ?>"> <?php echo do_shortcode('[contact type="other_mobile2"]') ?></a>
                <div class="clearfix"></div>
            </li>
          </ul>
          <div class="offices">
            <div class="float-left">
              Offices: <img src="<?php  echo get_template_directory_uri() ?>/images/india.png" alt="india" class="ml-2"><span class="india_div">India</span>
            </div>
            <div  class="float-left usa-div">
            
            <img src="<?php  echo get_template_directory_uri() ?>/images/usa.png" alt="usa"><span class="">USA</span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- End Inner footer -->
<!--End contact  section7-->
<?php get_footer(); ?> 
<script type="text/javascript">

     $('#project_slider').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    center:true,
    lazyLoad: true,
     dots:true,
  responsive:{
        0:{
            items:1,
            stagePadding: 60
        },
        600:{
            items:1,
            stagePadding: 100
        },
        1000:{
            items:2,
            // stagePadding: 100
        },

    }
})
     
         $('#framework_slider').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    // center:true,
    // lazyLoad: true,
     dots:false,
  responsive:{
        0:{
            items:2,
                    autoplay:true,
autoplayTimeout:3000,
        },
        600:{
              autoplay:true,
autoplayTimeout:3000,
            items:3,
        },
        1000:{
            items:6,
            // stagePadding: 100
        },

    }
})
         $('#client_slider').owlCarousel({
    loop:true,
    margin:30,
    autoplay:false,
    dots:true,
autoplayTimeout:3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
})
</script>