<?php
  // Template Name:Sevices Listing Page
  the_post();
  /**
  * The main template file
  *
  * This is the most generic template file in a WordPress theme
  * and one of the two required files for a theme ( the other being style.css ).
  * It is used to display a page when nothing more specific matches a query.
  * E.g., it puts together the home page when no home.php file exists.
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package geekologix_website
  */
  
  get_header(); ?>
<!-- Start section 1 -->
<section class="sectionss" id="">
  <div id="" class="service_listing">
 <?php include 'header2.php'; ?>
    <div class="container-fluid common_heading">
      <div class="row">
        <div class="col-lg-6 col-md-6 pr-0">
          <h1 class="banner-heading ">
            <?php  echo the_title();?>
            <!--  <div id="text-type"></div> -->
          </h1>
          <?php echo the_content(); ?>
    <!--       <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. </span>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,.</p> -->
          <!--   <a href="#" title="Explore" class="web-btn web-btn-banner text-uppercase wow pulse">Explore</a> -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End section 1 -->
<!-- start Section 2  Services -->
<section class="section service_sections" id="">
  <div class="container">
    <div class="row">
       <?php
          $geekologixservices = array(
            "post_type"   => "geekologixservices",
            "post_status"   => "publish",
            "posts_per_page" => 8,
            "order"    => "ASC",
          );
          $geekologixservicesData = new WP_Query($geekologixservices);
        $count = 1;
          while ($geekologixservicesData->have_posts()) : $geekologixservicesData->the_post();
                  $get_services_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium' );
                    $servicesimage = $get_services_image[0];

          if ($count == 1){
                        $addClass='';
                      }
                      if ($count == 2){

                          $addClass='services2';
                      }
                       if ($count == 3){

                          $addClass='';
                      }
                         if ($count == 4){

                          $addClass='services2';
                      }
                         if ($count == 5){

                          $addClass='';
                      }
                         if ($count == 6){

                          $addClass='services2';
                      }
                         if ($count == 7){

                          $addClass='';
                      }
                     if ($count == 8){

                          $addClass='services2';
                      }

          ?>
      <div class="col-lg-6 col-md-6">
        <div class="listing_box <?php echo $addClass; ?> ">
          <a href="" class="listing_heading">
          <img src="<?php echo $servicesimage; ?>" alt="<?php echo the_title(); ?>">
          </a> 
          <a href="" title="<?php echo the_title(); ?>" class="text-uppercase listing_heading"> <?php echo the_title(); ?></a>
          <p>                         <?php 
                      
                    echo the_content();
                     ?></p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
          
                        <?php 
                          while( have_rows('service_tag') ): the_row(); 

                          ?>
                        <li>
                           <span><?php echo get_sub_field('add_tag_name'); ?></span> 
                        </li>
                             <?php  
                        endwhile; 
                    ?>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
           <?php  
                         $count++; endwhile;
          wp_reset_query();
          ?>
  <!--     <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/mobile_wearaables.png" alt="MOBILE & WEARABLES">
          </a> 
          <a href="" title="MOBILE & WEARABLES" class="text-uppercase listing_heading">MOBILE & WEARABLES</a>
          <p>We speak the language of creativity; building smart mobile applications for smart users.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Android</li>
            <li>Ios</li>
            <li>REACT</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Digital Marketing">
          </a> 
          <a href="" title="Web Development" class="text-uppercase listing_heading">DIGITAL MARKETING</a>
          <p>Make the most of your digital presence; Custom websites at affordable prices.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Seo</li>
            <li>ppc</li>
            <li>sem</li>
            <li>smo</li>
            <li>smm</li>
            <li>Mobile Marketing</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Ecommerce Development">
          </a> 
          <a href="" title="Web Development" class="text-uppercase listing_heading">ECOMMERCE DEVELOPMENT</a>
          <p>Establishing and empowering businesses online with e-com websites.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Shopify</li>
            <li>Magento</li>
            <li>Opencart</li>
            <li>Wordpress</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Devops Consulting">
          </a> 
          <a href="" title="DEVOPS CONSULTING" class="text-uppercase listing_heading">Devops Consulting</a>
          <p>Make the most of your digital presence; Custom websites at affordable prices.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Devops Assessment</li>
            <li>Devops Automation</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Cloud Based Sevices">
          </a> 
          <a href="" title="Cloud Based Sevices" class="text-uppercase listing_heading">Cloud Based Sevices</a>
          <p>Work Smarter with higher efficiency; Pay as you use.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Amazon Web Services</li>
            <li>Microsoft Azure</li>
            <li>Digitalocean</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Industry Solutions">
          </a> 
          <a href="" title="Industry Solutions" class="text-uppercase listing_heading">Industry Solutions</a>
          <p>Providing you the best in class solutions in quality and trust.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Consultation</li>
            <li>Enterprise Mobility</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="listing_box services2">
          <a href="" class="listing_heading">
          <img src="<?php  echo get_template_directory_uri() ?>/images/digital-market.png" alt="Business Consulting">
          </a> 
          <a href="" title="Business Consulting" class="text-uppercase listing_heading">Business Consulting</a>
          <p>Smart Ideas for a better future, better growth, better marketing.</p>
          <i class="fas fa-circle"></i>
          <ul class="service_tag">
            <li>Business Structuring</li>
            <li>Growth Strategy</li>
          </ul>
          <div class="clearfix"></div>
          <a href="" class="read_more text-uppercase" title="Read More">Read More</a>
        </div>
      </div> -->
    </div>
  </div>
</section>
<!-- End section 2  Services-->

<!--End contact  section7-->
<?php get_footer(); ?>