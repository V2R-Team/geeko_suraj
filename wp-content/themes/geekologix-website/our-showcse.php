<section class="mobile_project  common_sliders" id="showcase">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="inner_heading text-center">Our Showcase</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme" id="project_slider">
                    <div class="item">
                        <div class="mesmo_bg2 project_slider_bg">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 project_detail">
                                    <h5>PursueIt</h5>
                                    <p>An innovative online platform for UAE locals to help them find their favourite
                                        outdoor activities in the nearby areas and book them with Promotion Codes and
                                        App Discounts.</p>
                                    <a href="" class="text-uppercase" title="View Portfolio">View Portfolio</a>
                                </div>
                                <div class="col-lg-6 col-md-12 d-none d-md-block">
                                    <div class="mesmo-mobile-img">
                                        <img src="<?php echo get_template_directory_uri() ?>/images/persuit-mobile.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="mesmo_bg project_slider_bg">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 project_detail">
                                    <h5>Mesmo</h5>
                                    <p>An interactive Platform to help people search for hotels in the registered cities
                                        and book rooms with app special discounts, book e-tickets for movie, museum,
                                        earn rewards, and many more.</p>
                                    <a href="" class="text-uppercase" title="View Portfolio">View Portfolio</a>
                                </div>
                                <div class="col-lg-6 col-md-12 d-none d-md-block">
                                    <div class="mesmo-mobile-img">
                                        <img src="<?php echo get_template_directory_uri() ?>/images/mesmo-mobile.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="mesmo_bg3 project_slider_bg">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 project_detail">
                                    <h5>Airline Review</h5>
                                    <p>An online forum for people to discuss airline reviews specifically. The app also
                                        allows people to check Airline schedules and the status of the flights booked on
                                        app itself.</p>
                                    <a href="" class="text-uppercase" title="View Portfolio">View Portfolio</a>
                                </div>
                                <div class="col-lg-6 col-md-12 d-none d-md-block">
                                    <div class="mesmo-mobile-img">
                                        <img src="<?php echo get_template_directory_uri() ?>/images/airline-mobile.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>